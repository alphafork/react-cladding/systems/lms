import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import FormContainer from '../crud/FormContainer';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListReferrerContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const referrers = useApi(api.activeReferrer);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });
  const [showPopup, setShowPopup] = useState({
    referrerForm: false,
  });

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const action = (rowData) => (
    <Button
      label="View Referrals"
      icon="pi pi-eye"
      className="m-1 px-3 py-2"
      severity="success"
      onClick={() => (
        navigate(`/referrer/${rowData.id}`)
      )}
    />
  );

  const columns = [
    { header: 'Referrer', field: 'name' },
    { header: 'Email', field: 'email' },
    { header: 'Phone', field: 'phone_no' },
    { header: 'Action', body: action },
  ];

  const buttons = [{
    label: 'Add Referrer',
    icon: 'user-plus',
    onClick: () => togglePopup('referrerForm'),
  }];

  const eventHandlers = {
    onSuccess: () => {
      togglePopup('referrerForm');
      referrers.sendRequest({ url: api.referrer });
    },
    onCancel: () => togglePopup('referrerForm'),
  };

  return (
    <>
      <PageHeader
        title="Referrers"
        buttons={buttons}
      />

      <ListDataTable
        data={referrers.data}
        header={renderHeader}
        globalFilterFields={[
          'name',
          'email',
          'phone_no',
        ]}
        columns={columns}
        filters={filters}
      />

      <Dialog
        header="Add Referrer"
        visible={showPopup.referrerForm}
        style={{ width: '75vw' }}
        onHide={() => togglePopup('referrerForm')}
      >
        <FormContainer
          path="referrer"
          eventHandlers={eventHandlers}
        />
      </Dialog>

    </>
  );
}

export default ListReferrerContainer;
