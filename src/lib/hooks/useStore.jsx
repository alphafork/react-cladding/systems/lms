import { create } from 'zustand';

const useStore = create((set) => ({
  user: null,
  setUser: (user) => set({ user }),

  institution: null,
  setInstitution: (institution) => set({ institution }),

  notifications: null,
  setNotifications: (notifications) => set({ notifications }),
}));

export default useStore;
