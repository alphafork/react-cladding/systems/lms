import React from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';

function Detail({
  headerFields,
  fields,
  showBlankFields,
}) {
  return (
    !fields
      ? (
        <div className="flex justify-content-center">
          <ProgressSpinner />
        </div>
      ) : (
        <div className="surface-0 shadow-2 p-4 mt-2 border-round">
          {headerFields && (
            <div className="flex align-items-start flex-column lg:justify-content-between lg:flex-row mb-2">
              <div>
                {headerFields?.title && (
                  <div className="font-medium text-2xl text-900 mb-2">
                    {headerFields.title}
                  </div>
                )}
                {headerFields?.subtitle && (
                  <div className="text-500 mb-3">
                    {headerFields.subtitle}
                  </div>
                )}
              </div>
            </div>
          )}
          <ul className="list-none p-0 m-0">
            {Object.values(fields).map(({ label, value }) => (
              (showBlankFields || value) && (
                <li key={label} className={`flex flex-wrap align-items-center px-2 py-3 border-300 ${headerFields ? 'border-top-1' : 'border-bottom-1'}`}>
                  <div className="text-500 w-4 font-medium">{label}</div>
                  <div className="text-900 w-8 md:flex-order-0 flex-order-1">{value}</div>
                </li>
              )
            ))}
          </ul>
        </div>
      )
  );
}

export default Detail;
