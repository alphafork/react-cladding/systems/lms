import React from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';
import { Button } from 'primereact/button';
import InputField from '../InputField';

function Form({
  schema,
  data,
  relatedData,
  excludedFields,
  eventHandlers,
  fieldAlerts,
  buttons,
}) {
  return (
    <form>
      { !schema
        ? (
          <div className="flex justify-content-center">
            <ProgressSpinner />
          </div>
        ) : (
          <div className="surface-0 shadow-2 p-4 mt-2 border-round">
            {Object.entries(schema).map(([fieldName, fieldInfo]) => (
              !excludedFields?.includes(fieldName) && (
                <div key={fieldName}>
                  <label htmlFor={fieldName} className="block text-900 font-medium mb-2">
                    { fieldInfo.label }
                  </label>
                  <InputField
                    fieldName={fieldName}
                    fieldInfo={fieldInfo}
                    fieldAlert={fieldAlerts?.[fieldName]}
                    formData={data}
                    relatedData={relatedData}
                    eventHandlers={eventHandlers}
                  />
                </div>
              )
            ))}
            <div className="mt-2">
              { buttons && buttons.map((btn) => (
                <Button
                  key={btn.label.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')}
                  label={btn.label}
                  aria-label={btn.label}
                  className={`mr-2 mb-3 ${btn.extraClasses}`}
                  icon={`pi pi-${btn.icon}`}
                  severity={btn.severity ?? false}
                  onClick={btn.onClick ?? false}
                />
              ))}
            </div>
          </div>
        )}
    </form>
  );
}

export default Form;
