import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import useFieldParser from '../../hooks/useFieldParser';
import useApi from '../../hooks/useApi';
import api from '../../config/api';
import getConfig from '../../utils/getConfig';

function DetailExamPopup({
  visible,
  onHide,
  batchId,
  examId,
}) {
  const navigate = useNavigate();
  const config = getConfig('exam');
  const {
    displayData,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, examId);
  const examResult = useApi();

  useEffect(() => {
    if (examId) {
      const url = `${api.examResult}?filter_by={"exam":${examId}}`;
      examResult.sendRequest({ url });
    }
  }, [examId]);

  const buttons = () => {
    if (!examResult.data?.results) {
      return;
    }

    const examButtons = [];
    if (examResult.data.results?.length === 0) {
      const route = `/batch/${batchId}/exam/${examId}/result/create`;
      examButtons.push({
        label: 'Submit Marklist',
        onClick: () => navigate(route),
      });
    } else {
      const route = `/batch/${batchId}/exam/${examId}/result`;
      examButtons.push({
        label: 'View Marklist',
        onClick: () => navigate(route),
      });
    }

    return examButtons;
  };

  return (
    <Dialog
      header="Exam detail"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <PageHeader title="Exam" buttons={buttons()} />
      {examId && (
        <Detail
          headerFields={displayData?.headerFields}
          fields={displayData?.fields}
        />
      )}
      { getSchemaAlert }
      { getApiAlert }
    </Dialog>
  );
}

export default DetailExamPopup;
