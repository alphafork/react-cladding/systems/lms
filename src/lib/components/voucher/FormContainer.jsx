import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import Form from './Form';
import useSchema from '../../hooks/useSchema';
import useApi from '../../hooks/useApi';
import handleFormChange from '../../utils/crud/handleFormChange';
import voucher from '../../config/voucher';
import api from '../../config/api';

function FormContainer({
  path,
  schemaPath,
  action,
  customData,
  formButtons,
  onFormChange,
  onFormSubmit,
}) {
  dayjs.extend(utc);
  const { schema, relatedData, getSchemaAlert } = useSchema(schemaPath ?? path);
  const setFields = useApi();
  const accounts = useApi(api.transactionAccount);
  const [formData, setFormData] = useState(null);
  const [accountSchema, setAccountSchema] = useState(null);
  const { excludedFields } = voucher.create ?? [];

  useEffect(() => {
    if (!accounts.data?.results?.length || accountSchema) {
      return;
    }
    for (const account of accounts.data.results) {
      setAccountSchema((prevAccountSchema) => ({
        ...prevAccountSchema,
        [account.name]: {
          type: 'integer',
          label: account.name,
        },
      }));
    }
  }, [accounts.data, accountSchema]);

  useEffect(() => {
    if (!formData) {
      const data = { ...customData };
      data.date = data.date ?? dayjs(new Date()).format('YYYY-MM-DD');
      setFormData(data);
    }
  }, [formData, customData]);

  const onChange = (event) => {
    const { name } = event.target;
    const _schema = { ...schema, ...accountSchema };
    const { type, native_type: nativeType } = _schema[name];
    const handleChange = onFormChange ?? handleFormChange;
    handleChange(event, type, nativeType, setFormData);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    const method = action === 'create' ? 'POST' : 'PUT';
    const data = { ...formData };
    const paymentSplitup = {};
    for (const account of Object.keys(accountSchema)) {
      paymentSplitup[account] = data[account];
      delete data[account];
    }
    setFields.sendRequest({
      url: path,
      method,
      data: {
        ...data,
        payment_splitup: paymentSplitup,
      },
    });
  };

  useEffect(() => {
    if (!setFields.data) {
      return;
    }
    onFormSubmit?.(setFields.data, setFields.status);
  }, [setFields.data, setFields.status]);

  const buttons = formButtons ?? [
    {
      label: 'Submit',
      icon: 'check',
      onClick: onSubmit,
    },
  ];

  const eventHandlers = { onChange };

  return (
    <>
      <Form
        schema={{ ...schema, ...accountSchema }}
        data={formData}
        relatedData={relatedData}
        excludedFields={excludedFields}
        eventHandlers={eventHandlers}
        buttons={buttons}
      />
      { getSchemaAlert }
      { setFields?.getApiAlert }
    </>
  );
}

export default FormContainer;
