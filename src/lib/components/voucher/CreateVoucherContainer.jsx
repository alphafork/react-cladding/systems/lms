import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import PageHeader from '../PageHeader';
import FormContainer from './FormContainer';

function CreateContainer() {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const path = pathname.replace(/\/$/, '').replace(/\/create$/, '/');
  const title = 'Add Voucher';
  const [voucher, setVoucher] = useState(null);

  useEffect(() => {
    if (voucher?.status === 200 || voucher?.status === 201) {
      navigate(path);
    }
  }, [navigate, path, voucher?.status]);

  const onFormSubmit = (data, status) => {
    if (!data || !status) {
      return;
    }
    setVoucher({ data, status });
  };

  return (
    <>
      <PageHeader title={title} />
      <FormContainer
        path={path}
        action="create"
        onFormSubmit={onFormSubmit}
      />
    </>
  );
}

export default CreateContainer;
