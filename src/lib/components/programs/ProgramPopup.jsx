import React from 'react';
import { Dialog } from 'primereact/dialog';
import ProgramListContainer from './ListContainer';

function ProgramPopup({
  visible,
  onHide,
  studentId,
  programType,
}) {
  return (
    <Dialog
      header={`Apply for ${programType}s`}
      visible={visible}
      style={{ width: '75vw', 'min-height': '50%' }}
      onHide={onHide}
    >
      <ProgramListContainer
        studentId={studentId}
        programType={programType}
      />
    </Dialog>
  );
}

export default ProgramPopup;
