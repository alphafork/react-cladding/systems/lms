import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Calendar, dayjsLocalizer } from 'react-big-calendar';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Message } from 'primereact/message';
import { Card } from 'primereact/card';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import AddDaySchedulePopup from '../schedule/AddDaySchedulePopup';
import AddRepeatingSchedulePopup from '../schedule/AddRepeatingSchedulePopup';
import DetailSchedulePopup from '../schedule/DetailSchedulePopup';
import UpdateSchedulePopup from '../schedule/UpdateSchedulePopup';
import DeleteSchedulePopup from '../schedule/DeleteSchedulePopup';
import DetailExamPopup from '../exams/DetailExamPopup';
import useApi from '../../hooks/useApi';
import useFieldParser from '../../hooks/useFieldParser';
import getConfig from '../../utils/getConfig';
import api from '../../config/api';
import { convertToDatetime } from '../../utils/handleTime';
import '../../assets/sass/react-big-calendar.scss';

function DetailContainer() {
  dayjs.extend(utc);
  const { id: batch } = useParams();
  const path = 'batch';
  const navigate = useNavigate();
  const localizer = dayjsLocalizer(dayjs);
  const packagesUrl = `${api.batchPackages}?batch_id=${batch}`;
  const packages = useApi(packagesUrl);
  const subjects = useApi(`${api.batchSubject}/${batch}`);
  const dailyScheduleUrl = `${api.dailySchedule}?filter_by={"schedule__batch":${batch}}`;
  const dailySchedule = useApi(dailyScheduleUrl);
  const examsUrl = `${api.exam}?filter_by={"batch":${batch}}`;
  const exams = useApi(examsUrl);
  const setOneDaySchedule = useApi();
  const setRepeatingSchedule = useApi();
  const updateSchedule = useApi();
  const scheduleAvailability = useApi();
  const remove = useApi();
  const config = getConfig(path);
  const {
    rawData,
    displayData,
    status,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, batch);
  const [fields, setFields] = useState(null);
  const [fieldAlerts, setFieldAlerts] = useState(null);
  const [events, setEvents] = useState(null);
  const [activeEvent, setActiveEvent] = useState(null);
  const [activeExam, setActiveExam] = useState(null);
  const [showPopup, setShowPopup] = useState({
    oneDaySchedule: false,
    repeatingSchedule: false,
    scheduleDetail: false,
    editSchedule: false,
    deleteSchedule: false,
    examDetail: false,
  });
  const [defaultScheduleData, setDefaultScheduleData] = useState({
    batch: Number(batch),
    date: new Date(),
  });

  useEffect(() => {
    if (rawData?.batch_mentor && rawData?.room) {
      setDefaultScheduleData((prevDefaultScheduleData) => ({
        ...prevDefaultScheduleData,
        faculty: rawData.batch_mentor,
        room: rawData.room,
      }));
    }
  }, [rawData?.batch_mentor, rawData?.room]);

  const deleteItem = async (apiUrl, apiMethod) => {
    const url = apiUrl ?? `${config.api}/${batch}`;
    const method = apiMethod ?? 'DELETE';
    remove.sendRequest({ url, method });
  };

  const confirmDelete = (apiUrl, apiMethod) => {
    confirmDialog({
      message: 'Are you sure you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      acceptClassName: 'p-button-danger',
      accept: () => deleteItem(apiUrl, apiMethod),
    });
  };

  const [buttons, setButtons] = useState([
    {
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate(`/${path}`),
      extraClasses: 'p-button-outlined',
    }, {
      label: 'Edit',
      icon: 'pencil',
      onClick: () => navigate('edit'),
    }, {
      label: 'Delete',
      icon: 'trash',
      onClick: () => confirmDelete(),
      severity: 'danger',
    },
  ]);
  const title = `${path.charAt(0).toUpperCase()}${path.replace(/-/g, ' ').slice(1)} details`;

  useEffect(() => {
    if (subjects.data) {
      setDefaultScheduleData((prevDefaultScheduleData) => ({
        ...prevDefaultScheduleData,
        subject: subjects?.data?.[0]?.id,
      }));
    }
  }, [subjects.data]);

  useEffect(() => {
    if (dailySchedule.data?.results) {
      setEvents(dailySchedule.data.results?.map((event, index) => (
        {
          index,
          title: event.subject_name,
          start: new Date(`${event.date} ${event.start_time}`),
          end: new Date(`${event.date} ${event.end_time}`),
        }
      )));
    }
  }, [dailySchedule.data]);

  const togglePopup = (key) => {
    if (showPopup[key]) {
      setFieldAlerts(null);
      setDefaultScheduleData((prevDefaultScheduleData) => ({
        ...prevDefaultScheduleData,
        date: new Date(),
      }));
    }
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const handleSelectSlot = ({ start }) => {
    setDefaultScheduleData((prevDefaultScheduleData) => ({
      ...prevDefaultScheduleData,
      date: start,
    }));
    togglePopup('oneDaySchedule');
  };

  const handleSelectEvent = (event) => {
    const {
      id,
      schedule,
      date,
      start_time: startTime,
      end_time: endTime,
      faculty_name: faculty,
      subject_name: subject,
      room_name: room,
    } = dailySchedule.data?.results?.[event.index] ?? {};
    setActiveEvent({
      id,
      schedule,
      headerFields: {
        title: dayjs.utc(date).format('ll'),
        subtitle: `${dayjs.utc(date + startTime).format('hh:mm A')} - ${dayjs.utc(date + endTime).format('hh:mm A')}`,
      },
      fields: {
        faculty: {
          label: 'Faculty',
          value: faculty,
        },
        subject: {
          label: 'Subject',
          value: subject,
        },
        room: {
          label: 'Room',
          value: room === 'None' ? '' : room,
        },
      },
    });
    togglePopup('scheduleDetail');
  };

  useEffect(() => {
    if (status === 404) {
      navigate('/');
    }
  }, [status, navigate]);

  useEffect(() => {
    if (remove.status === 204) navigate(`/${path}`);
  }, [remove.status, navigate, path]);

  useEffect(() => {
    if (packages.data?.results?.length > 0 && buttons.length < 4) {
      setButtons((prevButtons) => ([
        ...prevButtons,
        {
          label: 'Migrate',
          icon: 'directions',
          onClick: () => { navigate(`/batch/${batch}/migrate`); },
          severity: 'warning',
        },
      ]));
    }
  }, [packages.data?.results]);

  const scheduleButtons = [
    {
      label: 'Add One Day Schedule',
      icon: 'calendar',
      onClick: () => togglePopup('oneDaySchedule'),
    },
    {
      label: 'Add Repeating Schedule',
      icon: 'calendar-plus',
      onClick: () => togglePopup('repeatingSchedule'),
    },
  ];

  const eventHandlers = {
    afterChange: (event, formData) => {
      event.preventDefault();
      let {
        date,
        start_date: startDate,
        end_date: endDate,
        start_time: startTime,
        end_time: endTime,
      } = formData;
      if (date) {
        date = dayjs(date).format('YYYY-MM-DD');
      }
      if (startDate) {
        startDate = dayjs(startDate).format('YYYY-MM-DD');
      }
      if (endDate) {
        endDate = dayjs(endDate).format('YYYY-MM-DD');
      }
      if (startTime) {
        const time = convertToDatetime(startTime);
        startTime = dayjs(time).format('HH:mm');
      }
      if (endTime) {
        const time = convertToDatetime(endTime);
        endTime = dayjs(time).format('HH:mm');
      }
      let url = `${api.scheduleAvailability}`
        + `?start_time=${startTime}`
        + `&end_time=${endTime}`;
      if (date && startTime && endTime) {
        url += `&start_date=${date}&end_date=${date}`;
        scheduleAvailability.sendRequest({ url });
      }
      if (startDate && endDate && startTime && endTime) {
        url += `&start_date=${startDate}&end_date=${endDate}`;
        scheduleAvailability.sendRequest({ url });
      }
      setFields({
        ...formData,
        start_time: startTime,
        end_time: endTime,
      });
    },
    onSubmit: (event, formData) => {
      event.preventDefault();
      const startTime = convertToDatetime(formData.start_time);
      const endTime = convertToDatetime(formData.end_time);
      const data = {
        ...formData,
        start_time: dayjs(startTime)
          .set('second', 0)
          .format('HH:mm'),
        end_time: dayjs(endTime)
          .set('second', 0)
          .format('HH:mm'),
      };
      if (formData.date && !formData.weekdays) {
        data.date = dayjs(formData.date).format('YYYY-MM-DD');
        if (formData.id) {
          updateSchedule.sendRequest({
            url: `${api.dailySchedule}/${formData.id}`,
            method: 'PUT',
            data,
          });
        } else {
          setOneDaySchedule.sendRequest({
            url: api.schedule,
            method: 'POST',
            data,
          });
        }
      }
      if (formData.start_date && formData.end_date && formData.weekdays) {
        data.start_date = dayjs(formData.start_date).format('YYYY-MM-DD');
        data.end_date = dayjs(formData.end_date).format('YYYY-MM-DD');
        setRepeatingSchedule.sendRequest({
          url: api.repeatingSchedule,
          method: 'POST',
          data,
        });
      }
    },
    onCancel: () => {
      for (const popup in showPopup) {
        if (showPopup[popup]) {
          togglePopup(popup);
        }
      }
    },
  };

  useEffect(() => {
    const {
      faculty_availability: faculties,
      room_availability: rooms,
    } = scheduleAvailability?.data ?? {};

    if (fields?.faculty && faculties) {
      const facultySchedule = faculties
        .filter((schedule) => schedule?.faculty === fields.faculty)
        .filter((schedule) => schedule?.faculty_schedule?.length > 0);
      if (facultySchedule.length > 0) {
        setFieldAlerts((prevFieldAlerts) => ({
          ...prevFieldAlerts,
          faculty: (
            <Message
              severity="warn"
              text="Faculty not available for the selected schedule"
            />
          ),
        }));
      } else {
        setFieldAlerts((prevFieldAlerts) => ({
          ...prevFieldAlerts,
          faculty: null,
        }));
      }
    }

    if (fields?.room && rooms) {
      const roomSchedule = rooms
        .filter((schedule) => schedule?.room === fields.room)
        .filter((schedule) => schedule?.room_schedule?.length > 0);
      if (roomSchedule.length > 0) {
        setFieldAlerts((prevFieldAlerts) => ({
          ...prevFieldAlerts,
          room: (
            <Message
              severity="warn"
              text="Room not available for the selected schedule"
            />
          ),
        }));
      } else {
        setFieldAlerts((prevFieldAlerts) => ({
          ...prevFieldAlerts,
          room: null,
        }));
      }
    }
  }, [scheduleAvailability.data, fields]);

  useEffect(() => {
    if (setOneDaySchedule.status === 200 || setOneDaySchedule.status === 201) {
      togglePopup('oneDaySchedule');
      dailySchedule.sendRequest({
        url: dailyScheduleUrl,
        method: 'GET',
      });
    }
    setOneDaySchedule.status = null;
  }, [setOneDaySchedule.status]);

  useEffect(() => {
    if (setRepeatingSchedule.status === 200 || setRepeatingSchedule.status === 201) {
      togglePopup('repeatingSchedule');
      dailySchedule.sendRequest({
        url: dailyScheduleUrl,
        method: 'GET',
      });
    }
    setRepeatingSchedule.status = null;
  }, [setRepeatingSchedule.status]);

  useEffect(() => {
    if (updateSchedule.status === 200) {
      togglePopup('editSchedule');
      dailySchedule.sendRequest({
        url: dailyScheduleUrl,
        method: 'GET',
      });
    }
  }, [updateSchedule.status]);

  const onClickEditSchedule = () => {
    togglePopup('scheduleDetail');
    togglePopup('editSchedule');
  };

  const onClickDeleteSchedule = () => {
    togglePopup('scheduleDetail');
    togglePopup('deleteSchedule');
  };

  return (
    <>
      <ConfirmDialog />
      <PageHeader title={title} buttons={buttons} />
      <Detail
        headerFields={displayData?.headerFields}
        fields={displayData?.fields}
        showBlankFields
      />
      <div className="mt-6">
        <PageHeader
          title="Schedule"
          buttons={scheduleButtons}
        />
      </div>
      <AddDaySchedulePopup
        visible={showPopup.oneDaySchedule}
        onHide={() => togglePopup('oneDaySchedule')}
        eventHandlers={eventHandlers}
        defaultData={defaultScheduleData}
        fieldAlerts={fieldAlerts}
      />
      <AddRepeatingSchedulePopup
        visible={showPopup.repeatingSchedule}
        onHide={() => togglePopup('repeatingSchedule')}
        eventHandlers={eventHandlers}
        defaultData={defaultScheduleData}
        fieldAlerts={fieldAlerts}
      />
      <div className="surface-0 shadow-2 p-4 mt-2 border-round">
        <Calendar
          localizer={localizer}
          dayLayoutAlgorithm="no-overlap"
          events={events ?? []}
          onSelectSlot={handleSelectSlot}
          onSelectEvent={handleSelectEvent}
          selectable
          style={{ height: 512 }}
        />
      </div>
      {exams.data?.results && (
        <Card title="Exams" className="surface-ground mt-5">
          <div className="grid">
            {exams.data.results?.map?.((exam) => (
              <div
                className="col-12 md:col-6 lg:col-3"
                style={{ cursor: 'pointer' }}
                onClick={() => {
                  setActiveExam(exam.id);
                  togglePopup('examDetail');
                }}
              >
                <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
                  <div className="flex justify-content-between mb-3">
                    <div>
                      <div className="text-900 font-medium text-xl">
                        {exam.name}
                      </div>
                      <span className="block font-medium mt-4">
                        {
                          `Date: ${dayjs(exam.date).format('ddd, MMM D, YYYY')}`
                        }
                      </span>
                      <span className="block font-medium mt-2">
                        {
                          `Time: ${dayjs(convertToDatetime(exam.start_time)).format('hh:mm A')}`
                            + ' - '
                            + `${dayjs(convertToDatetime(exam.end_time)).format('hh:mm A')}`
                        }
                      </span>
                    </div>
                    <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '2.5rem', height: '2.5rem' }}>
                      <i className="pi pi-server text-blue-500 text-xl" />
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </Card>
      )}
      <DetailSchedulePopup
        visible={showPopup.scheduleDetail}
        onHide={() => togglePopup('scheduleDetail')}
        onClickEditSchedule={onClickEditSchedule}
        onClickDeleteSchedule={onClickDeleteSchedule}
        batch={batch}
        event={activeEvent}
      />
      <UpdateSchedulePopup
        visible={showPopup.editSchedule}
        onHide={() => togglePopup('editSchedule')}
        eventHandlers={eventHandlers}
        event={activeEvent}
      />
      <DeleteSchedulePopup
        visible={showPopup.deleteSchedule}
        onHide={() => togglePopup('deleteSchedule')}
        event={activeEvent}
        confirmDelete={confirmDelete}
      />
      <DetailExamPopup
        visible={showPopup.examDetail}
        onHide={() => togglePopup('examDetail')}
        batchId={batch}
        examId={activeExam}
      />
      { setOneDaySchedule.getApiAlert }
      { setRepeatingSchedule.getApiAlert }
      { updateSchedule.getApiAlert }
      { getSchemaAlert }
      { getApiAlert }
      { remove.getApiAlert }
    </>
  );
}

export default DetailContainer;
