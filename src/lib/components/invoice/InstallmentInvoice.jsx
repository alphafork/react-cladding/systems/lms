import React from 'react';
import { Dialog } from 'primereact/dialog';
import FormContainer from './FormContainer';
import api from '../../config/api';

function InstallmentInvoice({
  visible,
  onHide,
  invoiceData,
  onFormSubmit,
}) {
  return (
    <Dialog
      header="Installment Invoice"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      {invoiceData?.address && (
        <FormContainer
          path={api.studentInvoice}
          action="create"
          customData={invoiceData}
          onFormSubmit={onFormSubmit}
        />
      )}
    </Dialog>
  );
}

export default InstallmentInvoice;
