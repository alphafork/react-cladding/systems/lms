import StudentSideBar from './Student';
import FacultySideBar from './Faculty';
import AccountantSideBar from './Accountant';
import ManagerSideBar from './Manager';
import AdminSideBar from './Admin';

export {
  StudentSideBar,
  FacultySideBar,
  AccountantSideBar,
  ManagerSideBar,
  AdminSideBar,
};
