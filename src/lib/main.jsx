import PublicRegistrationContainer from './components/students/PublicRegistrationContainer';
import RegistrationContainer from './components/students/RegistrationContainer';
import InstitutionProfileContainer from './components/institution/InstitutionProfileContainer';
import DetailBatchContainer from './components/batches/DetailContainer';
import ListMigrationContainer from './components/batches/ListMigrationContainer';
import CreateExamContainer from './components/exams/CreateContainer';
import ListExamResultContainer from './components/exams/ListExamResultContainer';
import ListAttendanceContainer from './components/batches/ListAttendanceContainer';
import DetailStudentProgram from './components/students/DetailStudentProgram';
import DetailProfileContainer from './components/profile/DetailProfileContainer';
import ListStaffContainer from './components/staff/ListContainer';
import CreateStaffContainer from './components/staff/CreateContainer';
import RegisterStaffContainer from './components/staff/RegisterContainer';
import ListAllStudentContainer from './components/students/ListAllStudentContainer';
import ListStudentContainer from './components/students/ListStudentContainer';
import ListTransactionContainer from './components/accounting/ListTransactionContainer';
import ListFeeContainer from './components/fee/ListFeeContainer';
import ListOperationContainer from './components/operations/ListContainer';
import DetailOperationContainer from './components/operations/DetailContainer';
import ListOperationLogContainer from './components/operations/ListLogContainer';
import DetailOperationLogContainer from './components/operations/DetailLogContainer';
import DailyAccountingOperation from './components/accounting/DailyAccountingOperation';
import ListInvoiceContainer from './components/invoice/ListInvoiceContainer';
import CreateInvoiceContainer from './components/invoice/CreateInvoiceContainer';
import ListVoucherContainer from './components/voucher/ListVoucherContainer';
import CreateVoucherContainer from './components/voucher/CreateVoucherContainer';
import ListPermissionContainer from './components/permissions/ListPermissionContainer';
import ListNotificationContainer from './components/notifications/ListNotificationContainer';
import CertificatePreview from './components/public/CertificatePreview';
import JobPlacementContainer from './components/profile/JobPlacementContainer';
import ReferralContainer from './components/referrals/ReferralContainer';
import ListReferrerContainer from './components/referrals/ListReferrerContainer';
import ListReferralContainer from './components/referrals/ListReferralContainer';
import ChangePasswordContainer from './components/profile/ChangePasswordContainer';
import PageNotFound from './components/public/PageNotFound';

import Home from './modules/home/components/Home';
import ListContainer from './modules/generic-crud/components/ListContainer';
import DetailContainer from './modules/generic-crud/components/DetailContainer';
import CreateContainer from './modules/generic-crud/components/CreateContainer';
import UpdateContainer from './modules/generic-crud/components/UpdateContainer';
import ProgramApplication from './modules/program-application/components/ProgramApplication';
import StudentProfile from './modules/student-profile/components/StudentProfile';

export {
  PublicRegistrationContainer,
  RegistrationContainer,
  InstitutionProfileContainer,
  DetailBatchContainer,
  ListMigrationContainer,
  CreateExamContainer,
  ListExamResultContainer,
  ListAttendanceContainer,
  DetailStudentProgram,
  DetailProfileContainer,
  RegisterStaffContainer,
  ListStaffContainer,
  CreateStaffContainer,
  ListAllStudentContainer,
  ListStudentContainer,
  ListTransactionContainer,
  ListFeeContainer,
  ListOperationContainer,
  DetailOperationContainer,
  ListOperationLogContainer,
  DetailOperationLogContainer,
  DailyAccountingOperation,
  ListInvoiceContainer,
  CreateInvoiceContainer,
  ListVoucherContainer,
  CreateVoucherContainer,
  ListPermissionContainer,
  ListNotificationContainer,
  CertificatePreview,
  JobPlacementContainer,
  ReferralContainer,
  ListReferrerContainer,
  ListReferralContainer,
  ChangePasswordContainer,
  PageNotFound,

  Home,
  ListContainer,
  DetailContainer,
  CreateContainer,
  UpdateContainer,
  ProgramApplication,
  StudentProfile,
};
