import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import Form from './Form';
import useSchema from '../../../hooks/useSchema';
import useApi from '../../../hooks/useApi';
import getConfig from '../../../utils/getConfig';
import { getApiToken } from '../../../utils/api/apiToken';

function FormContainer({
  path,
  resourceId,
  eventHandlers,
  fieldAlerts,
  defaultData,
  redirectOnError = true,
}) {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const { api, excludedFields } = getConfig(path);
  const { schema, relatedData, getSchemaAlert } = useSchema(api);
  const [formData, setFormData] = useState(null);
  const [initialFormData, setInitialFormData] = useState(null);
  const getFields = useApi();
  const setFields = useApi();

  useEffect(() => {
    if (resourceId || !schema || formData) {
      return;
    }

    // Since DRF OPTIONS response doesn't contain the default value for fields,
    // we set all boolean fields to false by default in order to avoid
    // inconsistences between the data shown in the frontend and the actual
    // data that get saved in the backend.
    const booleanFields = {};
    for (const key of Object.keys(schema)) {
      const { type } = schema[key];
      if (type === 'boolean') {
        booleanFields[key] = false;
      }
      setFormData(booleanFields);
    }
  }, [resourceId, schema, formData]);

  useEffect(() => {
    if (!resourceId && !formData && defaultData) {
      setFormData(defaultData);
    }

    if (!resourceId || formData) {
      return;
    }

    const url = `${api}/${resourceId}`;
    getFields.sendRequest({ url });
  }, [api, resourceId, formData]);

  useEffect(() => {
    if (redirectOnError && getFields.status === 404) {
      navigate('/');
    }

    if (getFields.data && !formData) {
      setFormData(getFields.data);
      setInitialFormData(getFields.data);
    }
  }, [getFields.status, navigate, getFields.data, formData]);

  const onChange = (event) => {
    eventHandlers?.beforeChange?.(event, formData, setFormData);
    let newFormData;
    if (eventHandlers?.onChange) {
      eventHandlers.onChange(event, formData, setFormData);
    } else {
      const { name } = event.target;
      let { value } = event.target;
      const { type, native_type: nativeType } = schema[name];
      const isDate = type === 'date';
      const isDateTime = type === 'datetime';
      const isTime = type === 'time';
      const isMultiSelect = nativeType === 'ManyRelatedField';

      if (value instanceof Date) {
        value = isDate ? dayjs(value).format('YYYY-MM-DD') : value;
        value = isDateTime ? dayjs.utc(value).format('YYYY-MM-DDTHH:mm:ss[Z]') ?? false : value;
        value = isTime ? dayjs(value).set('second', 0).format('HH:mm') : value;
      }
      value = isMultiSelect ? value.map((item) => item.id) : value;
      value = value ?? null;
      newFormData = { [name]: value };

      setFormData((prevFormData) => ({
        ...prevFormData,
        ...newFormData,
      }));
    }
    eventHandlers?.afterChange?.(event, { ...formData, ...newFormData }, setFormData);
  };

  const onSelectFile = async (event, fieldName) => {
    const file = event.files[0];
    setFormData((prevFormData) => ({
      ...prevFormData,
      [fieldName]: file,
    }));
  };

  const onCancelFile = (fieldName) => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      [fieldName]: initialFormData[fieldName],
    }));
  };

  const onDeleteFile = (event, fieldName) => {
    event.preventDefault();
    setFormData((prevFormData) => ({
      ...prevFormData,
      [fieldName]: '',
    }));
    setInitialFormData((prevFormData) => ({
      ...prevFormData,
      [fieldName]: '',
    }));
  };

  const onSubmit = (event) => {
    event.preventDefault();
    eventHandlers?.beforeSubmit?.(event, formData);
    if (eventHandlers?.onSubmit) {
      eventHandlers.onSubmit(event, formData);
    } else {
      let url = resourceId ? `${api}/${resourceId}` : api;
      let method = resourceId ? 'PUT' : 'POST';
      const data = { ...formData };
      const extraConfig = {};

      if (getFields.error?.errors?.[0]?.code === 'not_found') {
        url = api;
        method = 'POST';
      }

      const fileFields = Object.keys(schema ?? {}).filter(
        (field) => (
          schema[field].type === 'image upload'
          || schema[field].type === 'file upload'
        ),
      );

      if (fileFields.length > 0) {
        for (const field of fileFields) {
          if (typeof data[field] === 'string' && data[field] !== '') {
            delete data[field];
          }
        }

        const token = getApiToken();
        extraConfig.headers = {
          Authorization: `Token ${token}`,
          'Content-type': 'multipart/form-data',
        };
      }

      setFields.sendRequest({
        url,
        method,
        data,
        extraConfig,
      });
    }
    eventHandlers?.afterSubmit?.(event, formData);
  };

  useEffect(() => {
    const { status } = setFields;
    const onSuccess = () => {
      if (status === 200) {
        navigate(`/${path}/${resourceId}`);
      }
      if (status === 201) {
        navigate(`/${path}`);
      }
    };
    if (status === 200 || status === 201) {
      if (eventHandlers?.onSuccess) {
        eventHandlers.onSuccess(status);
      } else {
        onSuccess();
      }
    }
    if (status >= 400 && status < 600) {
      eventHandlers?.onFailure?.(status);
    }
  }, [setFields.status, navigate, path, resourceId]);

  const formButtons = [
    {
      label: 'Submit',
      icon: 'check',
      onClick: onSubmit,
    },
    {
      label: 'Cancel',
      icon: 'times',
      extraClasses: 'p-button-outlined',
      onClick: (event) => {
        event.preventDefault();
        if (eventHandlers?.onCancel) {
          eventHandlers.onCancel();
        } else {
          navigate(`/${path}`);
        }
      },
    },
  ];

  const fieldEventHandlers = {
    onChange,
    onSelectFile,
    onCancelFile,
    onDeleteFile,
  };

  return (
    <>
      <Form
        schema={schema}
        data={formData}
        relatedData={relatedData}
        excludedFields={excludedFields}
        eventHandlers={fieldEventHandlers}
        fieldAlerts={fieldAlerts}
        buttons={formButtons}
      />
      { getSchemaAlert }
      { !redirectOnError && getFields?.getApiAlert }
      { setFields?.getApiAlert }
    </>
  );
}

export default FormContainer;
