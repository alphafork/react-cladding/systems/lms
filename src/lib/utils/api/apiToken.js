// Wrapper methods for storing and retrieving API access token from
// local storage.

const tokenIdentifier = process.env.API_TOKEN_IDENTIFIER;

const getApiToken = () => {
  const token = window.localStorage.getItem(tokenIdentifier);
  return token;
};

const setApiToken = (token) => {
  window.localStorage.setItem(tokenIdentifier, token);
};

export { getApiToken, setApiToken };
