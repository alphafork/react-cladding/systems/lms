import React from 'react';
import dayjs from 'dayjs';
import {
  Page, Document, StyleSheet, Font,
} from '@react-pdf/renderer';
import Html from 'react-pdf-html';

function VoucherPdfTemplate({ voucher }) {
  Font.register({ family: 'Helvetica' });
  const styles = StyleSheet.create({
    page: {
      flexDirection: 'row',
      backgroundColor: '#ffffff',
      margin: '5px',
      fontFamily: 'Helvetica',
    },
  });

  const html = `
    <html>
      <head>
        <meta charset="utf-8" />
        <style>
          .voucher-box {
            width: 580px;
            margin: auto;
            padding: 30px;
            border: 1px solid #888;
            font-size: 11px;
            line-height: 22px;
            color: #333;
            font-family: 'Helvetica';
          }

          .voucher-box .main-title {
            font-size:26px;
            font-weight:bold;
            text-align:center;
          }

          .voucher-box table {
            width: 520px;
            line-height: inherit;
            text-align: left;
            border-bottom:1px solid #eee;
            padding-bottom:15px;
          }

          .voucher-box table td {
            padding: 5px;
            vertical-align: top;
          }

          .voucher-box table tr td:nth-child(2) {
            text-align: right;
          }

          .voucher-box table tr.top table td {
            padding-bottom: 20px;
          }

          .voucher-box table tr.top table td.title {
            font-size: 12px;
            line-height: 18px;
            font-weight:bold;
            color: #333;
            height:44px;
          }

          .voucher-box table tr.information table td {
            padding-bottom: 40px;
            text-align:left;
          }

          .voucher-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
          }

          .voucher-box table tr.details td {
            padding-bottom: 20px;
          }

          .voucher-box table tr.item td {
            border-bottom: 1px solid #eee;
          }

          .voucher-box table tr.item.last td {
            border-bottom: none;
          }

          .voucher-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
          }

          .voucher-box table .title img {
            width: 120px;
          }

          .voucher-box .note-box {
            text-align:center;
            margin-top:120px;
            border-top:1px solid #888;
          }

          .voucher-box .note {
            font-weight:bold;
            font-size:14px;
            text=align:center;
            width:100%;
          }

          .voucher-box .note-list li{
            padding-left:10px;
            list-style-type: square;
          }

          .voucher-box .band {
            border-top:1px solid #888;
            border-bottom:1px solid #888;
            padding: 8px 0px;
          }
        </style>
      </head>

      <body>
        <div class="voucher-box">
          <table cellPadding="0" cellSpacing="0">
            <tbody>
              <tr>
                <td colSpan="2">
                  <p class="main-title">Voucher</p>
                </td>
              </tr>

              <tr>
                <td colSpan="2">
                  <table>
                    <tbody>
                      <tr>
                        <td class="title">
                          Voucher No: ${voucher?.data?.voucher_no}<br /> <br/>
                          Date: ${dayjs(voucher?.data?.date).format('DD/MM/YYYY')}<br />
                        </td>
                        <td>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr class="information">
                <td colSpan="2">
                  <table>
                  <tbody>
                    <tr>
                      <td colSpan="2">
                      ${(voucher?.data?.address)?.replaceAll?.('\n', '<br/>')}
                      </td>
                    </tr>
                  </tbody>
                  </table>
                </td>
              </tr>

              <tr class="heading">
                <td>Description</td>
                <td>Amount</td>
              </tr>

              <tr>
                <td>${voucher?.data?.title}</td>
                <td>${voucher?.data?.voucher_amount}</td>
              </tr>

              <tr class="total">
                <td></td>
                <td>Total: ${voucher?.data?.voucher_amount}</td>
              </tr>
              <tr class="band">
                <td><b>Payment Method:</b><br>
                  ${JSON.stringify(voucher?.data?.payment_splitup)
    ?.replaceAll?.('{', '')
    ?.replaceAll?.('}', '')
    ?.replaceAll?.('"', '')
    ?.replaceAll?.(':', ': ')
    ?.replaceAll?.(',', '<br>')
}
                </td>
                <td></td>
              </tr>

              <tr>
                <td>Remarks: ${voucher?.data?.remarks}</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </body>
    </html>
  `;

  return (
    html && (
      <Document>
        <Page size="A4" orientation="portrait" dpi="72" style={styles.page}>
          <Html resetStyles>{html}</Html>
        </Page>
      </Document>
    )
  );
}

export default VoucherPdfTemplate;
