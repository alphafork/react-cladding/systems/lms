import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import RegistrationContainer from './RegistrationContainer';
import imgUrl from '../../assets/images/register-1.png';

function PublicRegistrationContainer() {
  const title = 'Student Registration';
  const navigate = useNavigate();

  return (
    <div className="grid grid-nogutter surface-0 text-800 pl-8">
      <div className="col-12 md:col-6 p-6 text-center md:text-left flex">
        <section className="align-items-start justify-content-start">
          <div className="text-6xl text-primary font-bold mb-3">{title}</div>
          <p className="mt-0 mb-4 text-700 line-height-6">
            {`To register a new student fill up the details of the student
            given below. To login go back to the `}
            <Link to="/login"> login</Link>
            &nbsp;form.
          </p>
          <RegistrationContainer
            header={false}
            eventHandlers={{ onSuccess: () => navigate('/login') }}
          />
        </section>
      </div>
      <div className="col-12 md:col-6 overflow-hidden">
        <img
          src={imgUrl}
          alt="Fancy decorator"
          className="md:ml-auto block md:h-full"
          style={{ clipPath: 'polygon(8% 0, 100% 0%, 100% 100%, 0 100%)' }}
        />
      </div>
    </div>
  );
}

export default PublicRegistrationContainer;
