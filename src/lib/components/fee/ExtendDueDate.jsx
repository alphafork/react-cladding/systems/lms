import React from 'react';
import FormContainer from '../crud/FormContainer';

function ExtendDueDate({ student, eventHandlers }) {
  const defaultData = {
    registration: student.registration,
  };

  return (
    <FormContainer
      path="due-extension"
      eventHandlers={eventHandlers}
      defaultData={defaultData}
    />
  );
}

export default ExtendDueDate;
