import React from 'react';
import { Dialog } from 'primereact/dialog';
import DetailStudent from './DetailStudent';

function DetailStudentPopup({
  visible,
  onHide,
  student,
}) {
  return (
    <Dialog
      header="Student Details"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <DetailStudent student={student} />
    </Dialog>
  );
}

export default DetailStudentPopup;
