import React from 'react';
import dayjs from 'dayjs';
import {
  Document,
  Image,
  Font,
  Page,
  StyleSheet,
  Text,
  View,
} from '@react-pdf/renderer';
import headerImg from '../assets/images/header.png';
import footerImg from '../assets/images/footer.png';
import placeholderImg from '../assets/images/student_photo_placeholder.png';
import DetailsFont from '../assets/fonts/GIL.ttf';
import HeadFont from '../assets/fonts/GillSans-Bold.otf';

function IdCardPdfTemplate({ program, photo }) {
  Font.register({
    family: 'GilSansMT',
    format: 'sans-serif',
    src: DetailsFont,
  });
  Font.register({
    family: 'GilSansBold',
    format: 'sans-serif',
    src: HeadFont,
  });
  Font.register({ family: 'Helvetica' });
  const styles = StyleSheet.create({
    body: {
      margin: 0,
      position: 'relative',
    },

    pageBackground: {
      position: 'absolute',
      width: '100%',
      top: 0,
      left: 0,
      zIndex: '-1',
    },
    pageView: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      display: 'block',
    },
    page: {
      flexDirection: 'row',
      backgroundColor: '#ffffff',
      margin: '5px',
      fontFamily: 'Helvetica',
    },
    headingText: {
      position: 'absolute',
      top: 22,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '20px',
      fontFamily: 'GilSansBold',
      color: '#000',
      textTransform: 'uppercase',
    },
    idTextNo: {
      position: 'absolute',
      top: 235,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      paddingLeft: '40',
      fontSize: '16px',
      fontFamily: 'GilSansMT',
      color: '#000',
    },
    idTextName: {
      position: 'absolute',
      top: 260,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      paddingLeft: '43',
      fontSize: '16px',
      fontFamily: 'GilSansMT',
      color: '#000',
    },
    idTextVFrom: {
      position: 'absolute',
      top: 285,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      paddingLeft: '68',
      fontSize: '16px',
      fontFamily: 'GilSansMT',
      color: '#000',
    },
    idTextVUpto: {
      position: 'absolute',
      top: 310,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      paddingLeft: '68',
      fontSize: '16px',
      fontFamily: 'GilSansMT',
      color: '#000',
    },

    idTextCourse: {
      position: 'absolute',
      top: 335,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      paddingLeft: '84',
      fontSize: '16px',
      fontFamily: 'GilSansMT',
      color: '#000',
    },
    idCard: {
      zIndex: '100',
      top: 48,
      left: 68,
      borderRadius: '200px',
      width: '180px',
      height: '180px',
      borderTop: 13,
      borderBottom: 13,
      borderLeft: 13,
      borderRight: 13,
      borderColor: '#000',
      borderStyle: 'solid',

    },
    pathTop: {
      position: 'absolute',
      width: '100%',
      top: 0,
      left: 0,
      zIndex: '1',
    },
    bottominfo: {
      position: 'absolute',
      top: 370,
      left: 0,
      width: '96%',
      zIndex: '1',

    },
  });

  return (
    <Document>
      <Page size="ID1" orientation="portrait" dpi="150" style={styles.body}>
        <View style={styles.pageView}>
          <Image src={headerImg} style={styles.pathTop} alt="Header image" />
          <Text style={styles.headingText}>STUDENT ID CARD</Text>
          <Text style={styles.idTextNo}>
            Student ID No:
            &nbsp;
            {program?.registration_no}
          </Text>
          <Text style={styles.idTextName}>
            Student Name:
            &nbsp;
            {program?.student?.name}
          </Text>
          <Text style={styles.idTextVFrom}>
            Valid From:
            &nbsp;
            {program?.program?.start_date && (
              dayjs(program?.program?.start_date)?.format?.('DD/MM/YYYY')
            )}
            {' '}
            {}
          </Text>
          <Text style={styles.idTextVUpto}>
            Valid Upto:
            &nbsp;
            {program?.program?.end_date && (
              dayjs(program?.program?.end_date)?.format?.('DD/MM/YYYY')
            )}
          </Text>
          <Text style={styles.idTextCourse}>
            &nbsp;
            {program?.program?.type === 'package' ? `Package: ${program?.program?.name}` : `Course: ${program?.program?.name}`}
          </Text>
          <Image src={photo ?? placeholderImg} style={styles.idCard} alt="Student photo" />
          <Image src={footerImg} style={styles.bottominfo} alt="Footer image" />
        </View>
      </Page>
    </Document>
  );
}

export default IdCardPdfTemplate;
