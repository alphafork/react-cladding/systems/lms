import React from 'react';
import { Tag } from 'primereact/tag';

function RegistrationStatus({ status }) {
  const label = `${status[0].toUpperCase?.()}${status.slice?.(1)}`;
  let severity;
  switch (status) {
    case 'applied':
      severity = 'danger';
      break;

    case 'approved':
      severity = 'warning';
      break;

    case 'completed':
      severity = 'success';
      break;

    case 'discontinued':
    case 'deregistered':
      severity = 'info';
      break;

    default:
      severity = null;
  }

  return (
    <Tag
      value={label}
      severity={severity}
    />
  );
}

export default RegistrationStatus;
