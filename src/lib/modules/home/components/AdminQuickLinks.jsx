const AdminQuickLinks = [
  {
    label: 'Courses',
    link: '/course',
  },
  {
    label: 'Packages',
    link: '/package',
  },
  {
    label: 'Batches',
    link: '/batch',
  },
  {
    label: 'Student Fees',
    link: '/student/fee',
  },
];

export default AdminQuickLinks;
