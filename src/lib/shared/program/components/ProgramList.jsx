import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import { Message } from 'primereact/message';
import { SelectButton } from 'primereact/selectbutton';
import PageHeader from '../../generic/components/PageHeader';
import List from '../../generic/components/List';
import ProgramPopup from './ProgramPopup';
import useApi from '../../../hooks/useApi';
import useAlert from '../../../hooks/useAlert';
import api from '../../../config/api';
import getConfig from '../../../utils/getConfig';

function ProgramList({ studentId, programType }) {
  const profile = useApi(api.profileStatus);
  const { pathname } = useLocation();
  const userId = studentId ?? window.localStorage.getItem('USER_ID');
  const page = programType ?? (
    pathname.includes('package') ? 'package' : 'course'
  );
  let url = page === 'package' ? api.studentPackage : api.studentCourse;
  url = `${url}/${userId}`;
  const { headerFields } = getConfig(page);
  const programs = useApi(url);
  const apply = useApi();
  const withdraw = useApi();
  const applyAlert = useAlert();
  const withdrawAlert = useAlert();
  const [programOption, setProgramOption] = useState('Not Applied');
  const [filteredPrograms, setFilteredPrograms] = useState(null);
  const [buttons, setButtons] = useState(null);
  const [activeProgram, setActiveProgram] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const title = `${page.charAt(0).toUpperCase()}${page.replace(/-/g, '').slice(1)} List`;

  useEffect(() => {
    if (profile.data === true) {
      window.localStorage.setItem('PROFILE_COMPLETED', true);
    }
    if (profile.data === false) {
      window.localStorage.setItem('PROFILE_COMPLETED', false);
    }
  }, [profile.data]);

  useEffect(() => {
    if (!programs.data?.results) {
      return;
    }

    setFilteredPrograms(programs.data);

    const onClickView = (programId) => {
      const program = programs.data?.results?.find?.((item) => (
        item.id === programId
      ));
      setActiveProgram(program);
      setShowPopup(true);

      return () => {
        setActiveProgram(null);
      };
    };

    const onClickApply = (programId) => {
      apply.sendRequest({
        url: page === 'package' ? api.packageApplication : api.courseApplication,
        method: 'POST',
        data: {
          [page]: programId,
          student: userId,
        },
      });
      setButtons((prevButtons) => ({
        ...prevButtons,
        apply: {
          ...prevButtons?.apply,
          [programId]: {
            ...prevButtons?.apply?.[programId],
            icon: 'star-fill',
            disabled: true,
            tooltip: 'Already applied',
          },
        },
      }));
    };

    const withdrawApplication = (registrationId) => {
      const withdrawUrl = `${api.studentRegistration}/${registrationId}`;
      withdraw.sendRequest({
        url: withdrawUrl,
        method: 'DELETE',
      });
    };

    const onClickWithdraw = (registrationId) => {
      confirmDialog({
        message: 'Are you sure you want to withdraw the application?',
        header: 'Withdraw Confirmation',
        icon: 'pi pi-info-circle',
        acceptClassName: 'p-button-danger',
        accept: () => withdrawApplication(registrationId),
      });
    };

    let programButtons = {};
    for (const program of programs.data.results) {
      programButtons = ({
        ...programButtons,
        view: {
          ...programButtons?.view,
          [program.id]: {
            label: 'View',
            icon: 'eye',
            severity: 'secondary',
            onClick: onClickView,
          },
        },
      });

      if (program.is_registration_open) {
        if (program.status === 'unregistered') {
          programButtons = ({
            ...programButtons,
            apply: {
              ...programButtons?.apply,
              [program.id]: {
                label: 'Apply',
                icon: 'star',
                onClick: onClickApply,
              },
            },
          });
        }
      } else {
        programButtons = ({
          ...programButtons,
          apply: {
            ...programButtons?.apply,
            [program.id]: {
              label: 'Apply',
              icon: 'star',
              disabled: true,
              tooltip: 'Registrations closed',
            },
          },
        });
      }

      if (program.status === 'applied') {
        programButtons = ({
          ...programButtons,
          apply: {
            ...programButtons?.apply,
            [program.id]: {
              label: 'Withdraw',
              icon: 'exclamation-circle',
              severity: 'danger',
              onClick: () => onClickWithdraw(program.registration_id),
            },
          },
        });
      }

      if (program.status !== 'unregistered' && program.status !== 'applied') {
        programButtons = ({
          ...programButtons,
          apply: {
            ...programButtons?.apply,
            [program.id]: {
              label: 'Apply',
              icon: 'star-fill',
              disabled: true,
              tooltip: 'Already applied',
            },
          },
        });
      }
    }
    setButtons((prevButtons) => ({
      ...prevButtons,
      ...programButtons,
    }));
  }, [programs.data?.results]);

  useEffect(() => {
    if (withdraw.status === 204) {
      withdrawAlert.setAlert('success', 'Application withdrawn successfully');
      programs.sendRequest({ url });
    }
  }, [withdraw.status]);

  const onHide = () => {
    setShowPopup(false);
    setActiveProgram(null);
  };

  useEffect(() => {
    if (apply.status === 201) {
      applyAlert.setAlert('success', `Applied for ${page} successfully, please wait for approval.`);
    }
  }, [apply.status]);

  const selectButton = (
    <SelectButton
      className="mb-4"
      value={programOption}
      onChange={(e) => setProgramOption(e.value)}
      options={['Not Applied', 'Applied']}
    />
  );

  useEffect(() => {
    if (programOption === 'Not Applied') {
      setFilteredPrograms(() => {
        const _filteredPrograms = programs.data?.results?.filter?.((program) => (
          program.status === 'unregistered'
        ));
        return { results: _filteredPrograms };
      });
    } else {
      setFilteredPrograms(() => {
        const _filteredPrograms = programs.data?.results?.filter?.((program) => (
          program.status !== 'unregistered'
        ));
        return { results: _filteredPrograms };
      });
    }
  }, [programOption, programs.data]);

  return (
    <>
      <ConfirmDialog />
      <PageHeader title={title} selectButton={selectButton} />
      {window.localStorage.getItem('PROFILE_COMPLETED') === 'false'
        ? (
          <Message
            className="p-4 mb-4"
            style={{ borer: 'solid', borderWidth: '0 0 0 5px' }}
            severity="warn"
            text="Your profile is incomplete, you need to complete your profile before applying for any course or packages!"
          />
        ) : (
          <>
            <List
              headerFields={headerFields}
              data={filteredPrograms}
              buttons={buttons}
            />
            <ProgramPopup
              header={`${title} Details`}
              visible={showPopup}
              onHide={onHide}
              program={activeProgram}
            />
            {programs.getApiAlert}
            {apply.getApiAlert}
            {applyAlert.getAlert}
            {withdrawAlert.getAlert}
          </>
        )}
    </>
  );
}

export default ProgramList;
