import React, { useEffect, useRef, useState } from 'react';
import {
  Navigate,
  Outlet,
  useLocation,
  useNavigate,
  useParams,
} from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';
import { OverlayPanel } from 'primereact/overlaypanel';
import { Avatar } from 'primereact/avatar';
import { DataView } from 'primereact/dataview';
import { Button } from 'primereact/button';
import { Badge } from 'primereact/badge';
import { Menu } from 'primereact/menu';
import { Toast } from 'primereact/toast';
import DashboardContainer, {
  TopBar, Container, Body, Footer,
} from '@ims/dashboard';
import {
  StudentSideBar,
  FacultySideBar,
  AccountantSideBar,
  ManagerSideBar,
  AdminSideBar,
} from './SideBar';
import useUserDetails from './lib/hooks/useUserDetails';
import useInstitution from './lib/hooks/useInstitution';
import useNotification from './lib/hooks/useNotification';
import useApi from './lib/hooks/useApi';
import api from './lib/config/api';
import getConfig from './lib/utils/getConfig';
import './lib/assets/sass/style.scss';
import placeholderImg from './lib/assets/images/user_photo_placeholder.png';

function Dashboard() {
  const {
    id: userId,
    role: userRole,
    username,
    firstName,
    photo,
  } = useUserDetails() ?? {};
  const {
    reloadNotifications,
    recentUnreadNotifications,
    markNotificationAsRead,
    markAllNotificationsAsRead,
  } = useNotification();
  const notificationPanel = useRef(null);
  const menu = useRef(null);
  const toast = useRef(null);
  const { pathname } = useLocation();
  const { page } = useParams();
  const navigate = useNavigate();
  const profileStatus = useApi();
  const institution = useInstitution();
  const institutionLogo = useApi();
  const staffDesignation = useApi();
  const updateStaffDesignation = useApi();
  const [staffRoles, setStaffRoles] = useState(null);
  const config = getConfig(page);

  const [menuItems, setMenuItems] = useState([
    { label: 'Profile', icon: 'pi pi-user', url: '/profile' },
    { label: 'Change Password', icon: 'pi pi-key', url: '/change-password' },
    { label: 'Logout', icon: 'pi pi-sign-out', url: '/logout' },
  ]);

  useEffect(() => {
    window.localStorage.setItem('PROFILE_COMPLETED', '');
  }, []);

  useEffect(() => {
    if (!userId) return;

    if (userRole && userRole !== 'Student') {
      const staffDesignationUrl = `${api.staffDesignation}/?user=${userId}&is_active=1`;
      staffDesignation.sendRequest({ url: staffDesignationUrl });
    }
  }, [userId, userRole]);

  useEffect(() => {
    const designations = staffDesignation.data?.results;
    if (!designations || !userRole) return;

    if (designations.length > 1) {
      const roles = designations.filter((designation) => (
        designation.designation_name !== userRole
      )).map((designation) => (
        designation.designation_name
      ));
      setStaffRoles(roles);
    }
  }, [staffDesignation.data?.results, userRole]);

  useEffect(() => {
    if (staffRoles?.length) {
      const switchRole = {
        label: 'Switch Role',
        icon: 'pi pi-sign-in',
        items: staffRoles.map((role) => ({
          label: role,
          command: () => {
            const defaultLogin = staffDesignation.data?.results?.find?.((designation) => (
              designation.designation_name === role
            ));
            defaultLogin.is_default = true;
            updateStaffDesignation.sendRequest({
              url: `${api.staffDesignation}/${defaultLogin?.id}`,
              method: 'PUT',
              data: defaultLogin,
            });
          },
        })),
      };
      setMenuItems((prevMenuItems) => (
        prevMenuItems
          .toSpliced(0, 0, switchRole)
          .toSpliced(1, 0, { separator: true })
      ));
    }
  }, [staffRoles?.length]);

  useEffect(() => {
    if (updateStaffDesignation.status === 200) {
      navigate('/home');
      window.location.reload('/');
    }
  }, [updateStaffDesignation.status]);

  const getAvatar = (
    <button
      type="button"
      className="p-link flex align-items-center ml-3 px-3 py-2 hover:surface-200"
      onClick={(e) => menu.current.toggle(e)}
    >
      <Avatar
        image={photo ?? placeholderImg}
        className="mr-2"
        shape="circle"
      />
      <div className="flex flex-column align">
        <span className="font-bold">
          {firstName || username}
        </span>
        <span className="font-sm">
          {userRole}
        </span>
      </div>
    </button>
  );

  const notificationList = (notification) => (
    <div className="w-full flex justify-content-between px-2">
      <div className="flex align-items-center justify-content-center">
        {notification.notification}
      </div>
      <div className="flex align-items-center justify-content-center">
        <Button
          icon="pi pi-check"
          rounded
          text
          aria-label="Mark as read"
          onClick={() => markNotificationAsRead(notification)}
        />
      </div>
    </div>
  );

  useEffect(() => {
    if (!userRole) return;
    if (pathname !== '/') return;
    if (userRole === 'Student') {
      const url = api.profileStatus;
      profileStatus.sendRequest({ url });
    }
    navigate('/home');
  }, [userRole, pathname, navigate, profileStatus]);

  useEffect(() => {
    if (userRole === 'Admin' || pathname !== '/') return;
    if (profileStatus.data === false) {
      window.localStorage.setItem('PROFILE_COMPLETED', false);
      navigate('/profile');
    }
  }, [userRole, profileStatus.data, navigate, pathname]);

  const getSideBar = () => {
    if (userRole === 'Faculty') {
      return (<FacultySideBar />);
    }
    if (userRole === 'Accountant') {
      return (<AccountantSideBar />);
    }
    if (userRole === 'Manager') {
      return (<ManagerSideBar />);
    }
    if (userRole === 'Admin') {
      return (<AdminSideBar />);
    }

    return (<StudentSideBar />);
  };

  useEffect(() => {
    reloadNotifications();
  }, [pathname]);

  useEffect(() => {
    if (institution?.logo) {
      institutionLogo.sendRequest({ url: institution.logo });
    }
  }, [institution?.logo]);

  return (
    <DashboardContainer>
      {!userId ? (
        <div className="flex justify-content-center m-5">
          <ProgressSpinner />
        </div>
      ) : (
        <>
          <TopBar>
            <a href="/" className="layout-topbar-logo">
              {institutionLogo.status === 200 && (
                <Avatar
                  image={institution.logo}
                  size="large"
                />
              )}
              <span className="text-xl">
                {institution?.name}
              </span>
            </a>

            <div className="layout-topbar-menu">
              <button
                type="button"
                className="p-link"
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: '50%',
                  position: 'relative',
                  top: '5px',
                  color: 'var(--text-color-secondary)',
                  display: 'inline-flex',
                  height: '3rem',
                  width: '3rem',
                  cursor: 'ponter',
                  transition: 'background-color .2s',
                }}
                onClick={(e) => notificationPanel.current.toggle(e)}
              >
                <i
                  className="pi pi-bell p-overlay-badge"
                  style={{ fontSize: '1.5rem' }}
                >
                  <Badge
                    value={recentUnreadNotifications?.length ?? 0}
                    severity="danger"
                  />
                </i>
              </button>
              <OverlayPanel className="w-3" ref={notificationPanel}>
                <DataView
                  value={recentUnreadNotifications}
                  itemTemplate={notificationList}
                />
                <div className="flex justify-content-end flex-wrap">
                  <Button
                    className="flex mt-3 ml-1"
                    icon="pi pi-check"
                    label="Mark all as read"
                    size="small"
                    onClick={markAllNotificationsAsRead}
                    disabled={recentUnreadNotifications.length === 0}
                  />
                  <Button
                    className="flex mt-3 ml-1"
                    icon="pi pi-eye"
                    label="View all"
                    size="small"
                    severity="success"
                    onClick={(e) => {
                      navigate('/notifications');
                      notificationPanel.current.toggle(e);
                    }}
                  />
                </div>
              </OverlayPanel>
              {getAvatar}
              <OverlayPanel ref={menu}>
                <Menu model={menuItems} />
              </OverlayPanel>
            </div>
          </TopBar>
          {getSideBar()}
          <Container>
            <Body>
              <div className="col-12">
                <div className="card">
                  {(page && !config.api) ? (
                    <Navigate to="/" replace />
                  ) : (
                    <Outlet />
                  )}
                </div>
              </div>
            </Body>
            <Footer />
          </Container>
          <Toast ref={toast} />
        </>
      )}
      {updateStaffDesignation.getApiAlert}
    </DashboardContainer>
  );
}

export default Dashboard;
