import React from 'react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import {
  Document,
  Image,
  Font,
  Page,
  StyleSheet,
  Text,
  View,
} from '@react-pdf/renderer';
import internshipcertbg from '../assets/images/intershipcertbg.png';
import certificateTextFont from '../assets/fonts/ErasMediumITC.ttf';
import certificateTitleFont from '../assets/fonts/NexaRegular.otf';

function InternshipCertificatePdfTemplate({ studentName, internship }) {
  dayjs.extend(utc);
  Font.register({
    family: 'ErasMediumITC',
    format: 'sans-serif',
    src: certificateTextFont,
  });

  Font.register({
    family: 'NexaRegular',
    format: 'sans-serif',
    src: certificateTitleFont,
  });

  Font.register({ family: 'Helvetica' });

  const styles = StyleSheet.create({
    body: {
      margin: 0,
      position: 'relative',
    },
    page: {
      flexDirection: 'row',
      backgroundColor: '#ffffff',
      margin: '5px',
      fontFamily: 'Helvetica',
    },
    pageBackground: {
      position: 'absolute',
      width: '100%',
      top: 0,
      zIndex: '-1',
    },
    pageView: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      display: 'block',
    },
    certificateDate: {
      position: 'absolute',
      top: 465,
      left: 100,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      fontSize: '20px',
      fontWeight: 'normal',
      fontFamily: 'ErasMediumITC',
      color: '#333',
      textTransform: 'capitalize',
    },
    certificatePlace: {
      position: 'absolute',
      top: 505,
      left: 100,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      fontSize: '20px',
      fontWeight: 'normal',
      fontFamily: 'ErasMediumITC',
      color: '#333',
      textTransform: 'capitalize',
    },
    certificateTitle: {
      position: 'absolute',
      top: 625,
      left: 450,
      display: 'block',
      textAlign: 'left',
      fontSize: '22px',
      fontFamily: 'NexaRegular',
      color: '#333',
      textTransform: 'uppercase',
    },
    certificateText1: {
      position: 'absolute',
      top: 710,
      width: '100%',
      display: 'block',
      fontSize: '20px',
      fontFamily: 'NexaRegular',
      color: '#333',
      paddingLeft: '100px',
      paddingRight: '100px',
      textAlign: 'justify',
      lineHeight: 2,
    },
    certificateText2: {
      position: 'absolute',
      top: 900,
      width: '100%',
      display: 'block',
      fontSize: '20px',
      fontFamily: 'NexaRegular',
      color: '#333',
      paddingLeft: '100px',
      paddingRight: '100px',
      textAlign: 'justify',
      lineHeight: 2,
    },
    certificateText3: {
      position: 'absolute',
      top: 1050,
      width: '100%',
      display: 'block',
      fontSize: '20px',
      fontFamily: 'NexaRegular',
      color: '#333',
      paddingLeft: '100px',
      paddingRight: '100px',
      textAlign: 'justify',
      lineHeight: 2,
    },
    thankyouText: {
      position: 'absolute',
      top: 1265,
      left: 100,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      fontSize: '20px',
      fontWeight: 'normal',
      fontFamily: 'ErasMediumITC',
      color: '#333',
      textTransform: 'capitalize',
    },
    forText: {
      position: 'absolute',
      top: 1305,
      left: 100,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      fontSize: '20px',
      fontWeight: 'normal',
      fontFamily: 'ErasMediumITC',
      color: '#333',
      textTransform: 'capitalize',
    },
    nameText: {
      position: 'absolute',
      top: 1375,
      left: 1050,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      fontSize: '20px',
      fontWeight: 'normal',
      fontFamily: 'ErasMediumITC',
      color: '#333',
      textTransform: 'capitalize',
    },
    postText: {
      position: 'absolute',
      top: 1415,
      left: 1050,
      width: '100%',
      display: 'block',
      textAlign: 'left',
      fontSize: '20px',
      fontWeight: 'normal',
      fontFamily: 'ErasMediumITC',
      color: '#333',
      textTransform: 'capitalize',
    },

    superScript: {
      fontSize: 11,
      lineHeight: 35,
      textTransform: 'lowercase',
      verticalAlign: 'super',
    },
    pSpace: {
      display: 'block',
      width: 150,
    },
  });

  const certificateText1 = `This is to certify that ${studentName} has completed internship in ${internship?.internship_title} at Synnefo Solutions. The duration of the internship was from ${dayjs(internship?.start_date).format('MMM D, YYYY')} to ${dayjs(internship?.end_date).format('MMM D, YYYY')}. During this period, the student has shown dedication, enthusiasm, and a willingness to learn.`;
  const certificateText2 = `By completing this internship, ${studentName} has gained valuable practical experience and a deeper understanding of ${internship?.internship_title} concepts. Their commitment to learning and growth has been commendable, and we are confident that they will continue to excel in their future endeavors.`;
  const certificateText3 = `We extend our best wishes to ${studentName} for their future career and thank them for their dedication and hard work during their time at Synnefo Solutions.`;

  return (
    <Document>
      <Page size="A4" orientation="portrait" dpi="150" style={styles.body}>
        <View style={styles.pageView}>
          <Text style={styles.certificateDate}>
            Date:
            {' '}
            {dayjs(internship?.date_of_issue).format('DD-MM-YYYY')}
          </Text>
          <Text style={styles.certificatePlace}>Place: Cochin</Text>
          <Text style={styles.certificateTitle}>TO WHOME IT MAY CONCERN</Text>
          <Text style={styles.certificateText1}>
            <Text style={styles.pSpace}>                    </Text>
            {certificateText1}
          </Text>
          <Text style={styles.certificateText2}>
            <Text style={styles.pSpace}>                    </Text>
            {certificateText2}
          </Text>
          <Text style={styles.certificateText3}>
            <Text style={styles.pSpace}>                    </Text>
            {certificateText3}
          </Text>
          <Text style={styles.thankyouText}>Thankyou</Text>
          <Text style={styles.forText}>For Synnefo Solutions</Text>
          <Text style={styles.nameText}>Rehna V R</Text>
          <Text style={styles.postText}>HR Manager</Text>
          <Image src={internshipcertbg} style={styles.pageBackground} alt="Certificate image background" />
        </View>
      </Page>
    </Document>
  );
}

export default InternshipCertificatePdfTemplate;
