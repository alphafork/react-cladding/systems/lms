import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import useSchema from '../../hooks/useSchema';
import useApi from '../../hooks/useApi';
import Form from './Form';
import handleFormChange from '../../utils/crud/handleFormChange';

function FormContainer({
  path,
  schemaPath,
  filterString,
  hiddenFields,
  togglePopup,
  action,
  excludedFields,
  formMessages,
  formButtons,
  onFormChange,
  onSuccess,
  authorized,
}) {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const {
    schema,
    relatedData,
    getSchemaAlert,
  } = useSchema(schemaPath ?? path, filterString, authorized);
  const [formData, setFormData] = useState(null);
  const getFields = useApi();
  const setFields = useApi();

  useEffect(() => {
    if (action !== 'create' || !schema || formData) {
      return;
    }

    // Since DRF OPTIONS response doesn't contain the default value for fields,
    // we set all boolean fields to false by default in order to avoid
    // inconsistences between the data shown in the frontend and the actual
    // data that get saved in the backend.
    const booleanFields = {};
    for (const key of Object.keys(schema)) {
      const { type } = schema[key];
      if (type === 'boolean') {
        booleanFields[key] = false;
      }
      setFormData(booleanFields);
    }
  }, [action, schema, formData]);

  useEffect(() => {
    if (action !== 'edit' || formData) {
      return;
    }
    getFields.sendRequest({ url: path });
  }, [action, path, formData]);

  useEffect(() => {
    if (getFields.data && !formData) {
      // const _formData = getFields.data;
      // if (_formData?.pk) {
      //   _formData.id = _formData.pk;
      // }
      // setFormData(_formData);
      setFormData(getFields.data);
    }
  }, [getFields.data, formData]);

  const onChange = (event) => {
    const { name } = event.target;
    const { type, native_type: nativeType } = schema[name];
    const handleChange = onFormChange ?? handleFormChange;
    handleChange(event, type, nativeType, setFormData);
  };

  const onSubmit = (event) => {
    event.preventDefault();
    const method = action === 'create' ? 'POST' : 'PUT';
    setFields.sendRequest({
      url: path,
      method,
      authorized,
      data: { ...formData, ...hiddenFields },
    });
  };

  useEffect(() => {
    const { status } = setFields;
    if (status === 200 || status === 201 || status === 204) {
      if (onSuccess) {
        onSuccess(status);
      } else {
        togglePopup();
        navigate(0);
      }
    }
    // if (setFields.status === 400 && setFields.error?.errors?.[0]?.code === 'unique') {
    //   setFields.sendRequest({ url: path, method: 'PATCH', data: formData });
    // }
  }, [setFields.status]);

  const buttons = formButtons ?? [
    {
      label: 'Submit',
      icon: 'check',
      onClick: onSubmit,
    },
  ];

  const eventHandlers = { onChange };

  return (
    <>
      <Form
        schema={schema}
        data={formData}
        relatedData={relatedData}
        excludedFields={excludedFields}
        eventHandlers={eventHandlers}
        messages={formMessages}
        buttons={buttons}
      />
      { getSchemaAlert }
      { setFields?.getApiAlert }
    </>
  );
}

export default FormContainer;
