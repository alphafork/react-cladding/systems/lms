import React from 'react';
import dayjs from 'dayjs';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ColumnGroup } from 'primereact/columngroup';
import { Row } from 'primereact/row';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function DueFee({ registrationId }) {
  const url = registrationId ? `${api.feeSplitup}/${registrationId}` : null;
  const dueFee = useApi(url);

  const getPaymentType = ({ item }) => (
    `${item.charAt(0).toUpperCase()}${item.slice(1)}`
  );

  const getDueDate = (rowData) => (
    dayjs(rowData.deu_date).format('MMM D, YYYY')
  );

  const dueFeeColumns = [
    { header: 'Item', body: getPaymentType },
    { header: 'Due date', body: getDueDate },
    { header: 'Amount', field: 'amount' },
    { header: 'Balance', field: 'balance_amount' },
  ];

  const dueFeeFooter = (
    <ColumnGroup>
      <Row>
        <Column
          footer="Total Payable Amount (INR):"
          colSpan={3}
          footerStyle={{ textAlign: 'right' }}
        />
        <Column footer={dueFee.data?.total_payable} />
      </Row>
    </ColumnGroup>
  );
  return (
    <DataTable
      footerColumnGroup={dueFeeFooter}
      value={dueFee.data?.payment_history}
      className="my-5"
    >
      {dueFeeColumns?.map((col, index) => (
        <Column
          key={col.field ?? index}
          header={col.header}
          field={col.field}
          body={col.body}
        />
      ))}
    </DataTable>
  );
}

export default DueFee;
