import React, { useEffect, useState } from 'react';
import { Chip } from 'primereact/chip';
import FormContainer from './FormContainer';
import handleFormChange from '../../utils/crud/handleFormChange';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function FeeSubscription({
  togglePopup,
  excludedFields,
  onFormSubmit,
  onFormCancel,
  visible,
}) {
  const subscriptionScheme = useApi();
  const [studentFee, setStudentFee] = useState(null);
  const [downPayment, setDownPayment] = useState(null);
  const [formMessages, setFormMessages] = useState(null);
  const [isSchemaChanged, setIsSchemaChanged] = useState(false);

  useEffect(() => {
    if (!visible) {
      setStudentFee(0);
      setIsSchemaChanged(false);
    }
  }, [visible]);

  useEffect(() => {
    const scheme = subscriptionScheme.data?.periodic_installments;
    if (!scheme) return;
    const installmentAmount = studentFee - downPayment;
    const messages = scheme.map?.(
      (installment, index) => {
        if (!Number.isNaN(studentFee)
          && studentFee > 0
          && installment != null
          && isSchemaChanged
        ) {
          return (
            <Chip
              key={index}
              label={`Rs.${(installment * installmentAmount) / 100}`}
              className="mr-1 bg-indigo-400 text-indigo-50"
            />
          );
        }
      },
    );
    setFormMessages({ scheme: messages });
  }, [subscriptionScheme.data, studentFee, downPayment, isSchemaChanged]);

  const onFormChange = (event, type, nativeType, setFormData) => {
    const { name, value } = event.target;
    handleFormChange(event, type, nativeType, setFormData);
    if (name === 'scheme') {
      subscriptionScheme.sendRequest({
        url: `${api.installmentScheme}/${value}`,
      });
      setIsSchemaChanged(true);
    }
    if (name === 'student_fee') {
      setStudentFee(value);
    }
    if (name === 'down_payment') {
      setDownPayment(value);
    }
  };

  const formButtons = [
    {
      label: 'Subscribe & Proceed to Pay',
      icon: 'check',
      onClick: onFormSubmit,
    },
    {
      label: 'Cancel',
      icon: 'times',
      onClick: onFormCancel,
      extraClasses: 'p-button-outlined',
    },
  ];

  return (
    <>
      <FormContainer
        path={api.studentFee}
        togglePopup={togglePopup}
        action="create"
        excludedFields={excludedFields}
        formButtons={formButtons}
        formMessages={formMessages}
        onFormChange={onFormChange}
      />
    </>
  );
}

export default FeeSubscription;
