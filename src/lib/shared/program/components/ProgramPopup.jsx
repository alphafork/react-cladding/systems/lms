import React from 'react';
import { Dialog } from 'primereact/dialog';
import Program from './Program';

function ProgramPopup({
  header,
  visible,
  onHide,
  program,
}) {
  return (
    <Dialog
      header={header}
      visible={visible}
      onHide={onHide}
      style={{ width: '75vw' }}
    >
      <Program program={program} />
    </Dialog>
  );
}

export default ProgramPopup;
