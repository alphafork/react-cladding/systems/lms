import React from 'react';
import { Dialog } from 'primereact/dialog';
import DetailStudent from '../students/DetailStudent';
import DueFee from './DueFee';
import FeeEntry from './FeeEntry';

function FeeEntryPopup({
  visible,
  onHide,
  student,
  onFormSubmit,
  onFormCancel,
}) {
  return (
    <Dialog
      header="Pay Installment"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <DetailStudent student={student} />
      <DueFee registrationId={student?.registration} />
      <FeeEntry
        onFormSubmit={onFormSubmit}
        onFormCancel={onFormCancel}
      />
    </Dialog>
  );
}

export default FeeEntryPopup;
