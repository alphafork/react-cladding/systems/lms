import React, { useState } from 'react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import { Tag } from 'primereact/tag';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import DetailInvoice from '../invoice/DetailInvoice';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListTransactionContainer() {
  dayjs.extend(utc);
  const transactions = useApi(api.transaction);
  const [invoiceId, setInvoiceId] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    entry: { value: null, matchMode: FilterMatchMode.EQUALS },
    account_name: { value: null, matchMode: FilterMatchMode.EQUALS },
  });
  const title = 'Transaction List';

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const togglePopup = () => {
    setShowPopup((prevShowPopup) => {
      if (prevShowPopup) setInvoiceId(null);
      return !prevShowPopup;
    });
  };

  const getPaymentType = (rowData) => {
    let label;
    let severity;
    if (rowData.entry === 'Dr') {
      label = 'Income';
      severity = 'success';
    }
    if (rowData.entry === 'Cr') {
      label = 'Expense';
      severity = 'danger';
    }

    return (
      <Tag
        value={label}
        severity={severity}
      />
    );
  };

  const getPaymentTypeFilterTemplate = (status) => (
    <Tag
      value={status.label}
      severity={status.value === 'Dr' ? 'success' : 'danger'}
    />
  );

  const getPaymentTypeFilter = (options) => (
    <Dropdown
      value={options.value}
      options={[
        {
          label: 'Income',
          value: 'Dr',
        },
        {
          label: 'Expense',
          value: 'Cr',
        },
      ]}
      itemTemplate={getPaymentTypeFilterTemplate}
      onChange={(e) => options.filterApplyCallback(e.value)}
      placeholder="Select One"
      showClear
      style={{ minWidth: '12rem' }}
    />
  );

  const getDate = (rowData) => (
    dayjs(rowData.date)?.format?.('MMM D YYYY')
  );

  const onClickViewInvoice = (id) => {
    togglePopup();
    setInvoiceId(id);
  };

  const actions = (rowData) => (
    <Button
      label="View Invoice"
      icon="pi pi-file"
      className="mr-2 px-3 py-2"
      onClick={() => onClickViewInvoice(rowData.invoice)}
    />
  );

  const columns = [
    { header: 'Title', field: 'title' },
    { header: 'Amount', field: 'amount' },
    {
      header: 'Type',
      field: 'entry',
      body: getPaymentType,
      filter: true,
      filterElement: getPaymentTypeFilter,
      showFilterMenu: false,
    },
    {
      header: 'Account',
      field: 'account_name',
    },
    { header: 'Date', body: getDate },
    { header: 'Action', body: actions },
  ];

  const pagination = {
    onClickPrev: (e, callback) => {
      if (transactions.data?.previous) {
        transactions.sendRequest({ url: transactions.data.previous });
      }
      callback(e);
    },
    onClickNext: (e, callback) => {
      if (transactions.data?.next) {
        transactions.sendRequest({ url: transactions.data.next });
      }
      callback(e);
    },
  };

  return (
    <>
      <PageHeader title={title} />
      <ListDataTable
        data={transactions.data}
        header={renderHeader}
        globalFilterFields={[
          'title',
          'amount',
        ]}
        columns={columns}
        filters={filters}
        pagination={pagination}
      />
      {invoiceId && (
        <DetailInvoice
          visible={showPopup}
          onHide={togglePopup}
          invoiceId={invoiceId}
        />
      )}
    </>
  );
}

export default ListTransactionContainer;
