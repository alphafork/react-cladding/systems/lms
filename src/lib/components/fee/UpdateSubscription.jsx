import React from 'react';
import FormContainer from '../crud/FormContainer';

function UpdateSubscription({ student, eventHandlers }) {
  return (
    <FormContainer
      path="update-subscription"
      resourceId={student?.registration}
      eventHandlers={eventHandlers}
    />
  );
}

export default UpdateSubscription;
