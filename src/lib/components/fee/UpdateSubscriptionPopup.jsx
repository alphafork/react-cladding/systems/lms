import React from 'react';
import { Dialog } from 'primereact/dialog';
import DetailStudent from '../students/DetailStudent';
import UpdateSubscription from './UpdateSubscription';

function UpdateSubscriptionPopup({
  student,
  refresh,
  visible,
  onHide,
}) {
  const eventHandlers = {
    onSuccess: () => {
      onHide?.();
      refresh?.();
    },
    onCancel: () => onHide?.(),
  };

  return (
    <Dialog
      header="Update Fee Subscription"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <DetailStudent student={student} />
      {student?.registration && (
        <UpdateSubscription
          student={student}
          eventHandlers={eventHandlers}
        />
      )}
    </Dialog>
  );
}

export default UpdateSubscriptionPopup;
