import React from 'react';
import { Dialog } from 'primereact/dialog';
import FormContainer from '../crud/FormContainer';

function AddDaySchedulePopup({
  visible,
  onHide,
  eventHandlers,
  defaultData,
  fieldAlerts,
}) {
  return (
    <Dialog
      header="Add One Day Schedule"
      visible={visible}
      style={{ width: '50vw' }}
      onHide={onHide}
    >
      <FormContainer
        path="schedule"
        eventHandlers={eventHandlers}
        defaultData={defaultData}
        fieldAlerts={fieldAlerts}
      />
    </Dialog>
  );
}

export default AddDaySchedulePopup;
