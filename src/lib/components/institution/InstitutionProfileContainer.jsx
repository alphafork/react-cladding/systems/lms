import React from 'react';
import InstitutionProfile from './InstitutionProfile';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function InstitutionProfileContainer() {
  const institutionProfile = useApi(api.institutionProfile);

  return (
    institutionProfile.data?.results?.[0] && (
      <InstitutionProfile id={institutionProfile.data.results[0].id} />
    )
  );
}

export default InstitutionProfileContainer;
