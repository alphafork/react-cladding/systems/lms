import api from '../api';
import commonProfile from './profile.common';
import multiEntryProfile from './profile.multientry';
import studentProfile from './profile.student';

const pages = {
  default: {
    headerFields: {
      titleField: 'name',
      subtitleField: 'description',
    },
    excludedFields: [
      'id',
    ],
  },
  ...commonProfile,
  ...multiEntryProfile,
  ...studentProfile,
  'institution-profile': {
    api: api.institutionProfile,
    headerFields: {
      titleField: 'name',
      subtitleField: 'short_description',
    },
  },
  'change-password': {
    api: api.changePassword,
  },
  course: {
    excludedFields: [
      'id',
      'duration_unit',
    ],
  },
  'student-registration': {
    api: api.registerStudent,
    excludedFields: [
      'username',
    ],
  },
  'update-subscription': {
    api: api.updateSubscription,
  },
  schedule: {
    api: api.schedule,
  },
  'repeating-schedule': {
    api: api.repeatingSchedule,
  },
  'daily-schedule': {
    api: api.dailySchedule,
    excludedFields: [
      'id',
      'schedule',
    ],
  },
  attendance: {
    api: api.attendance,
    excludedFields: [
      'id',
      'daily_schedule',
      'batch_student',
    ],
  },
  'exam-result': {
    api: api.examResult,
    excludedFields: [
      'id',
      'exam',
      'batch_student',
    ],
  },
  'due-extension': {
    api: api.dueExtension,
    excludedFields: [
      'id',
      'registration',
    ],
  },
  'operation-log': {
    api: api.operationLog,
    headerFields: {
      titleField: 'operation_name',
    },
  },
  staff: {
    api: api.staffDesignation,
    headerFields: {
      titleField: 'staff',
      subtitleField: 'designation',
    },
    excludedFields: [
      'id',
      'user',
    ],
  },
  'staff-registration': {
    api: api.staffRegistration,
  },
  'staff-designation': {
    api: api.staffDesignation,
    headerFields: {
      titleField: 'name',
      subtitleField: 'designation_name',
    },
  },
  'staff-permanent-designation': {
    api: api.staffPermanentDesignation,
  },
  'staff-temporary-designation': {
    api: api.staffTemporaryDesignation,
  },
  'staff-leave': {
    api: api.staffLeave,
    headerFields: {
      titleField: 'staff_name',
      subtitleField: 'leave_type_name',
    },
  },
  certificate: {
    api: api.certificate,
    excludedFields: [
      'registration',
      'serial_number',
    ],
  },
  internship: {
    api: api.internship,
    excludedFields: [
      'registration',
      'serial_number',
    ],
  },
  referral: {
    api: api.referral,
    excludedFields: [
      'registration',
      'referrer',
    ],
  },
  referrer: {
    api: api.referrer,
    headerFields: {
      titleField: 'phone_no',
      subtitleField: 'email',
    },
  },
  'im-user': {
    api: api.imUser,
    excludedFields: [
      'user',
      'im_userid',
      'im_roomid',
      'response_log',
    ],
  },
};

export default pages;
