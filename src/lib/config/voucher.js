const voucher = {
  create: {
    excludedFields: [
      'id',
      'payment_splitup',
    ],
  },
};

export default voucher;
