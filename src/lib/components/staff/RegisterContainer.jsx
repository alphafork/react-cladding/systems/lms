import React from 'react';
import { useNavigate } from 'react-router-dom';
import PageHeader from '../PageHeader';
import FormContainer from '../crud/FormContainer';

function RegisterContainer() {
  const navigate = useNavigate();
  const eventHandlers = {
    onSuccess: (statusCode) => {
      if (statusCode === 201) navigate('/staff');
    },
    onCancel: () => navigate('/staff'),
  };

  return (
    <>
      <PageHeader title="Add Staff" />
      <FormContainer
        path="staff-registration"
        eventHandlers={eventHandlers}
      />
    </>
  );
}

export default RegisterContainer;
