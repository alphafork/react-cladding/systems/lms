import React from 'react';
import { pdf } from '@react-pdf/renderer';
import { saveAs } from 'file-saver';
import dayjs from 'dayjs';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import InvoicePdfTemplate from './InvoicePdfTemplate';
import useFieldParser from '../../hooks/useFieldParser';
import useApi from '../../hooks/useApi';
import api from '../../config/api';
import getConfig from '../../utils/getConfig';

function InvoiceDetail({
  visible,
  onHide,
  invoiceId,
  buttons,
}) {
  const invoice = useApi(`${api.invoice}/${invoiceId}`);
  const config = getConfig('invoice');
  const {
    displayData,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, invoiceId);

  const downloadInvoicePdf = async () => {
    if (invoice.data) {
      const fileName = `invoice-${invoiceId}-${dayjs().format('YYYYMMDD-HHmmss')}.pdf`;
      const blob = await pdf((
        <InvoicePdfTemplate invoice={invoice} />
      )).toBlob();
      saveAs(blob, fileName);
    }
  };

  const invoiceButtons = buttons ?? [
    {
      label: 'Print',
      icon: 'print',
      onClick: () => downloadInvoicePdf(invoiceId),
    },
  ];

  return (
    <Dialog
      header="Installment Invoice Details"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <PageHeader buttons={invoiceButtons} />
      {invoiceId && (
        <Detail
          headerFields={displayData?.headerFields}
          fields={displayData?.fields}
          showBlankFields
        />
      )}
      { getSchemaAlert }
      { getApiAlert }
    </Dialog>
  );
}

export default InvoiceDetail;
