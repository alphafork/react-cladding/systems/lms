import React from 'react';
import { Link } from 'react-router-dom';

function AdminSideBar() {
  return (
    <div className="layout-sidebar">
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Dashboard</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/home">
                <i className="layout-menuitem-icon pi pi-fw pi-home" />
                <span className="layout-menuitem-text">Home</span>
              </Link>
            </li>
          </ul>
          <div className="layout-menuitem-root-text">ACADEMIC</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/course">
                <i className="layout-menuitem-icon pi pi-fw pi-list" />
                <span className="layout-menuitem-text">Courses</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/package">
                <i className="layout-menuitem-icon pi pi-fw pi-briefcase" />
                <span className="layout-menuitem-text">Packages</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/subject">
                <i className="layout-menuitem-icon pi pi-fw pi-book" />
                <span className="layout-menuitem-text">Subjects</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/batch">
                <i className="layout-menuitem-icon pi pi-fw pi-inbox" />
                <span className="layout-menuitem-text">Batches</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/room">
                <i className="layout-menuitem-icon pi pi-fw pi-building" />
                <span className="layout-menuitem-text">Rooms</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/exam">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Exams</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">STUDENTS</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/all">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">All</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/applied">
                <i className="layout-menuitem-icon pi pi-fw pi-hourglass" />
                <span className="layout-menuitem-text">Applied</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/approved">
                <i className="layout-menuitem-icon pi pi-fw pi-check-circle" />
                <span className="layout-menuitem-text">Approved</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/alumni">
                <i className="layout-menuitem-icon pi pi-fw pi-star" />
                <span className="layout-menuitem-text">Alumni</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/register">
                <i className="layout-menuitem-icon pi pi-fw pi-user-plus" />
                <span className="layout-menuitem-text">New Student Registration</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">ACCOUNTING</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/daily-operation">
                <i className="layout-menuitem-icon pi pi-fw pi-replay" />
                <span className="layout-menuitem-text">Daily Accounting Operation</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/fee">
                <i className="layout-menuitem-icon pi pi-fw pi-money-bill" />
                <span className="layout-menuitem-text">Student Fees</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/transaction">
                <i className="layout-menuitem-icon pi pi-fw pi-arrow-right-arrow-left" />
                <span className="layout-menuitem-text">Transactions</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/invoice">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Invoices</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/voucher">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Vouchers</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/installment-scheme">
                <i className="layout-menuitem-icon pi pi-fw pi-table" />
                <span className="layout-menuitem-text">Installment Schemes</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/transaction-account">
                <i className="layout-menuitem-icon pi pi-fw pi-wallet" />
                <span className="layout-menuitem-text">Transaction Accounts</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/finance-account">
                <i className="layout-menuitem-icon pi pi-fw pi-book" />
                <span className="layout-menuitem-text">All Accounts</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">STAFF</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/staff/register">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">New Staff Registration</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/staff/permanent">
                <i className="layout-menuitem-icon pi pi-fw pi-id-card" />
                <span className="layout-menuitem-text">Grant Permanent Designation</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/staff/temporary">
                <i className="layout-menuitem-icon pi pi-fw pi-ticket" />
                <span className="layout-menuitem-text">Grant Temporary Designation</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/staff">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">Staff Management</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/staff-leave">
                <i className="layout-menuitem-icon pi pi-fw pi-user-minus" />
                <span className="layout-menuitem-text">Leave</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Referrals</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/referrer">
                <i className="layout-menuitem-icon pi pi-directions" />
                <span className="layout-menuitem-text">Referrers</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">CONFIGURATION</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/institution-profile">
                <i className="layout-menuitem-icon pi pi-fw pi-building" />
                <span className="layout-menuitem-text">Institution Profile</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/user-gender">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">Gender</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/user-honorific">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">Honorific</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student-relationship">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">Relationship</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/staff-leave-type">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">Staff Leave Type</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student-leave-reason">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">Student Leave Reason</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/room-type">
                <i className="layout-menuitem-icon pi pi-fw pi-building" />
                <span className="layout-menuitem-text">Room Type</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/exam-type">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Exam Type</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/transaction-account-type">
                <i className="layout-menuitem-icon pi pi-fw pi-wallet" />
                <span className="layout-menuitem-text">Transaction Account Type</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/transaction-account-category">
                <i className="layout-menuitem-icon pi pi-fw pi-wallet" />
                <span className="layout-menuitem-text">Transaction Account Category</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/transaction-account-tag">
                <i className="layout-menuitem-icon pi pi-fw pi-wallet" />
                <span className="layout-menuitem-text">Transaction Account Tag</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/operation">
                <i className="layout-menuitem-icon pi pi-fw pi-play" />
                <span className="layout-menuitem-text">Operations</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/operation-log">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Operations Log</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/permission/group">
                <i className="layout-menuitem-icon pi pi-fw pi-unlock" />
                <span className="layout-menuitem-text">Group Permissions</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  );
}

export default AdminSideBar;
