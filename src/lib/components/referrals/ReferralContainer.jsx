import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import PageHeader from '../PageHeader';
import FormContainer from '../crud/FormContainer';
import DetailContainer from './DetailContainer';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ReferralContainer() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const { studentId, registrationId } = state ?? {};
  const referral = useApi();
  const updateReferral = useApi();
  const referrer = useApi();
  const [referrerPhone, setReferrerPhone] = useState('');
  const [showPopup, setShowPopup] = useState({
    referrerDetail: false,
    referrerForm: false,
  });

  useEffect(() => {
    if (!registrationId) {
      navigate('/');
    }
    const url = `${api.referral}/${registrationId}`;
    referral.sendRequest({ url });
  }, [registrationId]);

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const handleReferrer = (event) => {
    event.preventDefault();
    const url = `${api.referrer}?phone_no=${referrerPhone}`;
    referrer.sendRequest({ url });
  };

  useEffect(() => {
    if (!referrer.data?.results) return;

    if (referrer.data?.results?.[0]) {
      togglePopup('referrerDetail');
    } else {
      confirmDialog({
        message: 'No records found, would you like to create a new referrer instead?',
        header: 'New Referrer Confirmation',
        icon: 'pi pi-info-circle',
        acceptClassName: 'p-button-danger',
        accept: () => togglePopup('referrerForm'),
      });
    }
  }, [referrer.data?.results]);

  const handleReferral = () => {
    updateReferral.sendRequest({
      url: api.referral,
      method: 'POST',
      data: {
        registration: registrationId,
        referrer: referrer.data?.results?.[0]?.id,
        date: dayjs().format('YYYY-MM-DD'),
        is_paid: 0,
      },
    });
  };

  useEffect(() => {
    if (updateReferral.status === 201) {
      const url = `${api.referral}/${registrationId}`;
      referral.sendRequest({ url });
      togglePopup('referrerDetail');
    }
  }, [updateReferral.status]);

  const buttons = [
    {
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate(`/student/${studentId}`),
      extraClasses: 'p-button-outlined',
    },
  ];

  const eventHandlers = {
    onSuccess: () => {
      togglePopup('referrerForm');
      const url = `${api.referrer}?phone_no=${referrerPhone}`;
      referrer.sendRequest({ url });
    },
    onCancel: () => togglePopup('referrerForm'),
  };

  return (
    <>
      <PageHeader title="Referral Management" buttons={buttons} />
      {referral.status === 404 && (
        <form>
          <div className="flex flex-column gap-2">
            <label
              htmlFor="phone"
              className="block text-900 font-medium mt-4 mb-2"
            >
              Phone number of referrer (10 digits)
            </label>
            <InputText
              value={referrerPhone}
              onChange={(e) => setReferrerPhone(e.target.value)}
              className="w-3"
            />
            <div className="mt-2">
              <Button
                type="submit"
                label="Check"
                className="mr-2 mb-2"
                onClick={handleReferrer}
                disabled={referrerPhone.length !== 10}
              />
            </div>
          </div>
        </form>
      )}

      {referral.status === 200 && (
        <DetailContainer id={referral.data.referrer} />
      )}

      <Dialog
        header="Referrer Details"
        visible={showPopup.referrerDetail}
        style={{ width: '50vw' }}
        onHide={() => togglePopup('referrerDetail')}
      >
        <>
          <div className="surface-0 shadow-2 p-4 mt-2 border-round">
            <ul className="list-none p-0 m-0">
              <li className="flex flex-wrap align-items-center px-2 py-3 border-300 border-bottom-1">
                <div className="text-500 w-4 font-medium">Name</div>
                <div className="text-900 w-8 md:flex-order-0 flex-order-1">
                  {referrer.data?.results?.[0]?.name}
                </div>
              </li>
              <li className="flex flex-wrap align-items-center px-2 py-3 border-300 border-bottom-1">
                <div className="text-500 w-4 font-medium">Email</div>
                <div className="text-900 w-8 md:flex-order-0 flex-order-1">
                  {referrer.data?.results?.[0]?.email}
                </div>
              </li>
            </ul>
          </div>
          <Button
            label="Add as Referrer"
            className="mt-4"
            onClick={handleReferral}
          />
        </>
      </Dialog>

      <ConfirmDialog />
      <Dialog
        header="Add Referrer"
        visible={showPopup.referrerForm}
        style={{ width: '75vw' }}
        onHide={() => togglePopup('referrerForm')}
      >
        <FormContainer
          path="referrer"
          defaultData={{ phone_no: referrerPhone }}
          eventHandlers={eventHandlers}
        />
      </Dialog>

      {updateReferral.getApiAlert}
    </>
  );
}

export default ReferralContainer;
