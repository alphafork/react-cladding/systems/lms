import { useCallback, useEffect } from 'react';
import useStore from './useStore';
import useApi from './useApi';
import api from '../config/api';

function useNotification() {
  const userNotifications = useApi(api.userNotification);
  const updateUserNotification = useApi();
  const notifications = useStore((state) => state.notifications);
  const setNotifications = useStore((state) => state.setNotifications);

  const reloadNotifications = useCallback(() => {
    userNotifications.sendRequest({ url: api.userNotification });
  }, [userNotifications]);

  useEffect(() => {
    if (userNotifications.data?.results) {
      setNotifications(userNotifications.data.results.reverse());
    }
  }, [userNotifications.data?.results]);

  const getRecentUnreadNotifications = () => {
    const unreadNotifications = notifications?.filter?.((notification) => (
      notification.is_read === false
    )) ?? [];
    const recentUnreadNotifications = unreadNotifications.slice(0, 10);
    return recentUnreadNotifications;
  };

  const markNotificationAsRead = (notification) => {
    const updateUserNotificationURL = `${api.userNotification}/${notification.id}`;
    const notificationData = notification;
    notificationData.is_read = true;
    updateUserNotification.sendRequest({
      url: updateUserNotificationURL,
      method: 'PUT',
      data: notificationData,
    });
  };

  useEffect(() => {
    if (updateUserNotification.status === 200) {
      reloadNotifications();
    }
  }, [updateUserNotification.status]);

  const markAllNotificationsAsRead = () => {
    notifications?.map?.(
      (notification) => (markNotificationAsRead(notification)),
    );
  };

  return ({
    notifications,
    reloadNotifications,
    markNotificationAsRead,
    markAllNotificationsAsRead,
    recentUnreadNotifications: getRecentUnreadNotifications(),
  });
}

export default useNotification;
