import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import useApi from '../../../hooks/useApi';
import {
  PageHeader,
  List,
} from '../../../submodules';
import getConfig from '../../../utils/getConfig';

function ListContainer() {
  const navigate = useNavigate();
  const { page: path } = useParams();
  const { api, headerFields } = getConfig(path);
  const { data, getApiAlert } = useApi(api);
  const [bodyButtons, setBodyButtons] = useState(null);
  const title = `${path[0].toUpperCase?.()}${path.replace(/-/g, ' ').slice?.(1)} list`;

  const headerButtons = [
    {
      label: 'Add',
      icon: 'plus',
      onClick: () => navigate(`/${path}/create`),
    },
  ];

  useEffect(() => {
    if (!data?.results) {
      return;
    }

    let buttons = {};
    for (const item of data.results) {
      buttons = {
        ...buttons,
        view: {
          ...buttons?.view,
          [item.id]: {
            label: 'View',
            icon: 'eye',
            severity: 'secondary',
            onClick: (id) => (navigate(`/${path}/${id}`)),
          },
        },
      };
    }
    setBodyButtons(buttons);

    return () => {
      setBodyButtons(null);
    };
  }, [data?.results, path, navigate]);

  return (
    <>
      <PageHeader title={title} buttons={headerButtons} />
      <List
        headerFields={headerFields}
        data={data}
        buttons={bodyButtons}
      />
      { getApiAlert }
    </>
  );
}

export default ListContainer;
