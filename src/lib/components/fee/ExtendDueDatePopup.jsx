import React from 'react';
import { Dialog } from 'primereact/dialog';
import DetailStudent from '../students/DetailStudent';
import ExtendDueDate from './ExtendDueDate';

function ExtendDueDatePopup({
  student,
  refresh,
  visible,
  onHide,
}) {
  const eventHandlers = {
    onSuccess: () => {
      onHide?.();
      refresh?.();
    },
    onCancel: () => onHide?.(),
  };

  return (
    <Dialog
      header="Extend Due Date"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <DetailStudent student={student} />
      {student?.registration && (
        <ExtendDueDate
          student={student}
          eventHandlers={eventHandlers}
        />
      )}
    </Dialog>
  );
}

export default ExtendDueDatePopup;
