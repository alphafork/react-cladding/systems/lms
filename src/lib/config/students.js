const students = {
  applied: {
    excludedFields: [
      'id',
      'created_at',
      'updated_at',
      'registration',
    ],
  },
  approved: {
    excludedFields: [
      'id',
      'registration',
    ],
  },
};

export default students;
