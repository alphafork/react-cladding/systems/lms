import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Tag } from 'primereact/tag';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import FeeManagementActions from './FeeManagementActions';
import useApi from '../../hooks/useApi';
import useSchema from '../../hooks/useSchema';
import useAlert from '../../hooks/useAlert';
import api from '../../config/api';

function FeeListContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const { slug: path = 'all' } = useParams();
  const students = useApi();
  const subscriptionAlert = useAlert();
  const { schema } = useSchema(api.installmentSubscription);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    payment_status: { value: null, matchMode: FilterMatchMode.EQUALS },
    subscription_status: { value: null, matchMode: FilterMatchMode.EQUALS },
  });

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const getStatusSeverity = (status, statusType) => {
    if (statusType === 'payment_status') {
      switch (status) {
        case 'due':
          return 'warning';

        case 'paid':
          return 'success';

        case 'extension':
          return 'info';

        case 'overdue':
          return 'danger';

        default:
          return null;
      }
    }

    if (statusType === 'subscription_status') {
      switch (status) {
        case 'not started':
          return 'warning';

        case 'active':
          return 'success';

        case 'paused':
          return 'info';

        case 'unrecoverable':
          return 'danger';

        default:
          return null;
      }
    }
  };

  const getStatusElement = (status, statusType) => (
    <Tag
      value={status}
      severity={getStatusSeverity(status, statusType)}
    />
  );

  const getStatusBody = (rowData, statusType) => (
    getStatusElement(rowData[statusType], statusType)
  );

  const getStatusRowFilter = (options, statusType) => {
    const statuses = schema?.[statusType]?.choices.map((choice) => choice.display_name);
    return (
      <Dropdown
        value={options.value}
        options={statuses}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={(status) => getStatusElement(status, statusType)}
        placeholder="Select One"
        className="p-column-filter"
        showClear
        style={{ minWidth: '12rem' }}
      />
    );
  };

  const getStudents = (status) => {
    let url;
    if (status === 'all') {
      url = api.feeInstallment;
    } else {
      navigate('/');
    }
    students.sendRequest({ url });
  };

  const getDueDate = (rowData) => {
    const { due_date: due } = rowData;
    if (!Number.isNaN(new Date(due).getTime())) {
      return dayjs(due).format('MMM D, YYYY');
    }

    return due;
  };

  useEffect(() => {
    getStudents(path);
  }, [path]);

  const columns = [
    { header: 'Registration No', field: 'registration.number' },
    { header: 'Student', field: 'student.name' },
    {
      header: 'Payment status',
      field: 'payment_status',
      body: (rowData) => getStatusBody(rowData, 'payment_status'),
      filter: true,
      filterElement: (options) => getStatusRowFilter(options, 'payment_status'),
      showFilterMenu: false,
    },
    {
      header: 'Subscription status',
      field: 'subscription_status',
      body: (rowData) => getStatusBody(rowData, 'subscription_status'),
      filter: true,
      filterElement: (options) => getStatusRowFilter(options, 'subscription_status'),
      showFilterMenu: false,
    },
    { header: 'Due Date', body: getDueDate },
  ];

  const pagination = {
    onClickPrev: (e, callback) => {
      students.data?.previous && students.sendRequest({ url: students.data.previous });
      callback(e);
    },
    onClickNext: (e, callback) => {
      students.data?.next && students.sendRequest({ url: students.data.next });
      callback(e);
    },
  };

  const actions = (rowData) => {
    const student = {
      ...rowData?.student,
      registration: rowData?.registration.id,
      registration_no: rowData?.registration.number,
      subscriptionStatus: rowData?.subscription_status,
    };

    return (
      <FeeManagementActions
        student={student}
        onSuccess={() => getStudents(path)}
        setAlert={subscriptionAlert.setAlert}
      />
    );
  };

  columns.push({
    header: 'Actions',
    body: actions,
  });

  return (
    <>
      <PageHeader title="Student List" />
      <ListDataTable
        data={students?.data}
        header={renderHeader}
        globalFilterFields={[
          'registration.number',
          'student.name',
          'balance_amount',
          'status',
        ]}
        columns={columns}
        filters={filters}
        pagination={pagination}
      />
      { subscriptionAlert.getAlert }
    </>
  );
}

export default FeeListContainer;
