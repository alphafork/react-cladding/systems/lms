import React from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';

function Program({ program }) {
  return (
    !program ? (
      <div className="flex justify-content-center">
        <ProgressSpinner />
      </div>
    ) : (
      <div className="surface-0 shadow-2 p-4 mt-2 mb-5 mx-5 border-round">
        <div className="surface-0">
          <div className="flex align-items-start flex-column lg:justify-content-between lg:flex-row">
            <div>
              <div className="font-medium text-2xl text-900 mb-2">{program?.name}</div>
              <div className="text-500 mb-5">{program?.description}</div>
            </div>
          </div>
          <ul className="list-none p-0 m-0">
            <li className="flex align-items-center py-3 px-2 border-top-1 border-300 flex-wrap">
              <div className="text-500 w-4 font-medium">
                {'package_code' in program ? 'Program Code' : 'Course Code'}
              </div>
              <div className="text-900 w-8 md:flex-order-0 flex-order-1">
                {program?.package_code ?? program?.course_code}
              </div>
            </li>
            <li className="flex align-items-center py-3 px-2 border-top-1 border-300 flex-wrap">
              <div className="text-500 w-4 font-medium">Duration in days</div>
              <div className="text-900 w-8 md:flex-order-0 flex-order-1">
                {program?.duration_days}
              </div>
            </li>
            <li className="flex align-items-center py-3 px-2 border-top-1 border-300 flex-wrap">
              <div className="text-500 w-4 font-medium">Fee</div>
              <div className="text-900 w-8 md:flex-order-0 flex-order-1">
                ₹
                {program?.fee}
              </div>
            </li>
          </ul>
        </div>
      </div>
    )
  );
}

export default Program;
