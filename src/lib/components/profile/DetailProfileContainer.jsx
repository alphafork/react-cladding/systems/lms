import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Message } from 'primereact/message';
import DetailProfile from './DetailProfile';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function DetailProfileContainer() {
  const { data: profileStatus } = useApi(api.profileStatus);
  const { state } = useLocation();
  const userId = state?.userId ?? window.localStorage.getItem('USER_ID');

  useEffect(() => {
    if (profileStatus === true) {
      window.localStorage.setItem('PROFILE_COMPLETED', true);
    }
  }, [profileStatus]);

  return (
    <>
      {window.localStorage.getItem('PROFILE_COMPLETED') === 'false' && (
        <Message
          className="p-4 mb-4"
          style={{ borer: 'solid', borderWidth: '0 0 0 5px' }}
          severity="warn"
          text="Welcome. Your profile is incomplete, you need to complete your profile before applying for any courses!"
        />
      )}
      <DetailProfile userId={userId} />
    </>
  );
}

export default DetailProfileContainer;
