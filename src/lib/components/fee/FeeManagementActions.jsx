import React, { useEffect, useState } from 'react';
import { SplitButton } from 'primereact/splitbutton';
import FeePayment from './FeePayment';
import UpdateSubscriptionPopup from './UpdateSubscriptionPopup';
import ExtendDueDatePopup from './ExtendDueDatePopup';
import useApi from '../../hooks/useApi';
import useAlert from '../../hooks/useAlert';
import api from '../../config/api';

function FeeManagementActions({
  student,
  onSuccess,
  setAlert,
}) {
  const subscription = useApi();
  const subscriptionAlert = useAlert();
  const [showPopup, setShowPopup] = useState({
    paymentForm: false,
    subscriptionForm: false,
    dueExtensionForm: false,
  });

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const onClickStartSubscription = () => {
    const subscriptionUrl = `${api.startInstallment}/${student?.registration}`;
    subscription.sendRequest({ url: subscriptionUrl });
  };

  useEffect(() => {
    if (subscription.status === 200) {
      const message = 'Installment subscription started successfully.';
      if (setAlert) {
        setAlert('success', message);
      } else {
        subscriptionAlert.setAlert('success', message);
      }
      onSuccess?.();
    }
  }, [subscription.status, subscriptionAlert, onSuccess]);

  const onClickUpdateSubscription = () => {
    togglePopup('subscriptionForm');
  };

  const onClickExtendDueDate = () => {
    togglePopup('dueExtensionForm');
  };

  const subscriptionActions = () => ([
    {
      label: 'Start Subscription',
      icon: 'pi pi-check-circle',
      command: () => onClickStartSubscription(),
      disabled: student?.subscriptionStatus !== 'not started',
    },
    {
      label: 'Update Subscription',
      icon: 'pi pi-pencil',
      command: () => onClickUpdateSubscription(),
      disabled: student?.subscriptionStatus === 'not started',
    },
    {
      label: 'Extend Due Date',
      icon: 'pi pi-calendar-plus',
      command: () => onClickExtendDueDate(),
      disabled: student?.subscriptionStatus === 'not started',
    },
  ]);

  const onClickPay = () => {
    togglePopup('paymentForm');
  };

  return (
    <>
      <SplitButton
        label="Pay"
        icon="pi pi-money-bill"
        onClick={() => onClickPay()}
        model={subscriptionActions()}
      />
      <FeePayment
        student={student}
        visible={showPopup.paymentForm}
        togglePaymentPopup={() => togglePopup('paymentForm')}
        onSuccess={onSuccess}
      />
      <UpdateSubscriptionPopup
        student={student}
        visible={showPopup.subscriptionForm}
        onHide={() => togglePopup('subscriptionForm')}
      />
      <ExtendDueDatePopup
        student={student}
        visible={showPopup.dueExtensionForm}
        onHide={() => togglePopup('dueExtensionForm')}
      />
      { subscriptionAlert.getAlert }
    </>
  );
}

export default FeeManagementActions;
