import React from 'react';
import { useNavigate } from 'react-router-dom';
import PageHeader from '../PageHeader';
import FormContainer from './FormContainer';
import getConfig from '../../utils/getConfig';

function RegistrationContainer({
  header = true,
  eventHandlers,
}) {
  const navigate = useNavigate();
  const { api, excludedFields } = getConfig('student-registration');
  const { onSuccess } = eventHandlers ?? {
    onSuccess: (status) => {
      if (status === 204) {
        navigate('/student/academic/all');
      }
    },
  };

  return (
    <>
      {header && (
        <PageHeader title="Add Student" />
      )}
      <FormContainer
        path={api}
        excludedFields={excludedFields}
        action="create"
        onSuccess={onSuccess}
        authorized={false}
      />
    </>
  );
}

export default RegistrationContainer;
