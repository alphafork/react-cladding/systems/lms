import React from 'react';
import { useParams } from 'react-router-dom';
import {
  PageHeader,
  FormContainer,
} from '../../../submodules';

function UpdateContainer() {
  const { page: path, id: resourceId } = useParams();
  const title = `Update ${path.replace(/-/g, ' ')}`;

  return (
    <>
      <PageHeader title={title} />
      <FormContainer path={path} resourceId={resourceId} />
    </>
  );
}

export default UpdateContainer;
