import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Chip } from 'primereact/chip';
import { Checkbox } from 'primereact/checkbox';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import PageHeader from '../PageHeader';
import DetailStudentPopup from '../students/DetailStudentPopup';
import ListDataTable from '../datatable/ListDataTable';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListMigrationContainer() {
  const navigate = useNavigate();
  const { id: batchId } = useParams();
  if (Number.isNaN(batchId)) navigate('/');
  const currentBatch = useApi(`${api.batch}/${batchId}`);
  const batchStudentsUrl = `${api.batchStudent}?filter_by={"batch":${batchId}}`;
  const batchStudents = useApi(batchStudentsUrl);
  const studentDetail = useApi();
  const programsUrl = `${api.batchPackages}?batch_id=${batchId}`;
  const programs = useApi(programsUrl);
  const batches = useApi();
  const batchMigration = useApi();
  const [migration, setMigration] = useState(null);
  const [program, setProgram] = useState(null);
  const [batch, setBatch] = useState(null);
  const [programOptions, setProgramOptions] = useState([]);
  const [batchOptions, setBatchOptions] = useState([]);

  const [showPopup, setShowPopup] = useState({
    studentDetail: false,
    form: false,
  });

  let title = 'Batch Migration';
  if (currentBatch.data?.name) title += ` - ${currentBatch.data.name}`;

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  useEffect(() => {
    const options = programs.data?.results?.map?.((option) => ({
      label: option.name,
      value: option.id,
    }));

    if (options?.length > 0) {
      setProgramOptions(options);
    }
  }, [programs.data?.results]);

  useEffect(() => {
    const options = batches.data?.results
      ?.filter?.((option) => (
        Number(option.id) !== Number(batchId)
      ))
      ?.map?.((option) => ({
        label: option.name,
        value: option.id,
      }));

    if (options?.length > 0) {
      setBatchOptions(options);
    }
  }, [batches.data?.results]);

  const resetMigration = (students) => {
    let initialMigration = {};
    for (const batchStudent of students) {
      initialMigration = {
        ...initialMigration,
        [batchStudent.registration.number]: {
          batchStudent: batchStudent.id,
          migrate: false,
        },
      };
    }
    setMigration(initialMigration);
  };

  useEffect(() => {
    if (batchStudents.data?.results) {
      resetMigration(batchStudents.data?.results);
    }
  }, [batchStudents.data?.results]);

  const onClickStudent = (userId) => {
    const _url = `${api.userAccount}/${userId}`;
    studentDetail.sendRequest({ url: _url });
    togglePopup('studentDetail');
  };

  const getStudent = (rowData) => (
    <Chip
      label={rowData.student.name}
      className="cursor-pointer bg-indigo-400 text-indigo-50"
      onClick={() => onClickStudent(rowData.student.id)}
    />
  );

  const getMigration = (rowData) => {
    const {
      registration: {
        number: registration,
      },
    } = rowData;

    const handleChange = (event) => {
      setMigration((prevMigration) => ({
        ...prevMigration,
        [registration]: {
          ...prevMigration[registration],
          migrate: event.checked,
        },
      }));
    };

    return (
      <Checkbox
        checked={migration?.[registration]?.migrate}
        onChange={handleChange}
      />
    );
  };

  const columns = [
    { header: 'Registration No', field: 'registration.number' },
    { header: 'Student', body: getStudent },
    { header: 'Email', field: 'student.email' },
    { header: 'Migrate', body: getMigration },
  ];

  const handleMigration = () => {
    const formData = {
      batch_student_migration: Object.values(migration).filter((value) => (
        value.migrate === true
      )).map((value) => ({
        batch_student: value.batchStudent,
        new_batch: batch,
      })),
    };
    batchMigration.sendRequest({
      url: api.batchMigration,
      method: 'POST',
      data: formData,
    });
  };

  useEffect(() => {
    if (batchMigration.status === 201) {
      navigate('/batch');
    }
  }, [batchMigration.status]);

  const handleProgramChange = (event) => {
    setProgram(event.value);
    batchStudents.sendRequest({ url: batchStudentsUrl });
    const batchesUrl = `${api.packageBatches}?package_id=${event.value}`;
    batches.sendRequest({ url: batchesUrl });
  };

  const handleBatchChange = (event) => {
    resetMigration(batchStudents.data?.results);
    setBatch(event.value);
  };

  const isSelectable = (data) => (data.status === 'ac');
  const rowClassName = (data) => (isSelectable(data) ? '' : 'p-disabled');
  const isStudentSelectable = (event) => (event.data ? isSelectable(event.data) : true);

  return (
    <>
      <PageHeader title={title} />
      <Dropdown
        type="choices"
        value={program}
        options={programOptions}
        onChange={handleProgramChange}
        placeholder="Select Package"
        className="w-5 my-4 mr-4"
      />
      <Dropdown
        type="choices"
        value={batch}
        options={batchOptions}
        onChange={(e) => handleBatchChange(e)}
        placeholder="Select New Batch"
        className="w-5 my-4 mr-4"
      />
      <DetailStudentPopup
        visible={showPopup.studentDetail}
        onHide={() => togglePopup('studentDetail')}
        student={studentDetail.data}
      />
      {program && (
        <>
          <ListDataTable
            data={batchStudents.data}
            isDataSelectable={isStudentSelectable}
            rowClassName={rowClassName}
            columns={columns}
          />
          {batch && (
            <Button
              className="mt-4"
              onClick={handleMigration}
            >
              Migrate
            </Button>
          )}
        </>
      )}
      {batchMigration.getApiAlert}
    </>
  );
}

export default ListMigrationContainer;
