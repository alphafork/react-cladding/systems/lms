import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import useFieldParser from '../../hooks/useFieldParser';
import getConfig from '../../utils/getConfig';

function DetailContainer() {
  const path = 'operation';
  const { id } = useParams();
  const navigate = useNavigate();
  const config = getConfig(path);
  const {
    displayData,
    status,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, id);
  const title = `${path.charAt(0).toUpperCase()}${path.replace(/-/g, ' ').slice(1)} details`;

  useEffect(() => {
    if (status === 404) {
      navigate('/');
    }
  }, [status, navigate]);

  const buttons = [
    {
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate(`/${path}`),
      extraClasses: 'p-button-outlined',
    },
  ];

  return (
    <>
      <PageHeader title={title} buttons={buttons} />
      <Detail
        headerFields={displayData?.headerFields}
        fields={displayData?.fields}
        showBlankFields
      />
      { getSchemaAlert }
      { getApiAlert }
    </>
  );
}

export default DetailContainer;
