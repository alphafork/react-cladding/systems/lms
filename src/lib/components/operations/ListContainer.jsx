import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import useApi from '../../hooks/useApi';
import PageHeader from '../PageHeader';
import List from '../crud/List';
import getConfig from '../../utils/getConfig';

function ListContainer() {
  const navigate = useNavigate();
  const path = 'operation';
  const { api, headerFields } = getConfig(path);
  const { data, getApiAlert } = useApi(api);
  const [bodyButtons, setBodyButtons] = useState(null);
  const title = `${path[0].toUpperCase?.()}${path.replace(/-/g, ' ').slice?.(1)} list`;

  useEffect(() => {
    if (!data?.results) {
      return;
    }

    let buttons = {};
    for (const item of data.results) {
      buttons = {
        ...buttons,
        view: {
          ...buttons?.view,
          [item.id]: {
            label: 'View',
            icon: 'eye',
            severity: 'secondary',
            onClick: (id) => (navigate(`/${path}/${id}`)),
          },
        },
      };
    }
    setBodyButtons(buttons);

    return () => {
      setBodyButtons(null);
    };
  }, [data?.results, path, navigate]);

  return (
    <>
      <PageHeader title={title} />
      <List
        headerFields={headerFields}
        data={data}
        buttons={bodyButtons}
      />
      { getApiAlert }
    </>
  );
}

export default ListContainer;
