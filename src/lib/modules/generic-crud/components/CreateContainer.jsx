import React from 'react';
import { useParams } from 'react-router-dom';
import {
  PageHeader,
  FormContainer,
} from '../../../submodules';

function CreateContainer() {
  const { page: path } = useParams();
  const title = `Add ${path.replace(/-/g, ' ')}`;

  return (
    <>
      <PageHeader title={title} />
      <FormContainer path={path} />
    </>
  );
}

export default CreateContainer;
