import React from 'react';
import { Dialog } from 'primereact/dialog';
import FormContainer from '../crud/FormContainer';

function UpdateSchedulePopup({
  visible,
  onHide,
  eventHandlers,
  event,
}) {
  return (
    <Dialog
      header="Edit Schedule"
      visible={visible}
      style={{ width: '50vw' }}
      onHide={onHide}
    >
      <FormContainer
        path="daily-schedule"
        resourceId={event?.id}
        eventHandlers={eventHandlers}
      />
    </Dialog>
  );
}

export default UpdateSchedulePopup;
