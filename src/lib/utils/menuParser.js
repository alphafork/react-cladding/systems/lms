// Util to format array of menu entries to heirarchical menu structure.

const fetchChildren = (recEntry, groupedParentEntries) => {
  const recItem = { ...recEntry };
  if (groupedParentEntries[recItem.id]) {
    const childEntries = [];
    const groupedEntry = groupedParentEntries[recItem.id];

    for (const childEntry of groupedEntry) {
      childEntries.push(fetchChildren(childEntry, groupedParentEntries));
    }

    recItem.items = childEntries;
  }
  return recItem;
};

const menuParser = (userMenu) => {
  const groupedParentEntries = {};
  for (const menuEntry of userMenu) {
    if (!groupedParentEntries[menuEntry.parent]) {
      groupedParentEntries[menuEntry.parent] = [];
    }
    groupedParentEntries[menuEntry.parent].push(menuEntry);
  }

  const menu = [];

  for (const menuEntry of userMenu) {
    if (menuEntry.parent === 0) {
      menu.push(fetchChildren(menuEntry, groupedParentEntries));
    }
  }

  return menu;
};

export default menuParser;
