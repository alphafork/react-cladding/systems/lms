import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Tag } from 'primereact/tag';
import { Button } from 'primereact/button';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListAllStudentContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const allStudents = useApi(api.allStudents);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    has_applied_any: { value: null, matchMode: FilterMatchMode.EQUALS },
  });

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const getApplicationStatus = (rowData) => {
    const { has_applied_any: appliedAny } = rowData;
    return (
      <Tag
        value={appliedAny ? 'Yes' : 'No'}
        severity={appliedAny ? 'success' : 'danger'}
        className="px-2 py-1"
      />
    );
  };

  const getApplicationStatusFilterTemplate = (status) => (
    <Tag
      value={status.label}
      severity={status.value ? 'success' : 'danger'}
      className="px-2 py-1"
    />
  );

  const getApplicationStatusFilter = (options) => {
    const statusOptions = [
      {
        label: 'Yes',
        value: true,
      },
      {
        label: 'No',
        value: false,
      },
    ];
    return (
      <Dropdown
        value={options.value}
        options={statusOptions}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={getApplicationStatusFilterTemplate}
        placeholder="Select One"
        className="p-column-filter"
        showClear
        style={{ minWidth: '12rem' }}
      />
    );
  };

  const action = (rowData) => (
    <Button
      label="View Profile"
      icon="pi pi-eye"
      className="m-1 px-3 py-2"
      severity="success"
      onClick={() => (
        navigate(`/student/${rowData.user_id}`)
      )}
    />
  );

  const columns = [
    { header: 'Student', field: 'full_name' },
    { header: 'Email', field: 'email' },
    {
      header: 'Applied any',
      field: 'has_applied_any',
      body: getApplicationStatus,
      filter: true,
      filterElement: getApplicationStatusFilter,
      showFilterMenu: false,
    },
    { header: 'Action', body: action },
  ];

  const pagination = {
    onClickPrev: (e, callback) => {
      if (allStudents.data?.previous) {
        allStudents.sendRequest({ url: allStudents.data.previous });
      }
      callback(e);
    },
    onClickNext: (e, callback) => {
      if (allStudents.data?.next) {
        allStudents.sendRequest({ url: allStudents.data.next });
      }
      callback(e);
    },
  };

  const buttons = [{
    label: 'Add Student',
    icon: 'user-plus',
    onClick: () => navigate('/student/register'),
  }];

  return (
    <>
      <PageHeader
        title="All Students"
        buttons={buttons}
      />
      <ListDataTable
        data={allStudents.data}
        header={renderHeader}
        globalFilterFields={[
          'full_name',
          'email',
        ]}
        columns={columns}
        filters={filters}
        pagination={pagination}
      />
    </>
  );
}

export default ListAllStudentContainer;
