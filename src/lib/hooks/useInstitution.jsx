import { useEffect } from 'react';
import useStore from './useStore';
import useApi from './useApi';
import api from '../config/api';

function useInstitution() {
  const institution = useStore((state) => state.institution);
  const setInstitution = useStore((state) => state.setInstitution);
  const institutionProfile = useApi(api.institutionProfile);

  useEffect(() => {
    if (institutionProfile.data?.results?.[0]) {
      setInstitution(institutionProfile.data.results[0]);
    }
  }, [institutionProfile.data?.results]);

  return institution;
}

export default useInstitution;
