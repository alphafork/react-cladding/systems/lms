import api from '../api';

export default {
  'profile/education': {
    api: api.userEducation,
    excludedFields: [
      'id',
      'user',
    ],
  },
  'profile/workexperience': {
    api: api.userWorkExperience,
    excludedFields: [
      'id',
      'user',
    ],
  },
};
