import React, { useEffect } from 'react';
import { Button } from 'primereact/button';
import PageHeader from '../PageHeader';
import useApi from '../../hooks/useApi';
import useAlert from '../../hooks/useAlert';
import api from '../../config/api';

function DailyAccountingOperation() {
  const alert = useAlert();
  const dailySync = useApi();

  const handleOnClick = () => {
    dailySync.sendRequest({
      url: api.dailyAccountingOperation,
      method: 'POST',
      data: { is_authorized: true },
    });
  };

  useEffect(() => {
    if (dailySync.status === 201) {
      alert.setAlert(
        'success',
        'Daily sync completed successfully',
      );
    }
  });

  return (
    <>
      <PageHeader title="Daily Accounting Operation" />
      <div className="card flex justify-content-center mt-4">
        <Button
          label="Run daily sync"
          icon="pi pi-replay"
          className="mt-2"
          onClick={handleOnClick}
        />
      </div>
      { alert.getAlert }
      { dailySync.getApiAlert }
    </>
  );
}

export default DailyAccountingOperation;
