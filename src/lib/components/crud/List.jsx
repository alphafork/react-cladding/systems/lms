import React from 'react';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { ProgressSpinner } from 'primereact/progressspinner';

function List({
  headerFields,
  data,
  buttons,
}) {
  const { titleField, subtitleField } = headerFields;
  return (
    <div className="grid">
      { !data
        ? <ProgressSpinner />
        : data?.results?.map?.((item) => (
          <div key={item.id} className="col-3 flex">
            <Card
              title={item[titleField]}
              subTitle={item[subtitleField]}
              className="relative w-full"
            >
              <div className="absolute" style={{ bottom: 15, right: 15 }}>
                { buttons && Object.values(buttons).map((btn) => (
                  btn?.[item?.id] && (
                    <Button
                      key={btn[item.id].label.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')}
                      label={btn[item.id].label}
                      className={`mt-2 mr-2 px-3 py-2 ${btn[item.id].extraClasses ?? ''}`}
                      icon={`pi pi-${btn[item.id].icon}`}
                      severity={btn[item.id].severity ?? false}
                      disabled={btn[item.id].disabled}
                      tooltip={btn[item.id].tooltip}
                      tooltipOptions={{ showOnDisabled: true, position: 'bottom' }}
                      onClick={() => btn[item.id].onClick(item.id)}
                    />
                  )
                ))}
              </div>
            </Card>
          </div>
        ))}
    </div>
  );
}

export default List;
