const FacultyQuickLinks = [
  {
    label: 'Batches',
    link: '/batch',
  },
  {
    label: 'Exams',
    link: '/exam',
  },
];

export default FacultyQuickLinks;
