import React from 'react';
import { Link } from 'react-router-dom';

function ManagerSideBar() {
  return (
    <div className="layout-sidebar">
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Dashboard</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/home">
                <i className="layout-menuitem-icon pi pi-fw pi-home" />
                <span className="layout-menuitem-text">Home</span>
              </Link>
            </li>
          </ul>
          <div className="layout-menuitem-root-text">ACADEMIC</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/course">
                <i className="layout-menuitem-icon pi pi-fw pi-list" />
                <span className="layout-menuitem-text">Courses</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/package">
                <i className="layout-menuitem-icon pi pi-fw pi-briefcase" />
                <span className="layout-menuitem-text">Packages</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/subject">
                <i className="layout-menuitem-icon pi pi-fw pi-book" />
                <span className="layout-menuitem-text">Subjects</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/batch">
                <i className="layout-menuitem-icon pi pi-fw pi-inbox" />
                <span className="layout-menuitem-text">Batches</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/room">
                <i className="layout-menuitem-icon pi pi-fw pi-building" />
                <span className="layout-menuitem-text">Rooms</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/exam">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Exams</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">STUDENTS</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/all">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">All</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/approved">
                <i className="layout-menuitem-icon pi pi-fw pi-check-circle" />
                <span className="layout-menuitem-text">Approved</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/alumni">
                <i className="layout-menuitem-icon pi pi-fw pi-star" />
                <span className="layout-menuitem-text">Alumni</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/register">
                <i className="layout-menuitem-icon pi pi-fw pi-user-plus" />
                <span className="layout-menuitem-text">New Student Registration</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Referrals</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/referrer">
                <i className="layout-menuitem-icon pi pi-directions" />
                <span className="layout-menuitem-text">Referrers</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  );
}

export default ManagerSideBar;
