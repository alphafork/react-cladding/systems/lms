import React from 'react';
import { Dialog } from 'primereact/dialog';
import FormContainer from '../crud/FormContainer';

function AddRepeatingSchedulePopup({
  visible,
  onHide,
  eventHandlers,
  defaultData,
  fieldAlerts,
}) {
  return (
    <Dialog
      header="Add Repeating Schedule"
      visible={visible}
      style={{ width: '50vw' }}
      onHide={onHide}
    >
      <FormContainer
        path="repeating-schedule"
        eventHandlers={eventHandlers}
        defaultData={defaultData}
        fieldAlerts={fieldAlerts}
      />
    </Dialog>
  );
}

export default AddRepeatingSchedulePopup;
