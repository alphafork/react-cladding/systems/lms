import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { pdf } from '@react-pdf/renderer';
import { saveAs } from 'file-saver';
import dayjs from 'dayjs';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { SplitButton } from 'primereact/splitbutton';
import { Badge } from 'primereact/badge';
import { Timeline } from 'primereact/timeline';
import { Dialog } from 'primereact/dialog';
import { ProgressSpinner } from 'primereact/progressspinner';
import PageHeader from '../../generic/components/PageHeader';
import FormContainer from '../../generic/components/FormContainer';
import StatusBadge from '../../status-badge/components/StatusBadge';
import ProgramListPopup from '../../program/components/ProgramListPopup';
import CertificatePdfTemplate from '../../program-certificate/components/CertificatePdfTemplate';
import InternshipCertificatePdfTemplate from '../../internship-certificate/components/InternshipCertificatePdfTemplate';
import IdCardPdfTemplate from '../../student-id-card/components/IdCardPdfTemplate';
import useStore from '../../../hooks/useStore';
import useAlert from '../../../hooks/useAlert';
import useApi from '../../../hooks/useApi';
import api from '../../../config/api';

function StudentProfile({ userId }) {
  const navigate = useNavigate();
  const params = useParams();
  const { role: userRole } = useStore((state) => state.user);
  const id = userId ?? params.id;
  const studentDetail = useApi(`${api.userAccount}/${id}`);
  const studentBio = useApi(`${api.userProfile}/${id}`);
  const studentContact = useApi(`${api.userContact}/${id}`);
  const studentGuardian = useApi(`${api.studentGuardian}/${id}`);
  const studentEmployment = useApi(`${api.studentEmployment}/${id}`);
  const studentPrograms = useApi(`${api.studentDetails}?student_id=${id}`);
  const studentFees = useApi(`${api.feeTimeline}?student_id=${id}`);
  const programStatus = useApi();
  const programStatusAlert = useAlert();
  const programCertificates = useApi();
  const internshipCertificates = useApi();
  const [isStudent, setIsStudent] = useState(null);
  const [studentCourses, setStudentCourses] = useState(null);
  const [studentPackages, setStudentPackages] = useState(null);
  const [registrationIds, setRegistrationIds] = useState([]);
  const [activeProgram, setActiveProgram] = useState(null);
  const [showPopup, setShowPopup] = useState({
    guardianForm: false,
    employmentForm: false,
    courseApplication: false,
    packageApplication: false,
    programCertificateForm: false,
    internshipCertificateForm: false,
  });

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  useEffect(() => {
    if (studentGuardian.status === 200 && studentEmployment.status === 200) {
      setIsStudent(true);
    }
    if (studentGuardian.status === 404 && studentEmployment.status === 404) {
      navigate('/');
    }
  }, [studentGuardian.status, studentEmployment.status]);

  useEffect(() => {
    if (!studentPrograms.data?.results) return;

    const regIds = studentPrograms.data.results.map((program) => (
      program.registration.id
    ));
    setRegistrationIds(regIds);

    const programCertUrl = `${api.certificate}?filter_by={"registration_id__in":[${regIds}]}`;
    programCertificates.sendRequest({ url: programCertUrl });
    const internCertUrl = `${api.internship}?filter_by={"registration_id__in":[${regIds}]}`;
    internshipCertificates.sendRequest({ url: internCertUrl });
  }, [studentPrograms.data?.results]);

  useEffect(() => {
    const programs = studentPrograms.data?.results;
    if (
      !programs
      || studentFees.status !== 200
      || programCertificates.status !== 200
      || internshipCertificates.status !== 200
    ) {
      return;
    }

    const programFees = studentFees.data?.results ?? [];
    const programCerts = programCertificates.data?.results ?? [];
    const internCerts = internshipCertificates.data?.results ?? [];
    const allPrograms = programs.map((program) => {
      const programFee = programFees.find((fee) => (
        fee.registration.id === program.registration.id
      ));

      const programCert = programCerts.find((cert) => (
        cert.registration === program.registration.id
      ));

      const internCert = internCerts.find((cert) => (
        cert.registration === program.registration.id
      ));

      return ({
        ...program,
        programFee,
        programCertificate: programCert,
        internshipCertificate: internCert,
      });
    });

    const groupedPrograms = Object.groupBy(
      allPrograms,
      ({ program }) => program.type,
    );
    setStudentCourses(groupedPrograms?.course);
    setStudentPackages(groupedPrograms?.package);
  }, [
    studentPrograms.data?.results,
    studentFees.status,
    programCertificates.status,
    internshipCertificates.status,
    studentFees.data?.results,
    programCertificates.data?.results,
    internshipCertificates.data?.results,
  ]);

  const markProgramAsCompleted = (program) => {
    const url = `${api.studentRegistration}/${program.registration.id}`;
    const method = 'PUT';
    const data = {
      registration_no: program.registration_no,
      applied_date: program.applied_date,
      student: program.student.id,
      status: 'co',
    };
    programStatus.sendRequest({
      url,
      method,
      data,
    });

    const programCertUrl = `${api.certificate}?filter_by={"registration_id__in":[${registrationIds}]}`;
    programCertificates.sendRequest({ url: programCertUrl });
    const internCertUrl = `${api.internship}?filter_by={"registration_id__in":[${registrationIds}]}`;
    internshipCertificates.sendRequest({ url: internCertUrl });
  };

  useEffect(() => {
    if (programStatus.status === 200) {
      programStatusAlert.setAlert('success', 'Successfully marked program as completed.');
      const url = `${api.studentDetails}?student_id=${id}`;
      studentPrograms.sendRequest({ url });
    }
  }, [programStatus.status]);

  const downloadIdCard = async (program) => {
    const blob = await pdf((
      <IdCardPdfTemplate
        program={program}
        photo={studentBio.data?.photo}
      />
    )).toBlob();
    const fileName = `student_id_card_${program.registration.id}`;
    saveAs(blob, fileName);
  };

  const downloadCertificate = async (program) => {
    const blob = await pdf((
      <CertificatePdfTemplate certificate={program?.programCertificate} />
    )).toBlob();
    const fileName = `certificate_${program.registration.id}`;
    saveAs(blob, fileName);
  };

  const downloadInternshipCertificate = async (program) => {
    const blob = await pdf((
      <InternshipCertificatePdfTemplate
        studentName={program?.student?.name}
        internship={program?.internshipCertificate}
      />
    )).toBlob();
    const fileName = `internship_certificate_${program.registration.id}`;
    saveAs(blob, fileName);
  };

  const generateProgramCertificate = (program) => {
    setActiveProgram(program);
    togglePopup('programCertificateForm');
  };

  const generateInternshipCertificate = (program) => {
    setActiveProgram(program);
    togglePopup('internshipCertificateForm');
  };

  const getProgramButtons = (program) => {
    const buttons = [];

    if (userRole === 'Admin' || userRole === 'Manager') {
      if (program.status === 'pr') {
        buttons.push({
          label: 'Mark as completed',
          className: 'ml-2',
          icon: 'pi pi-check',
          size: 'small',
          command: () => markProgramAsCompleted(program),
        });
      }
      if (program.status === 'co') {
        buttons.push({
          label: program?.programCertificate?.is_downloadable ? (
            'Edit Completion Certificate'
          ) : (
            'Generate Completion Certificate'
          ),
          className: 'ml-2',
          icon: program?.programCertificate?.is_downloadable ? (
            'pi pi-pencil'
          ) : (
            'pi pi-star'
          ),
          size: 'small',
          command: () => generateProgramCertificate(program),
        });
        if (
          program?.programCertificate?.is_downloadable
        ) {
          buttons.push({
            label: program?.internshipCertificate?.is_downloadable ? (
              'Edit Internship Certificate'
            ) : (
              'Generate Internship Certificate'
            ),
            className: 'ml-2',
            icon: program?.programCertificate?.is_downloadable ? (
              'pi pi-pencil'
            ) : (
              'pi pi-file'
            ),
            size: 'small',
            command: () => generateInternshipCertificate(program),
          });
        }
      }
    }

    if (program.status === 'co') {
      if (program?.programCertificate?.is_downloadable) {
        buttons.splice(
          1,
          0,
          {
            label: 'Download Completion Certificate',
            className: 'ml-2',
            icon: 'pi pi-star',
            size: 'small',
            severity: 'success',
            command: () => downloadCertificate(program),
          },
        );
      }
      if (program?.internshipCertificate?.is_downloadable) {
        buttons.push({
          label: 'Download Internship Certificate',
          className: 'ml-2',
          icon: 'pi pi-file',
          size: 'small',
          severity: 'success',
          command: () => downloadInternshipCertificate(program),
        });
      }
    }
    return buttons;
  };

  const packageHeader = (
    <div className="flex mt-4">
      <h3 className="flex-auto flex justify-content-between font-bold mt-4">Packages</h3>
      <div className="flex-auto flex align-items-center justify-content-end font-bold">
        <Button
          label="Apply"
          icon="pi pi-plus"
          onClick={() => togglePopup('packageApplication')}
        />
      </div>
    </div>
  );

  const courseHeader = (
    <div className="flex mt-4">
      <h3 className="flex-auto flex justify-content-between font-bold mt-4">Courses</h3>
      <div className="flex-auto flex align-items-center justify-content-end font-bold">
        <Button
          label="Apply"
          icon="pi pi-plus"
          onClick={() => togglePopup('courseApplication')}
        />
      </div>
    </div>
  );

  const getProgramHeader = (name, program) => (
    <div className="flex">
      <div className="flex flex-auto font-bold py-3">
        {name}
        &nbsp;
        <Badge value={`Reg No: ${program.registration_no}`} />
      </div>
      <div className="flex-auto flex align-items-center justify-content-end font-bold py-3">
        <Button
          label="Referral"
          icon="pi pi-directions"
          size="small"
          className="ml-2"
          severity="secondary"
          disabled={program.status === 'ap'}
          onClick={() => (navigate('/referral', {
            state: {
              studentId: id,
              registrationId: program.registration.id,
            },
          }))}
        />
        {program && (
          <SplitButton
            label="Download ID Card"
            icon="pi pi-id-card"
            size="small"
            className="ml-2"
            onClick={() => downloadIdCard(program)}
            disabled={program.status !== 'pr' && program.status !== 'co'}
            model={getProgramButtons(program)}
          />
        )}
      </div>
    </div>
  );

  const profileButtons = [
    {
      label: 'Edit Guardian Details',
      icon: 'pi pi-users',
      command: () => togglePopup('guardianForm'),
    },
    {
      label: 'Edit Job Preferences',
      icon: 'pi pi-briefcase',
      command: () => togglePopup('employmentForm'),
    },
    {
      label: 'Job Placement',
      icon: 'pi pi-globe',
      command: () => navigate('/job-placement', { state: { studentId: id } }),
    },
  ];

  const eventHandlers = {
    programCertificate: {
      onSuccess: () => {
        togglePopup('programCertificateForm');
        const programCertUrl = `${api.certificate}?filter_by={"registration_id__in":[${registrationIds}]}`;
        programCertificates.sendRequest({ url: programCertUrl });
        const internCertUrl = `${api.internship}?filter_by={"registration_id__in":[${registrationIds}]}`;
        internshipCertificates.sendRequest({ url: internCertUrl });
      },
    },
    internshipCertificate: {
      onSuccess: () => {
        togglePopup('internshipCertificateForm');
        const internCertUrl = `${api.internship}?filter_by={"registration_id__in":[${registrationIds}]}`;
        internshipCertificates.sendRequest({ url: internCertUrl });
      },
    },
  };

  const getInstallmentStyle = (installment) => {
    const style = {};
    if (installment.due_date) {
      if (Number(installment.balance_amount) === 0) {
        style.icon = 'pi pi-check-circle';
        style.color = 'var(--green-500)';
      } else if (Number(installment.balance_amount) === Number(installment.amount)) {
        style.icon = 'pi pi-times-circle';
        style.color = 'var(--red-500)';
      } else {
        style.icon = 'pi pi-info-circle';
        style.color = 'var(--orange-500)';
      }
    } else {
      style.icon = 'pi pi-minus-circle';
      style.color = 'var(--gray-500)';
    }
    return style;
  };

  const getFeeTimeline = (timeline) => (
    timeline?.map?.((installment, i) => ({
      ...installment,
      item: installment.item === 'down payment' ? 'Reg' : `T${i}`,
      ...getInstallmentStyle(installment),
    }))
  );

  const feeMarker = (node) => (
    <span
      className="flex w-2rem h-2rem align-items-center justify-content-center text-white border-circle z-1 shadow-1"
      style={{ backgroundColor: node.color }}
    >
      <i className={node.icon} />
    </span>
  );

  const feeContent = (installment) => (
    <>
      <span>{installment.item}</span>
      <br />
      <span>{installment.cumulative_percentage ?? 'Fee'}</span>
    </>
  );

  if (!isStudent) {
    return (
      <div className="flex justify-content-center">
        <ProgressSpinner />
      </div>
    );
  }

  return (
    <>
      <PageHeader
        title="Student Profile"
      >
        <SplitButton
          label="Manage Profile"
          icon="pi pi-user"
          onClick={() => navigate('/profile', { state: { userId: id } })}
          model={profileButtons}
        />
      </PageHeader>

      <div className="surface-0">
        <div className="grid">
          <div className="col-12 lg:col-4">
            <Card title="Basic Details">
              <ul className="list-none p-0 m-0">
                <li className="flex align-items-center p-1 flex-wrap">
                  <div className="text-500 w-3 md:w-3 font-medium">Name</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {`${studentDetail.data?.first_name} ${studentDetail.data?.last_name}`}
                  </div>
                </li>
                <li className="flex align-items-center p-1 flex-wrap">
                  <div className="text-500 w-3 md:w-3 font-medium">Mobile</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {studentContact.data?.primary_phone_no}
                    {(studentContact.data?.primary_phone_no && studentContact.data?.secondary_phone) && ', '}
                    {studentContact.data?.secondary_phone_no}
                  </div>
                </li>
                <li className="flex align-items-center p-1 flex-wrap">
                  <div className="text-500 w-3 md:w-3 font-medium">Email</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {studentDetail.data?.email}
                  </div>
                </li>
              </ul>
            </Card>
          </div>

          <div className="col-12 lg:col-4">
            <Card title="Guardian Details">
              <ul className="list-none p-0 m-0">
                <li className="flex align-items-center p-1 flex-wrap">
                  <div className="text-500 w-3 md:w-3 font-medium">Guardian</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    <span>{studentGuardian.data?.name}</span>
                    <span>{(studentGuardian.data?.name && (studentGuardian.data?.phone_1 || studentGuardian.data?.phone_2)) && ' - '}</span>
                    <span>{studentGuardian.data?.phone_1}</span>
                    <span>{(studentGuardian.data?.phone_1 && studentGuardian.data?.phone_2) && ', '}</span>
                    <span>{studentGuardian.data?.phone_2}</span>
                    <span>{((studentGuardian.data?.name || studentGuardian.data?.phone_1 || studentGuardian.data?.phone_2) && studentGuardian.data?.relationship_name) && ' ('}</span>
                    <span>{studentGuardian.data?.relationship_name}</span>
                    <span>{((studentGuardian.data?.name || studentGuardian.data?.phone_1 || studentGuardian.data?.phone_2) && studentGuardian.data?.relationship_name) && ')'}</span>
                  </div>
                </li>
                <li className="flex align-items-center p-1 flex-wrap">
                  <div className="text-500 w-3 md:w-3 font-medium">Address</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {studentContact.data?.current_address}
                  </div>
                </li>
                <li className="flex align-items-center p-1 flex-wrap">
                  <div className="text-500 w-3 md:w-3 font-medium">Pin</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {studentContact.data?.current_postalcode}
                  </div>
                </li>
              </ul>
            </Card>
          </div>

          <div className="col-12 lg:col-4">
            <Card title="Job Preferences">
              <ul className="list-none p-0 m-0">
                <li className="flex align-items-center p-2 flex-wrap">
                  <div className="text-500 w-4 md:w-4 font-medium">Preferences</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {studentEmployment.data?.is_job_needed ? ((studentEmployment.data?.is_technical ? 'Technical' : 'Non-technical') + (studentEmployment.data?.is_part_time ? ' (Part-time)' : ' (Full-time)')) : 'None'}
                  </div>
                </li>
                <li className="flex align-items-center p-2 flex-wrap">
                  <div className="text-500 w-4 md:w-4 font-medium">Resume</div>
                  <div className="text-900 md:flex-order-0 flex-order-1">
                    {studentEmployment.data?.resume ? (
                      <Button
                        link
                        label="View File"
                        icon="pi pi-download"
                        className="p-0"
                        onClick={() => window.open(studentEmployment.data?.resume)}
                        disabled={!studentEmployment.data?.resume}
                      />
                    ) : (
                      'Not uploaded'
                    )}
                  </div>
                </li>
              </ul>
            </Card>
          </div>
        </div>
      </div>

      {packageHeader}
      {studentPackages?.map?.((program) => (
        <Card
          title={getProgramHeader(program.program.name, program)}
          className="surface-ground mt-2 mb-5"
        >
          <div className="grid">
            {program.program.courses.map((course) => (
              <div className="col-4">
                <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
                  <div className="text-900 font-medium text-xl">
                    {course.course_name}
                  </div>
                  <span className="block font-medium my-2">
                    <StatusBadge
                      statusType="batchStudentStatus"
                      status={course.batch?.status ?? 'unenrolled'}
                    />
                  </span>
                  <span className="block text-500 font-medium mt-2">
                    Batch
                  </span>
                  <span className="text-900">
                    {course.batch?.batch_name ?? 'N/A'}
                  </span>
                  <span className="block text-500 font-medium mt-2">
                    Enrolled Date
                  </span>
                  <span className="text-900">
                    {course.batch?.enrolled_date ? dayjs(course.batch.enrolled_date).format('DD/MM/YYYY') : 'N/A'}
                  </span>
                  <span className="block text-500 font-medium mt-2">
                    Attendance
                  </span>
                  <span className="text-900">
                    {course.attendance?.total_days ? `${course.attendance.present_days}/${course.attendance.total_days} (${course.attendance.attendance_percentage})` : 'N/A'}
                  </span>
                  {Array.isArray(course.exam_results) ? (
                    course.exam_results?.map?.((result) => (
                      <>
                        <span className="block text-500 font-medium mt-2">
                          {result.exam}
                        </span>
                        <span className="text-900">
                          {`${result.marks}/${result.max_marks} (${result.percentage})`}
                          &nbsp;
                          <Badge
                            value={result.status === 'passed' ? 'Passed' : 'Failed'}
                            severity={result.status === 'passed' ? 'success' : 'danger'}
                          />
                        </span>
                      </>
                    ))
                  ) : (
                    <>
                      <span className="block text-500 font-medium mt-2">
                        Exams
                      </span>
                      <span className="text-900">
                        N/A
                      </span>
                    </>
                  )}
                </div>
              </div>
            ))}
            {program.programFee?.payment_timeline && (
              <div className="col-12">
                <div className="mt-4 mb-2 px-2">
                  <div className="flex align-items-start justify-content-between">
                    <div>
                      <div className="text-900 font-medium text-xl mb-1">
                        Fee Details
                        <br />
                      </div>
                      <span className="text-600">
                        Current Due:&nbsp;
                      </span>
                      <span className="text-900">
                        ₹
                        {program.programFee?.current_payable}
                        &nbsp;
                        {program.programFee?.current_payable > 0 && (
                          <>
                            &#40;
                            <span className="text-600">
                              Due Date:&nbsp;
                            </span>
                            <span className="text-900">
                              {program.programFee?.due_date && (
                                dayjs(program.programFee.due_date)
                                  .format('MMM D, YYYY')
                              )}
                            </span>
                            &#41;
                          </>
                        )}
                      </span>
                      <span className="font-medium ml-2 my-2">
                        <StatusBadge
                          statusType="paymentStatus"
                          status={program.programFee?.payment_status}
                        />
                      </span>
                      <br />
                      {(userRole === 'Admin'
                        || userRole === 'Accountant'
                        || userRole === 'Student'
                      ) && (
                        <>
                          <span className="text-600">
                            Total Paid:&nbsp;
                          </span>
                          <span className="text-900">
                            ₹
                            {program.programFee?.total_paid}
                            &nbsp;&#40;
                            <span className="text-600">
                              Out of:&nbsp;
                            </span>
                            <span className="text-900">
                              ₹
                              {program.programFee?.total_amount}
                            </span>
                            &#41;
                          </span>
                        </>
                      )}
                    </div>
                  </div>
                  <Timeline
                    value={getFeeTimeline(program.programFee?.payment_timeline)}
                    layout="horizontal"
                    className="customized-timeline"
                    marker={feeMarker}
                    content={feeContent}
                  />
                </div>
              </div>
            )}
          </div>
        </Card>
      ))}

      {courseHeader}
      {studentCourses?.map?.(({ program, programFee }, index) => (
        <Card
          title={getProgramHeader(program.name, studentCourses[index])}
          className="surface-ground mt-2 mb-5"
        >
          <div className="grid">
            <div className="col-4">
              <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
                <div className="flex justify-content-between mb-3">
                  <div>
                    <span className="block font-medium my-2">
                      <StatusBadge
                        statusType="batchStudentStatus"
                        status={program.batch?.status ?? 'unenrolled'}
                      />
                    </span>
                    <span className="block text-500 font-medium mt-2">
                      Batch
                    </span>
                    <span className="text-900">
                      {program.batch?.batch_name ?? 'N/A'}
                    </span>
                    <span className="block text-500 font-medium mt-2">
                      Enrolled Date
                    </span>
                    <span className="text-900">
                      {program.batch?.enrolled_date ? dayjs(program.batch.enrolled_date).format('DD/MM/YYYY') : 'N/A'}
                    </span>
                    <span className="block text-500 font-medium mt-2">
                      Attendance
                    </span>
                    <span className="text-900">
                      {program.attendance?.total_days ? `${program.attendance.present_days}/${program.attendance.total_days} (${program.attendance.attendance_percentage})` : 'N/A'}
                    </span>
                    {Array.isArray(program.exam_results) ? (
                      program.exam_results?.map?.((result) => (
                        <>
                          <span className="block text-500 font-medium mt-2">
                            {result.exam}
                          </span>
                          <span className="text-900">
                            {`${result.marks}/${result.max_marks} (${result.percentage})`}
                            &nbsp;
                            <Badge
                              value={result.status === 'passed' ? 'Passed' : 'Failed'}
                              severity={result.status === 'passed' ? 'success' : 'danger'}
                            />
                          </span>
                        </>
                      ))
                    ) : (
                      <>
                        <span className="block text-500 font-medium mt-2">
                          Exams
                        </span>
                        <span className="text-900">
                          N/A
                        </span>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
            {programFee?.payment_timeline && (
              <div className="col-12">
                <div className="mt-4 mb-2 px-2">
                  <div className="flex align-items-start justify-content-between">
                    <div>
                      <div className="text-900 font-medium text-xl mb-1">
                        Fee Details
                        <br />
                      </div>
                      <span className="text-600">
                        Current Due:&nbsp;
                      </span>
                      <span className="text-900">
                        ₹
                        {programFee?.current_payable}
                        &nbsp;
                        {programFee?.current_payable > 0 && (
                          <>
                            &#40;
                            <span className="text-600">
                              Due Date:&nbsp;
                            </span>
                            <span className="text-900">
                              {programFee?.due_date && (
                                dayjs(programFee.due_date)
                                  .format('MMM D, YYYY')
                              )}
                            </span>
                            &#41;
                          </>
                        )}
                      </span>
                      <span className="font-medium ml-2 my-2">
                        <StatusBadge
                          statusType="paymentStatus"
                          status={programFee?.payment_status}
                        />
                      </span>
                      <br />
                      {(userRole === 'Admin'
                        || userRole === 'Accountant'
                        || userRole === 'Student'
                      ) && (
                        <>
                          <span className="text-600">
                            Total Paid:&nbsp;
                          </span>
                          <span className="text-900">
                            ₹
                            {programFee?.total_paid}
                            &nbsp;&#40;
                            <span className="text-600">
                              Out of:&nbsp;
                            </span>
                            <span className="text-900">
                              ₹
                              {programFee?.total_amount}
                            </span>
                            &#41;
                          </span>
                        </>
                      )}
                    </div>
                  </div>
                  <Timeline
                    value={getFeeTimeline(programFee?.payment_timeline)}
                    layout="horizontal"
                    className="customized-timeline"
                    marker={feeMarker}
                    content={feeContent}
                  />
                </div>
              </div>
            )}
          </div>
        </Card>
      ))}

      <Dialog
        header="Edit Guardian Details"
        visible={showPopup.guardianForm}
        style={{ width: '75vw', 'min-height': '50%' }}
        onHide={() => togglePopup('guardianForm')}
      >
        <FormContainer
          path="student/guardian"
          resourceId={id}
          eventHandlers={{
            onSuccess: () => {
              togglePopup('guardianForm');
              studentGuardian.sendRequest({ url: `${api.studentGuardian}/${id}` });
            },
            onCancel: () => togglePopup('guardianForm'),
          }}
        />
      </Dialog>

      <Dialog
        header="Edit Job Preferences"
        visible={showPopup.employmentForm}
        style={{ width: '75vw', 'min-height': '50%' }}
        onHide={() => togglePopup('employmentForm')}
      >
        <FormContainer
          path="student/employment"
          resourceId={id}
          eventHandlers={{
            onSuccess: () => {
              togglePopup('employmentForm');
              studentEmployment.sendRequest({ url: `${api.studentEmployment}/${id}` });
            },
            onCancel: () => togglePopup('employmentForm'),
          }}
        />
      </Dialog>

      <Dialog
        header="Generate Completion Certificate"
        visible={showPopup.programCertificateForm}
        style={{ width: '75vw', minHeight: '50%' }}
        onHide={() => togglePopup('programCertificateForm')}
      >
        <FormContainer
          path="certificate"
          resourceId={activeProgram?.registration?.id}
          eventHandlers={eventHandlers.programCertificate}
        />
      </Dialog>

      <Dialog
        header="Generate Internship Certificate"
        visible={showPopup.internshipCertificateForm}
        style={{ width: '75vw', minHeight: '50%' }}
        onHide={() => togglePopup('internshipCertificateForm')}
      >
        <FormContainer
          path="internship"
          resourceId={activeProgram?.registration?.id}
          eventHandlers={eventHandlers.internshipCertificate}
        />
      </Dialog>

      <ProgramListPopup
        visible={showPopup.packageApplication}
        onHide={() => togglePopup('packageApplication')}
        studentId={id}
        programType="package"
      />

      <ProgramListPopup
        visible={showPopup.courseApplication}
        onHide={() => togglePopup('courseApplication')}
        studentId={id}
        programType="course"
      />

      {programStatus.getApiAlert}
      {programStatusAlert.getAlert}
    </>
  );
}

export default StudentProfile;
