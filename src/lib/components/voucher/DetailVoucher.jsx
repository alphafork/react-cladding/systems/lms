import React from 'react';
import { pdf } from '@react-pdf/renderer';
import { saveAs } from 'file-saver';
import dayjs from 'dayjs';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import VoucherPdfTemplate from './VoucherPdfTemplate';
import useFieldParser from '../../hooks/useFieldParser';
import useApi from '../../hooks/useApi';
import api from '../../config/api';
import getConfig from '../../utils/getConfig';

function VoucherDetail({
  visible,
  onHide,
  voucherId,
  buttons,
}) {
  const voucher = useApi(`${api.voucher}/${voucherId}`);
  const config = getConfig('voucher');
  const {
    displayData,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, voucherId);

  const downloadVoucherPdf = async () => {
    if (voucher.data) {
      const fileName = `voucher-${voucherId}-${dayjs().format('YYYYMMDD-HHmmss')}.pdf`;
      const blob = await pdf((
        <VoucherPdfTemplate voucher={voucher} />
      )).toBlob();
      saveAs(blob, fileName);
    }
  };

  const voucherButtons = buttons ?? [
    {
      label: 'Print',
      icon: 'print',
      onClick: () => downloadVoucherPdf(voucherId),
    },
  ];

  return (
    <Dialog
      header="Voucher Details"
      visible={visible}
      onHide={onHide}
      style={{ width: '75vw' }}
    >
      <PageHeader buttons={voucherButtons} />
      {voucherId && (
        <Detail
          headerFields={displayData?.headerFields}
          fields={displayData?.fields}
          showBlankFields
        />
      )}
      { getSchemaAlert }
      { getApiAlert }
    </Dialog>
  );
}

export default VoucherDetail;
