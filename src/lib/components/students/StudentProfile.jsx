import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import ProgramPopup from '../programs/ProgramPopup';
import RegistrationStatus from './RegistrationStatus';
import FormContainer from '../crud/FormContainer';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function StudentProfile({ userId }) {
  const navigate = useNavigate();
  const params = useParams();
  const id = userId ?? params.id;
  const studentDetail = useApi(`${api.userAccount}/${id}`);
  const studentContact = useApi(`${api.userContact}/${id}`);
  const studentGuardian = useApi(`${api.studentGuardian}/${id}`);
  const studentEmployment = useApi(`${api.studentEmployment}/${id}`);
  const studentCourse = useApi(`${api.courseApplication}?filter_by={"registration__student_id":${id}}`);
  const studentPackage = useApi(`${api.packageApplication}?filter_by={"registration__student_id":${id}}`);
  const [showPopup, setShowPopup] = useState({
    guardianForm: false,
    employmentForm: false,
    courseApplication: false,
    packageApplication: false,
  });

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const packageHeader = (
    <div className="flex">
      <div className="flex-auto flex justify-content-between font-bold py-3">Packages</div>
      <div className="flex-auto flex align-items-center justify-content-end font-bold  px-5 py-3">
        <Button
          label="Apply"
          icon="pi pi-plus"
          onClick={() => togglePopup('packageApplication')}
        />
      </div>
    </div>
  );

  const courseHeader = (
    <div className="flex">
      <div className="flex-auto flex justify-content-between font-bold py-3">Courses</div>
      <div className="flex-auto flex align-items-center justify-content-end font-bold  px-5 py-3">
        <Button
          label="Apply"
          icon="pi pi-plus"
          onClick={() => togglePopup('courseApplication')}
        />
      </div>
    </div>
  );

  const buttons = [
    {
      label: 'Edit Guardian Details',
      icon: 'pi pi-users',
      onClick: () => togglePopup('guardianForm'),
    },
    {
      label: 'Edit Employment Preferences',
      icon: 'pi pi-briefcase',
      onClick: () => togglePopup('employmentForm'),
    },
    {
      label: 'View Full Profile',
      icon: 'pi pi-user',
      severity: 'success',
      onClick: () => navigate('/profile', { state: { userId: id } }),
    },
  ];

  return (
    <>
      <PageHeader
        title={`${studentDetail.data?.first_name} ${studentDetail.data?.last_name}`}
        buttons={buttons}
      />
      <Card>
        <div className="surface-0">
          <ul className="list-none p-0 m-0">
            <li className="flex align-items-center p-2 flex-wrap">
              <div className="text-900 w-6 md:w-6 md:flex-order-0 flex-order-1">
                <span className="text-500 font-medium">Mobile: </span>
                {studentContact.data?.primary_phone_no}
                {(studentContact.data?.primary_phone_no && studentContact.data?.secondary_phone) && ', '}
                {studentContact.data?.secondary_phone_no}
              </div>
              <div className="text-900 w-6 md:w-6 md:flex-order-0 flex-order-1">
                <span className="text-500 font-medium">Email: </span>
                {studentDetail.data?.email}
              </div>
            </li>
            <li className="flex align-items-center p-2 flex-wrap">
              <div className="text-900 w-6 md:w-6 md:flex-order-0 flex-order-1">
                <span className="text-500 font-medium">Address: </span>
                {studentContact.data?.current_address}
              </div>
              <div className="text-900 w-6 md:w-6 md:flex-order-0 flex-order-1">
                <span className="text-500 font-medium">Pin: </span>
                {studentContact.data?.current_postalcode}
              </div>
            </li>
            <li className="flex align-items-center p-2 flex-wrap">
              <div className="text-900 w-6 md:w-6 md:flex-order-0 flex-order-1">
                <span className="text-500 font-medium">Guardian: </span>
                <span>{studentGuardian.data?.name}</span>
                <span>{(studentGuardian.data?.name && (studentGuardian.data?.phone_1 || studentGuardian.data?.phone_2)) && ' - '}</span>
                <span>{studentGuardian.data?.phone_1}</span>
                <span>{(studentGuardian.data?.phone_1 && studentGuardian.data?.phone_2) && ', '}</span>
                <span>{studentGuardian.data?.phone_2}</span>
                <span>{((studentGuardian.data?.name || studentGuardian.data?.phone_1 || studentGuardian.data?.phone_2) && studentGuardian.data?.relationship_name) && ' ('}</span>
                <span>{studentGuardian.data?.relationship_name}</span>
                <span>{((studentGuardian.data?.name || studentGuardian.data?.phone_1 || studentGuardian.data?.phone_2) && studentGuardian.data?.relationship_name) && ')'}</span>
              </div>
              <div className="text-900 w-6 md:w-6 md:flex-order-0 flex-order-1">
                <span className="text-500 font-medium">Employment preferences: </span>
                {studentEmployment.data?.is_job_needed ? ((studentEmployment.data?.is_technical ? 'Technical' : 'Non-technical') + (studentEmployment.data?.is_part_time ? ' (Part-time)' : ' (Full-time)')) : 'None'}
              </div>
            </li>
          </ul>
        </div>
      </Card>

      <Card title={packageHeader} className="surface-ground mt-5">
        <div className="grid">
          {studentPackage.data?.results?.map?.((program) => (
            <div
              className="col-12 md:col-6 lg:col-3"
              style={{ cursor: 'pointer' }}
              onClick={() => navigate(`/student/${id}/program/${program.registration.id}`)}
            >
              <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
                <div className="flex justify-content-between mb-3">
                  <div>
                    <div className="text-900 font-medium text-xl">
                      {program.package_name}
                    </div>
                    <span className="block font-medium mt-2">
                      <RegistrationStatus status={program.registration.status} />
                    </span>
                    <span className="block font-medium mt-4">
                      Registration No:
                      {program.registration.number}
                    </span>
                  </div>
                  <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '2.5rem', height: '2.5rem' }}>
                    <i className="pi pi-server text-blue-500 text-xl" />
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </Card>

      <Card title={courseHeader} className="surface-ground mt-5">
        <div className="grid">
          {studentCourse.data?.results?.map?.((program) => (
            <div
              className="col-12 md:col-6 lg:col-3"
              style={{ cursor: 'pointer' }}
              onClick={() => navigate(`/student/${id}/program/${program.registration.id}`)}
            >
              <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
                <div className="flex justify-content-between mb-3">
                  <div>
                    <div className="text-900 font-medium text-xl">
                      {program.course_name}
                    </div>
                    <span className="block font-medium mt-2">
                      <RegistrationStatus status={program.registration.status} />
                    </span>
                    <span className="block font-medium mt-4">
                      Registration No:
                      {program.registration.number}
                    </span>
                  </div>
                  <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '2.5rem', height: '2.5rem' }}>
                    <i className="pi pi-book text-blue-500 text-xl" />
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </Card>

      <Dialog
        header="Edit Guardian Details"
        visible={showPopup.guardianForm}
        style={{ width: '75vw', 'min-height': '50%' }}
        onHide={() => togglePopup('guardianForm')}
      >
        <FormContainer
          path="student/guardian"
          resourceId={id}
          eventHandlers={{
            onSuccess: () => {
              togglePopup('guardianForm');
              studentGuardian.sendRequest({ url: `${api.studentGuardian}/${id}` });
            },
          }}
        />
      </Dialog>

      <Dialog
        header="Edit Employment Details"
        visible={showPopup.employmentForm}
        style={{ width: '75vw', 'min-height': '50%' }}
        onHide={() => togglePopup('employmentForm')}
      >
        <FormContainer
          path="student/employment"
          resourceId={id}
          eventHandlers={{
            onSuccess: () => {
              togglePopup('employmentForm');
              studentEmployment.sendRequest({ url: `${api.studentEmployment}/${id}` });
            },
          }}
        />
      </Dialog>

      <ProgramPopup
        visible={showPopup.packageApplication}
        onHide={() => togglePopup('packageApplication')}
        studentId={id}
        programType="package"
      />

      <ProgramPopup
        visible={showPopup.courseApplication}
        onHide={() => togglePopup('courseApplication')}
        studentId={id}
        programType="course"
      />
    </>
  );
}

export default StudentProfile;
