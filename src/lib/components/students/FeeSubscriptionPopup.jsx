import React from 'react';
import { Dialog } from 'primereact/dialog';
import DetailStudent from './DetailStudent';
import FeeSubscription from './FeeSubscription';

function FeeSubscriptionPopup({
  visible,
  onHide,
  student,
  togglePopup,
  onFormSubmit,
  onFormCancel,
  excludedFields,
}) {
  return (
    <Dialog
      header="Student Subscription"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <DetailStudent student={student} />
      <FeeSubscription
        togglePopup={togglePopup}
        excludedFields={excludedFields}
        onFormSubmit={onFormSubmit}
        onFormCancel={onFormCancel}
        visible={visible}
      />
    </Dialog>
  );
}

export default FeeSubscriptionPopup;
