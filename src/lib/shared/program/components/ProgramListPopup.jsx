import React from 'react';
import { Dialog } from 'primereact/dialog';
import ProgramList from './ProgramList';

function ProgramListPopup({
  visible,
  onHide,
  studentId,
  programType,
}) {
  return (
    <Dialog
      header={`Apply for ${programType}s`}
      visible={visible}
      style={{ width: '75vw', 'min-height': '50%' }}
      onHide={onHide}
    >
      <ProgramList
        studentId={studentId}
        programType={programType}
      />
    </Dialog>
  );
}

export default ProgramListPopup;
