import React from 'react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { ProgressSpinner } from 'primereact/progressspinner';
import Detail from '../crud/Detail';

function DetailStudent({ student, extraData = [] }) {
  dayjs.extend(utc);
  const programType = student?.program_type === 'package' ? 'Package' : 'Course';

  const header = {
    title: student?.name ?? `${student?.first_name} ${student?.last_name}`,
    subtitle: student?.email,
  };

  const data = [
    {
      label: 'Date joined',
      value: dayjs(student?.date_joined)?.format('DD/MM/YYYY hh:mm A'),
    },
    {
      label: programType,
      value: student?.program_name,
    },
    {
      label: 'Fee',
      value: student?.program_fee,
    },
    ...extraData,
  ];

  return (
    !student
      ? (
        <div className="flex justify-content-center">
          <ProgressSpinner />
        </div>
      ) : (
        <Detail headerFields={header} fields={data} />
      )
  );
}

export default DetailStudent;
