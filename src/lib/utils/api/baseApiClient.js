import axios from 'axios';

const baseApiClient = axios.create({
  baseURL: process.env.API_BASE_URL,
  headers: {
    'Content-type': 'application/json',
    Accept: 'application/json',
  },
});

export default baseApiClient;
