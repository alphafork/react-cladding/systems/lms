import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { ConfirmDialog, confirmDialog } from 'primereact/confirmdialog';
import {
  PageHeader,
  Detail,
} from '../../../submodules';
import useApi from '../../../hooks/useApi';
import useFieldParser from '../../../hooks/useFieldParser';
import getConfig from '../../../utils/getConfig';

function DetailContainer() {
  const { page: path, id } = useParams();
  const navigate = useNavigate();
  const remove = useApi();
  const config = getConfig(path);
  const {
    displayData,
    status,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, id);
  const title = `${path.charAt(0).toUpperCase()}${path.replace(/-/g, ' ').slice(1)} details`;

  useEffect(() => {
    if (status === 404) {
      navigate('/');
    }
  }, [status, navigate]);

  const deleteItem = async () => {
    const url = `${config.api}/${id}`;
    remove.sendRequest({ url, method: 'DELETE' });
  };

  useEffect(() => {
    if (remove.status === 204) navigate(`/${path}`);
  }, [remove.status, navigate, path]);

  const confirmDelete = () => {
    confirmDialog({
      message: 'Are you sure you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      acceptClassName: 'p-button-danger',
      accept: deleteItem,
    });
  };

  const buttons = [
    {
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate(`/${path}`),
      extraClasses: 'p-button-outlined',
    }, {
      label: 'Edit',
      icon: 'pencil',
      onClick: () => navigate('edit'),
    }, {
      label: 'Delete',
      icon: 'trash',
      onClick: () => confirmDelete(),
      severity: 'danger',
    },
  ];

  return (
    <>
      <ConfirmDialog />
      <PageHeader title={title} buttons={buttons} />
      <Detail
        headerFields={displayData?.headerFields}
        fields={displayData?.fields}
        showBlankFields
      />
      { getSchemaAlert }
      { getApiAlert }
      { remove.getApiAlert }
    </>
  );
}

export default DetailContainer;
