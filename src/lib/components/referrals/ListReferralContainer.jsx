import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListReferralContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const { id } = useParams();
  const url = `${api.referral}?referrer_id=${id}`;
  const referrals = useApi(url);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const getAppliedDate = (rowData) => (
    dayjs(rowData.applied_date).format('DD-MM-YYYY')
  );

  const action = (rowData) => (
    <Button
      label="View Student"
      icon="pi pi-eye"
      className="m-1 px-3 py-2"
      severity="success"
      onClick={() => (
        navigate(`/student/${rowData.student_id}`)
      )}
    />
  );

  const columns = [
    { header: 'Registration number', field: 'registration_no' },
    { header: 'Student', field: 'student_name' },
    { header: 'Applied date', body: getAppliedDate },
    { header: 'Fee', field: 'student_fee' },
    { header: 'Action', body: action },
  ];

  const buttons = [
    {
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate('/referrer'),
      extraClasses: 'p-button-outlined',
    },
  ];

  return (
    <>
      <PageHeader
        title={`Referrals ${referrals.data?.results?.[0] ? (
          `by ${referrals.data?.results?.[0].referrer_name}`
        ) : (
          ''
        )}`}
        buttons={buttons}
      />
      <ListDataTable
        data={referrals.data}
        header={renderHeader}
        globalFilterFields={[
          'student_name',
          'registration_no',
        ]}
        columns={columns}
        filters={filters}
      />
    </>
  );
}

export default ListReferralContainer;
