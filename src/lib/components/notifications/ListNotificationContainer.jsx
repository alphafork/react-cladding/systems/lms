import React from 'react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { Button } from 'primereact/button';
import { DataView } from 'primereact/dataview';
import PageHeader from '../PageHeader';
import useNotification from '../../hooks/useNotification';

function ListNotificationContainer() {
  dayjs.extend(utc);
  const {
    notifications,
    markNotificationAsRead,
    markAllNotificationsAsRead,
  } = useNotification();

  const notificationList = (notification) => (
    <div className="w-full flex justify-content-between py-2">
      <div className="flex align-items-center justify-content-center">
        <span className={notification.is_read ? 'text-500' : ''}>{notification.notification}</span>
      </div>
      <div className="flex align-items-center justify-content-center">
        <span className="text-500">
          {dayjs(notification.notification_time).format('MMM D, YYYY')}
        </span>
        <Button
          icon="pi pi-check"
          className="ml-4"
          rounded
          text
          raised={!notification.is_read}
          aria-label="Mark as read"
          onClick={() => markNotificationAsRead(notification)}
          disabled={notification.is_read}
        />
      </div>
    </div>
  );

  const buttons = [
    {
      label: 'Mark all as read',
      icon: 'check',
      onClick: markAllNotificationsAsRead,
    },
  ];

  return (
    <>
      <PageHeader title="Notifications" buttons={buttons} />
      <DataView
        className="mt-2"
        value={notifications}
        itemTemplate={notificationList}
      />
    </>
  );
}

export default ListNotificationContainer;
