import baseApiClient from './baseApiClient';
import { getApiToken } from './apiToken';

// Wrapper method for sending HTTP requests.
const apiClient = async ({
  url,
  method = 'GET',
  data = null,
  authorized = true,
  extraConfig = {},
}) => {
  let loading;
  let response;
  let error;
  let status;
  const safeMethods = ['GET', 'OPTIONS', 'HEAD', 'TRACE'];

  const sanitizeUrl = (apiUrl) => {
    if (!apiUrl) return;
    let sanitizedUrl;
    if (apiUrl.startsWith('http://') || apiUrl.startsWith('https://')) {
      sanitizedUrl = apiUrl;
    } else {
      sanitizedUrl = apiUrl
        .replace(/^\//, '')
        .replace(/\/$/, '')
        .replace(/\/\//g, '/');
      sanitizedUrl = `/${sanitizedUrl}`;
      if (!sanitizedUrl.includes('?')) {
        sanitizedUrl = `${sanitizedUrl}/`;
      }
    }
    return sanitizedUrl;
  };

  const sanitizedUrl = sanitizeUrl(url);
  let config = {
    method,
    url: sanitizedUrl,
  };

  // Ignore request body for safe requests.
  if (data && !safeMethods.includes(method.toUpperCase())) {
    config.data = data;
  }

  // If authorized, set token in the Authorization header.
  if (authorized) {
    const token = getApiToken();
    config.headers = { Authorization: `Token ${token}` };
  }

  // Override config options using extraConfig options provided.
  config = {
    ...config,
    ...extraConfig,
  };

  loading = true;
  try {
    const _response = await baseApiClient(config);
    status = _response?.status;
    response = _response?.data;
  } catch (err) {
    status = err?.response?.status;
    if (err?.response?.headers?.['content-type'].includes('json')) {
      error = err?.response?.data;
    } else {
      error = 'Unable to retrieve remote data';
    }
  } finally {
    loading = false;
  }

  return [response, error, status, loading];
};

export default apiClient;
