import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Dropdown } from 'primereact/dropdown';
import { Checkbox } from 'primereact/checkbox';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import useAlert from '../../hooks/useAlert';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListPermissionContainer() {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  let action = 'view';
  if (pathname.includes('edit')) action = 'edit';
  const contentTypes = useApi(api.contentType);
  const permissions = useApi(api.groupPermission);
  const updatedPermissions = useApi();
  const permissionsAlert = useAlert();
  const [group, setGroup] = useState(null);
  const [groupPermissions, setGroupPermissions] = useState(null);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const title = 'Permission Management';

  useEffect(() => {
    if (permissions.data?.results && contentTypes.data?.results) {
      permissions.data.results.map((permission) => (
        setGroupPermissions((prevGroupPermissions) => ({
          ...prevGroupPermissions,
          [permission.group]: {
            ...permission,
            group_permissions: contentTypes.data?.results
              ?.map?.((contentType) => (
                permission.group_permissions?.find?.((groupPerm) => (
                  groupPerm.content_type === contentType.id
                )) ?? {
                  content_type: contentType?.id,
                  content_type_name: contentType?.model,
                  permissions: [],
                }
              )),
          },
        }))
      ));
    }
  }, [permissions.data?.results, contentTypes.data?.results]);

  const getPermissions = (rowData, permissionType) => {
    const { model } = rowData;

    const handleChange = (event) => {
      setGroupPermissions((prevGroupPermissions) => {
        const userGroupPerms = prevGroupPermissions?.[group]?.group_permissions
          ?.map?.((modelPerms) => {
            let userGroupModelPerms = new Set(modelPerms.permissions);
            if (modelPerms.content_type_name.replaceAll(' ', '') === model) {
              if (event.checked) {
                if (permissionType === 'all') {
                  userGroupModelPerms = new Set(['add', 'change', 'delete', 'view']);
                } else {
                  userGroupModelPerms.add(event.value);
                }
              } else {
                if (permissionType === 'all') {
                  userGroupModelPerms = new Set();
                } else {
                  userGroupModelPerms.delete(event.value);
                }
              }

              return ({
                ...modelPerms,
                permissions: [...userGroupModelPerms],
              });
            }

            return modelPerms;
          });

        return ({
          ...prevGroupPermissions,
          [group]: {
            ...prevGroupPermissions[group],
            group_permissions: userGroupPerms,
          },
        });
      });
    };

    const isChecked = () => {
      const selectedPermissions = new Set(
        groupPermissions?.[group]?.group_permissions
          ?.find?.((perms) => (
            perms.content_type_name.replaceAll(' ', '') === model
          ))
          ?.permissions,
      );

      if (permissionType === 'all') {
        const allPerms = ['add', 'change', 'delete', 'view'];
        return allPerms.every((perm) => selectedPermissions.has(perm));
      }

      return selectedPermissions.has(permissionType);
    };

    return (
      <div className="flex flex-wrap gap-3">
        <div className="flex align-items-center">
          <Checkbox
            id={`${group}-${model}-${permissionType}`}
            inputId={`${group}-${model}-${permissionType}`}
            disabled={action === 'view'}
            name={permissionType}
            value={permissionType}
            onChange={handleChange}
            checked={isChecked()}
          />
        </div>
      </div>
    );
  };

  const columns = [
    { header: 'Permission', field: 'model' },
    { header: 'View', body: (rowData) => getPermissions(rowData, 'view') },
    { header: 'Add', body: (rowData) => getPermissions(rowData, 'add') },
    { header: 'Change', body: (rowData) => getPermissions(rowData, 'change') },
    { header: 'Delete', body: (rowData) => getPermissions(rowData, 'delete') },
    { header: 'Select All', body: (rowData) => getPermissions(rowData, 'all') },
  ];

  const handleSubmit = (event = null) => {
    event?.preventDefault();
    const url = `${api.groupPermission}/${group}`;
    const method = 'PUT';
    const data = Object.values(groupPermissions).find((perms) => (
      perms.group === group
    ));
    updatedPermissions.sendRequest({ url, method, data });
  };

  useEffect(() => {
    if (updatedPermissions.status === 200) {
      const message = 'Permissions updated succesfully';
      permissionsAlert.setAlert('success', message);
      updatedPermissions.resetResponse();
    }
  }, [updatedPermissions.status, permissionsAlert]);

  useEffect(() => {
    if (!pathname.includes('edit')) {
      permissionsAlert.resetAlert();
    }
  }, [pathname]);

  const getButtons = () => {
    if (action === 'view') {
      return [{
        label: 'Edit',
        icon: 'pencil',
        onClick: () => navigate('edit'),
      }];
    }
    return [{
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate('/permission/group'),
      extraClasses: 'p-button-outlined',
    }];
  };

  return (
    <>
      <PageHeader title={title} buttons={getButtons()} />
      <div className="my-4">
        <Dropdown
          inputId="user-group"
          value={group}
          onChange={(e) => setGroup(e.value)}
          options={permissions.data?.results}
          optionLabel="group_name"
          optionValue="group"
          placeholder="Select User Role"
          className="w-full md:w-14rem"
        />
      </div>
      {group && (
        <>
          <ListDataTable
            data={contentTypes.data}
            header={renderHeader}
            globalFilterFields={['model']}
            columns={columns}
            filters={filters}
          />
          {action === 'edit' && (
            <>
              <Button
                className="mt-4"
                onClick={handleSubmit}
              >
                Submit
              </Button>
              {permissionsAlert.getAlert}
            </>
          )}
        </>
      )}
      {permissions.getApiAlert}
    </>
  );
}

export default ListPermissionContainer;
