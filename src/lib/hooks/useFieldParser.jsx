import React, { useEffect, useState } from 'react';
import { Chip } from 'primereact/chip';
import { Image } from 'primereact/image';
import { Button } from 'primereact/button';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import useSchema from './useSchema';
import useApi from './useApi';
import { convertToDatetime } from '../utils/handleTime';

function useFieldParser(config, resourceId, showHeader = true) {
  dayjs.extend(utc);
  let { api } = config;
  const { schemaUrl = api } = config;
  if (resourceId) {
    api = `${config.api}/${resourceId}`;
  }
  const { schema, relatedData, getSchemaAlert } = useSchema(schemaUrl);
  const { data, status, getApiAlert, sendRequest } = useApi(api);
  const [displayData, setDisplayData] = useState(null);
  const excludedFields = config?.excludedFields ?? [
    'id',
    'name',
    'description',
  ];

  const parseField = (fieldInfo, fieldValue, relatedField) => {
    let parsedValue;
    const {
      type,
      choices,
      native_type: nativeType,
    } = fieldInfo;

    if (!fieldValue && type !== 'boolean') {
      parsedValue = fieldValue;
    } else if (type === 'boolean') {
      parsedValue = fieldValue ? 'Yes' : 'No';
    } else if (type === 'choice') {
      const choice = choices.find(({ value }) => (
        value === fieldValue
      ));
      parsedValue = choice?.display_name;
    } else if (type === 'date') {
      parsedValue = dayjs(fieldValue).format('DD/MM/YYYY');
    } else if (type === 'datetime') {
      parsedValue = dayjs(fieldValue).format('DD/MM/YYYY hh:mm A');
    } else if (type === 'time') {
      const datetime = convertToDatetime(fieldValue);
      parsedValue = dayjs(datetime).format('hh:mm A');
    } else if (type === 'field' && nativeType === 'PrimaryKeyRelatedField') {
      if (relatedField) {
        for (const related of relatedField) {
          if (related.id === fieldValue) {
            parsedValue = (
              <Chip
                label={related.name}
                className="bg-indigo-400 text-indigo-50"
              />
            );
            break;
          }
        }
      }
    } else if (type === 'field' && nativeType === 'ManyRelatedField') {
      if (relatedField) {
        const element = [];
        for (const val of fieldValue) {
          for (const related of relatedField) {
            if (related.id === val) {
              element.push(
                <Chip
                  key={related.id}
                  label={related.name}
                  className="mr-1 my-1 bg-indigo-400 text-indigo-50"
                />,
              );
            }
          }
        }
        parsedValue = element;
      }
    } else if (type === 'field' && nativeType === 'JSONField') {
      if (Array.isArray(fieldValue)) {
        parsedValue = `${fieldValue.join('% : ')}${'%'}`;
      } else {
        parsedValue = Object.entries(fieldValue).map(([key, val]) => ` ${key}: ${val}`).join();
      }
    } else if (type === 'image upload') {
      parsedValue = (
        <Image
          src={fieldValue}
          zoomSrc={fieldValue}
          alt="Image"
          width="72"
          height="72"
          preview
        />
      );
    } else if (type === 'file upload') {
      parsedValue = (
        <Button
          link
          label="View File"
          icon="pi pi-external-link"
          className="p-0"
          onClick={() => window.open(fieldValue)}
        />

      );
    } else {
      parsedValue = fieldValue;
    }
    return parsedValue;
  };

  useEffect(() => {
    if (schema) {
      let fields = {};
      Object.entries(schema).filter(([fieldName]) => (
        !excludedFields?.includes(fieldName)
      )).map(([fieldName, fieldInfo]) => {
        fields = {
          ...fields,
          [fieldName]: {
            label: fieldInfo.label,
            value: parseField(
              fieldInfo,
              data?.[fieldName],
              relatedData?.[fieldName],
            ),
          },
        };
      });

      let title;
      let subtitle;
      const { headerFields } = config;
      const bodyFields = { ...fields };
      if (showHeader && headerFields) {
        const { titleField, subtitleField } = headerFields;
        title = bodyFields[titleField]?.value;
        subtitle = bodyFields[subtitleField]?.value;
        delete bodyFields[titleField];
        delete bodyFields[subtitleField];
      }

      setDisplayData({
        headerFields: (showHeader && headerFields) ? { title, subtitle } : null,
        fields: bodyFields,
      });

      return () => {
        setDisplayData(null);
      };
    }
  }, [
    schema,
    data,
    relatedData,
  ]);

  return {
    parseField,
    rawData: data,
    displayData,
    status,
    getSchemaAlert,
    getApiAlert,
    sendRequest,
  };
}

export default useFieldParser;
