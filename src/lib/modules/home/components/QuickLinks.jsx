import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Card } from 'primereact/card';
import FacultyQuickLinks from './FacultyQuickLinks';
import AccountantQuickLinks from './AccountantQuickLinks';
import ManagerQuickLinks from './ManagerQuickLinks';
import AdminQuickLinks from './AdminQuickLinks';

function QuickLinks({ userRole }) {
  const navigate = useNavigate();
  const [quickLinks, setQuickLinks] = useState([]);

  useEffect(() => {
    setQuickLinks(() => {
      if (userRole === 'Faculty') {
        return FacultyQuickLinks;
      }
      if (userRole === 'Accountant') {
        return AccountantQuickLinks;
      }
      if (userRole === 'Manager') {
        return ManagerQuickLinks;
      }
      if (userRole === 'Admin') {
        return AdminQuickLinks;
      }
      return [];
    });
  }, [userRole]);

  return (
    <Card title="Quick Links" className="surface-ground mt-2">
      <div className="grid">
        {quickLinks?.map?.((page) => (
          <div
            className="col-12 md:col-6 lg:col-3"
            style={{ cursor: 'pointer' }}
            onClick={() => navigate(page.link)}
          >
            <div className="surface-0 shadow-2 p-3 border-1 border-50 border-round">
              <div className="flex justify-content-between">
                <div>
                  <div className="text-900 font-medium text-xl mt-2">
                    {page.label}
                  </div>
                </div>
                <div className="flex align-items-center justify-content-center bg-blue-100 border-round" style={{ width: '2.5rem', height: '2.5rem' }}>
                  <i className="pi pi-star text-blue-500 text-xl" />
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </Card>
  );
}

export default QuickLinks;
