import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

dayjs.extend(utc);

const convertToDatetime = (time) => {
  // If input is already a valid date object, return unmodified
  if (!Number.isNaN(new Date(time).getTime())) {
    return time;
  }

  const today = new Date();
  const datetime = `${today.getFullYear()}`
    + `-${today.getMonth()}`
    + `-${today.getDate()}`
    + ` ${time}`;
  return dayjs(datetime).toDate();
};

export { convertToDatetime };
