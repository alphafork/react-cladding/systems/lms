import api from '../api';

export default {
  'profile/name': {
    title: 'Account',
    api: api.userAccount,
    excludedFields: [
      'id',
      'username',
      'groups',
      'user_permissions',
    ],
  },
  'profile/bio': {
    title: 'Bio',
    api: api.userProfile,
    excludedFields: [
      'id',
      'user',
    ],
  },
  'profile/contact': {
    title: 'Contact',
    api: api.userContact,
    excludedFields: [
      'user',
      'is_verified',
    ],
  },
};
