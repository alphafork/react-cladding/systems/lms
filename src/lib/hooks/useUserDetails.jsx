import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import useStore from './useStore';
import useApi from './useApi';
import api from '../config/api';

function useUserDetails() {
  const navigate = useNavigate();
  const user = useStore((state) => state.user);
  const setUser = useStore((state) => state.setUser);
  const userDetail = useApi(api.userDetail);
  const userPhoto = useApi();
  const student = useApi();
  const staff = useApi();
  const [userRole, setUserRole] = useState(null);

  useEffect(() => {
    if (userDetail.status === 401) navigate('/login');

    const {
      id,
      username,
      email,
      first_name: firstName,
      last_name: lastName,
    } = userDetail.data ?? {};

    if (id) {
      let fullName = firstName;
      fullName += lastName ? ` ${lastName}` : '';
      setUser({
        ...user,
        id,
        username,
        firstName,
        lastName,
        fullName,
        email,
      });

      window.localStorage.setItem('USER_ID', id);
      const studentUrl = `${api.student}/${id}`;
      student.sendRequest({ url: studentUrl });
      const userPhotoUrl = `${api.userProfile}/${id}`;
      userPhoto.sendRequest({ url: userPhotoUrl });
    }
  }, [userDetail.status, navigate]);

  useEffect(() => {
    const photo = userPhoto.data?.photo;
    if (photo) {
      setUser({
        ...user,
        photo,
      });
    }
  }, [userPhoto.data?.photo]);

  useEffect(() => {
    if (!student.status || student.status === 'loading') return;
    if (student.status === 200) {
      setUserRole('Student');
    } else {
      const staffUrl = `${api.staffDesignation}`
        + `?user=${userDetail.data?.id}`
        + '&is_default=1';
      staff.sendRequest({ url: staffUrl });
    }
  }, [student.status, userDetail.data?.id]);

  useEffect(() => {
    if (staff.data?.results?.[0]?.designation_name) {
      setUserRole(staff.data.results[0].designation_name);
    }
  }, [staff.data?.results]);

  useEffect(() => {
    if (userRole) {
      setUser({
        ...user,
        role: userRole,
      });
    }
  }, [userRole]);

  return user;
}

export default useUserDetails;
