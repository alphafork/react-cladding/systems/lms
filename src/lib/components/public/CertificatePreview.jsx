import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { PDFViewer } from '@react-pdf/renderer';
import { ProgressSpinner } from 'primereact/progressspinner';
import CertificatePdfTemplate from '../../shared/program-certificate/components/CertificatePdfTemplate';
import PageNotFound from './PageNotFound';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function CertificatePreview() {
  const { uuid } = useParams();
  const certificate = useApi();

  useEffect(() => {
    const url = `${api.publicCertificate}/${uuid}`;
    const extraConfig = {
      headers: { Accept: 'application/json' },
    };
    certificate.sendRequest({ url, extraConfig });
  }, [uuid]);

  if (certificate.status === 404) {
    return (<PageNotFound />);
  }

  if (certificate.loading === true || !certificate.data) {
    return (
      <div className="flex justify-content-center">
        <ProgressSpinner />
      </div>
    );
  }

  return (
    <PDFViewer style={{ width: '100vw', height: '100vh' }}>
      <CertificatePdfTemplate certificate={certificate.data} />
    </PDFViewer>
  );
}

export default CertificatePreview;
