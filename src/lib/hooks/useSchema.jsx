import { useEffect, useState } from 'react';
import apiClient from '../utils/api';
import handleError from '../utils/handleError';
import useAlert from './useAlert';
import useApi from './useApi';

function useSchema(url, queryString, authorized = true) {
  const { getAlert: getRelatedDataAlert, setAlert } = useAlert();
  const [schema, setSchema] = useState(null);
  const [relatedData, setRelatedData] = useState({});
  const { data, getApiAlert: getSchemaAlert } = useApi(url, 'OPTIONS', authorized);

  useEffect(() => {
    const { GET, POST, PUT } = data?.actions ?? {};
    if (data) {
      setSchema(POST ?? PUT ?? GET);
    }

    return () => {
      setSchema(null);
    };
  }, [data]);

  useEffect(() => {
    (async () => {
      if (!schema) {
        return;
      }

      for (const field of Object.keys(schema)) {
        const {
          native_type: nativeType,
          related_model_url: relatedModelUrl,
        } = schema[field];

        if (
          nativeType === 'PrimaryKeyRelatedField'
          || nativeType === 'ManyRelatedField'
        ) {
          // Retrieve related fields
          const qs = queryString ? `?${queryString}` : '';
          const relatedUrl = `${relatedModelUrl}/${qs}`;
          const [response, error, status] = await apiClient({ url: relatedUrl });
          if (status === 200) {
            setRelatedData((prevRelatedData) => ({
              ...prevRelatedData,
              [field]: response?.results ?? response,
            }));
          }
          if (error) {
            handleError(error, setAlert);
          }
        }
      }
    })();
  }, [schema]);

  return {
    schema,
    relatedData,
    getSchemaAlert,
    getRelatedDataAlert,
  };
}

export default useSchema;
