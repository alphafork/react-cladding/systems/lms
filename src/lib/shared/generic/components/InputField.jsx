import React, { useEffect, useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputSwitch } from 'primereact/inputswitch';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { AutoComplete } from 'primereact/autocomplete';
import { ListBox } from 'primereact/listbox';
import { Password } from 'primereact/password';
import { Chips } from 'primereact/chips';
import { Tag } from 'primereact/tag';
import { Image } from 'primereact/image';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { convertToDatetime } from '../../../utils/handleTime';

function InputField({
  fieldName,
  fieldInfo,
  fieldAlert,
  formData,
  relatedData,
  eventHandlers,
}) {
  const {
    type,
    choices,
    max_length: maxLength,
    read_only: readOnly,
    native_type: nativeType,
  } = fieldInfo;
  const [filteredItems, setFilteredItems] = useState({});
  const [selectedItems, setSelectedItems] = useState(null);

  useEffect(() => {
    setFilteredItems(relatedData);
    const _selectedItems = {};
    Object.keys(relatedData).map((field) => {
      _selectedItems[field] = relatedData?.[field]?.filter?.((item) => (
        Array.isArray(formData?.[field]) && formData[field].includes(item.id)
      ));
    });
    setSelectedItems(_selectedItems);
  }, [formData, relatedData]);

  const search = (e, field) => {
    const _filteredItems = {};
    if (!e.query.trim().length) {
      _filteredItems[field] = [...relatedData[field]];
    } else {
      _filteredItems[field] = relatedData[field].filter((item) => (
        item.name.toLowerCase().startsWith(e.query.toLowerCase())
      ));
    }
    setFilteredItems((prevFilteredItems) => ({
      ...prevFilteredItems,
      [field]: _filteredItems[field],
    }));
  };

  const onMultiSelectChange = (e, field) => {
    setSelectedItems((prevSelectedItems) => ({
      ...prevSelectedItems,
      [field]: e.value,
    }));
    eventHandlers.onChange(e);
  };

  let element;
  let overrideOnChange = false;
  let widthStyle = 'w-5';

  if (type === 'boolean') {
    element = (<InputSwitch type="checkbox" checked={formData?.[fieldName] ?? false} disabled={readOnly} />);
    widthStyle = null;
  } else if (type === 'date') {
    element = (<Calendar showIcon showButtonBar value={formData?.[fieldName] && new Date(formData[fieldName])} dateFormat="dd/mm/yy" disabled={readOnly} />);
  } else if (type === 'datetime') {
    element = (<Calendar showIcon showButtonBar value={formData?.[fieldName] && new Date(formData[fieldName])} dateFormat="dd/mm/yy" showTime hourFormat="12" disabled={readOnly} />);
  } else if (type === 'time') {
    const datetime = convertToDatetime(formData?.[fieldName]);
    element = (<Calendar value={formData?.[fieldName] && datetime} timeOnly hourFormat="12" disabled={readOnly} />);
  } else if (type === 'choice') {
    element = (<Dropdown type="choices" value={formData?.[fieldName]} options={choices} optionLabel="display_name" placeholder="Select" showClear disabled={readOnly} />);
  } else if (type === 'multiple choice') {
    element = (<ListBox multiple value={formData?.[fieldName]} options={choices} optionLabel="display_name" disabled={readOnly} />);
  } else if (type === 'field' && nativeType === 'PrimaryKeyRelatedField') {
    element = (<Dropdown type="choices" value={formData?.[fieldName]} options={relatedData?.[fieldName]} optionLabel="name" optionValue="id" placeholder="Select" showClear disabled={readOnly} />);
  } else if (type === 'field' && nativeType === 'ManyRelatedField') {
    element = (<AutoComplete type="choices" multiple dropdown value={selectedItems?.[fieldName]} suggestions={filteredItems?.[fieldName]} field="name" completeMethod={(e) => search(e, fieldName)} placeholder="Multi Select" onChange={(e) => onMultiSelectChange(e, fieldName)} disabled={readOnly} />);
    overrideOnChange = true;
    widthStyle += ' p-fluid';
  } else if (type === 'field' && nativeType === 'ArrayField') {
    element = (<Chips value={formData?.[fieldName] ?? []} separator="," />);
  } else if (type === 'password') {
    element = (<Password className="mb-4 w-4" />);
  } else if (type === 'image upload') {
    let previewElement;
    if (formData?.[fieldName] && typeof formData[fieldName] === 'string') {
      const imgSrc = new URL(formData[fieldName]);
      previewElement = (
        <Image src={imgSrc} zoomSrc={imgSrc} alt="Image" width="72" height="72" preview />
      );
    }
    const headerElement = (options) => (
      <div className={options.className}>
        {options.chooseButton}
        <Button label="Delete" icon="pi pi-trash" severity="danger" onClick={(e) => eventHandlers.onDeleteFile(e, fieldName)} disabled={!formData?.[fieldName]} />
      </div>
    );
    const itemElement = (file, props) => (
      formData?.[fieldName] && (
        <div className="flex align-items-center flex-wrap">
          <div className="flex align-items-center" style={{ width: '80%' }}>
            <img alt={file.name} role="presentation" src={file.objectURL} width={72} />
            <div className="flex flex-column ml-3 overflow-hidden text-overflow-ellipsis text-left">
              {file.name}
              <Tag value={props.formatSize} severity="warning" style={{ width: '72px' }} />
            </div>
          </div>
          <Button type="button" icon="pi pi-times" className="p-button-outlined p-button-rounded p-button-danger ml-auto" onClick={() => props.onRemove()} />
        </div>
      )
    );
    element = (
      <FileUpload accept="image/*" customUpload emptyTemplate={previewElement} headerTemplate={headerElement} onSelect={(e) => eventHandlers.onSelectFile(e, fieldName)} onRemove={() => eventHandlers.onCancelFile(fieldName)} itemTemplate={itemElement} />
    );
    overrideOnChange = true;
  } else if (type === 'file upload') {
    let previewElement;
    if (formData?.[fieldName] && typeof formData[fieldName] === 'string') {
      previewElement = (
        <Button
          link
          label="View File"
          icon="pi pi-external-link"
          className="p-0"
          onClick={() => window.open(formData[fieldName])}
        />
      );
    }
    const headerElement = (options) => (
      <div className={options.className}>
        {options.chooseButton}
        <Button label="Delete" icon="pi pi-trash" severity="danger" onClick={(e) => eventHandlers.onDeleteFile(e, fieldName)} disabled={!formData?.[fieldName]} />
      </div>
    );
    const itemElement = (file, props) => (
      formData?.[fieldName] && (
        <div className="flex align-items-center flex-wrap">
          <div className="flex align-items-center" style={{ width: '80%' }}>
            <div className="flex flex-column ml-3 overflow-hidden text-overflow-ellipsis text-left">
              {file.name}
              <Tag value={props.formatSize} severity="warning" style={{ width: '72px' }} />
            </div>
          </div>
          <Button type="button" icon="pi pi-times" className="p-button-outlined p-button-rounded p-button-danger ml-auto" onClick={() => props.onRemove()} />
        </div>
      )
    );
    element = (
      <FileUpload accept=".pdf" customUpload emptyTemplate={previewElement} headerTemplate={headerElement} onSelect={(e) => eventHandlers.onSelectFile(e, fieldName)} onRemove={() => eventHandlers.onCancelFile(fieldName)} itemTemplate={itemElement} />
    );
    overrideOnChange = true;
  } else if (type === 'string' && !maxLength) {
    element = (<InputTextarea autoResize value={formData?.[fieldName] || ''} disabled={readOnly} />);
    widthStyle = 'w-full';
  } else {
    element = (<InputText type="string" value={formData?.[fieldName] || ''} disabled={readOnly} />);
    widthStyle = 'w-full';
  }

  const extraProps = {
    className: `${widthStyle}`,
    id: fieldName,
    name: fieldName,
  };

  if (!overrideOnChange) {
    extraProps.onChange = eventHandlers?.onChange;
  }

  const inputElement = React.cloneElement(
    element,
    extraProps,
  );

  const alertElement = (
    <div
      className="mt-2 mb-5"
      style={{ display: 'block' }}
    >
      {fieldAlert}
    </div>
  );

  return [
    inputElement,
    alertElement,
  ];
}

export default InputField;
