import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { SelectButton } from 'primereact/selectbutton';
import PageHeader from '../PageHeader';
import List from '../crud/List';
import useApi from '../../hooks/useApi';
import getConfig from '../../utils/getConfig';

function ListContainer() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const { api, headerFields } = getConfig('staff-designation');
  const staff = useApi();
  const [bodyButtons, setBodyButtons] = useState(null);
  let initialStaffOption = 'Permanent';
  if (state?.isPermanent === false) {
    initialStaffOption = 'Temporary';
  }
  const [staffOption, setStaffOption] = useState(initialStaffOption);

  useEffect(() => {
    let filterValue = 1;
    if (staffOption === 'Temporary') {
      filterValue = 0;
    }
    const url = `${api}?filter_by={"is_permanent":${filterValue}}`;
    staff.sendRequest({ url });
  }, [staffOption]);

  useEffect(() => {
    if (!staff.data?.results) {
      return;
    }

    let buttons = {};
    for (const item of staff.data.results) {
      buttons = {
        ...buttons,
        view: {
          ...buttons?.view,
          [item.id]: {
            label: 'View',
            icon: 'eye',
            severity: 'secondary',
            onClick: (id) => (navigate(`/staff/${id}`)),
          },
        },
      };
    }
    setBodyButtons(buttons);

    return () => {
      setBodyButtons(null);
    };
  }, [staff.data?.results, navigate]);

  const selectButton = (
    <SelectButton
      className="mb-4"
      value={staffOption}
      onChange={(e) => setStaffOption(e.value)}
      options={['Permanent', 'Temporary']}
    />
  );

  return (
    <>
      <PageHeader
        title="Staff List"
        selectButton={selectButton}
      />
      <List
        headerFields={headerFields}
        data={staff.data}
        buttons={bodyButtons}
      />
      { staff.getApiAlert }
    </>
  );
}

export default ListContainer;
