import api from '../api';

export default {
  'student/guardian': {
    title: 'Guardian',
    api: api.studentGuardian,
    headerFields: false,
    excludedFields: [
      'created_at',
      'updated_at',
      'student',
    ],
  },
  'student/employment': {
    title: 'Employment',
    api: api.studentEmployment,
    excludedFields: [
      'student',
    ],
  },
  'student/placement': {
    api: api.studentPlacement,
    excludedFields: [
      'id',
      'student',
    ],
  },
};
