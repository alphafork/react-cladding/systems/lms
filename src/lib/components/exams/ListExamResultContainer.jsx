import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { Chip } from 'primereact/chip';
import { InputNumber } from 'primereact/inputnumber';
import { ToggleButton } from 'primereact/togglebutton';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { Badge } from 'primereact/badge';
import PageHeader from '../PageHeader';
import DetailStudentPopup from '../students/DetailStudentPopup';
import ListDataTable from '../datatable/ListDataTable';
import FormContainer from '../crud/FormContainer';
import useAlert from '../../hooks/useAlert';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListExamResultContainer() {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  let action = 'view';
  if (pathname.includes('create')) action = 'create';
  const { batchId, examId } = useParams();
  if (Number.isNaN(batchId)) navigate('/');
  const studentsUrl = `${api.batchStudent}?filter_by={"batch": ${batchId}}`;
  const students = useApi(studentsUrl);
  const studentDetail = useApi();
  const examUrl = `${api.exam}/${examId}`;
  const exam = useApi(examUrl);
  const batchExamResult = useApi();
  const editResultAlert = useAlert();
  const [result, setResult] = useState(null);
  const [activeStudent, setActiveStudent] = useState(null);
  const [activeRemarks, setActiveRemarks] = useState(null);

  const [showPopup, setShowPopup] = useState({
    studentDetail: false,
    remarksForm: false,
    editForm: false,
    remarksDetail: false,
  });

  const title = 'Exam Result';

  useEffect(() => {
    if (action === 'view') {
      const url = `${api.examResult}?filter_by={"batch_student__batch":${batchId},"exam":${examId}}`;
      if (!batchExamResult.data?.results) {
        batchExamResult.sendRequest({ url });
      }
    }
  }, [batchExamResult.data?.results]);

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  useEffect(() => {
    if (students.data?.results) {
      students.data.results.map((student) => {
        setResult((prevResult) => ({
          ...prevResult,
          [student.registration.number]: {
            marks: null,
            isPresent: true,
            remarks: '',
            exam: Number(examId),
            batchStudent: Number(student.id),
          },
        }));
      });
    }
  }, [students.data?.results]);

  useEffect(() => {
    if (!result || batchExamResult.status !== 200) return;
    const existingResult = batchExamResult.data.results;
    setResult((prevResult) => {
      const updatedResult = { ...prevResult };
      for (const student in updatedResult) {
        const studentResult = existingResult.find(
          (studResult) => (
            studResult.batch_student === prevResult[student].batchStudent
          ),
        );
        updatedResult[student].resultId = studentResult?.id;
        updatedResult[student].isPresent = studentResult?.is_present;
      }
      return updatedResult;
    });
  }, [batchExamResult.status, batchExamResult.data?.results]);

  const onClickStudent = (userId) => {
    const url = `${api.userAccount}/${userId}`;
    studentDetail.sendRequest({ url });
    togglePopup('studentDetail');
  };

  const getStudent = (rowData) => (
    <Chip
      label={rowData.student.name}
      className="cursor-pointer bg-indigo-400 text-indigo-50"
      onClick={() => onClickStudent(rowData.student.id)}
    />
  );

  const getAttendance = (rowData) => {
    const {
      id: batchStudentId,
      registration: {
        number: registration,
      },
    } = rowData;

    const handleChange = (event) => {
      if (event.value === false) {
        togglePopup('remarksForm');
      }
      setActiveStudent(registration);
      setResult((prevResult) => ({
        ...prevResult,
        [registration]: {
          ...prevResult[registration],
          isPresent: event.value,
        },
      }));
    };

    if (action === 'view') {
      const studentAttendance = batchExamResult.data?.results?.find((student) => (
        student.batch_student === batchStudentId
      ));
      const value = studentAttendance?.is_present ? 'Present' : 'Absent';
      const severity = value === 'Present' ? 'success' : 'danger';
      return (
        <Badge value={value} severity={severity} />
      );
    }

    return (
      <ToggleButton
        onIcon="pi pi-check"
        onLabel="Present"
        offIcon="pi pi-times"
        offLabel="Absent"
        checked={result?.[registration]?.isPresent}
        onChange={handleChange}
      />
    );
  };

  const getMarks = (rowData) => {
    const {
      id: batchStudentId,
      registration: {
        number: registration,
      },
    } = rowData;

    const handleChange = (event) => {
      setActiveStudent(registration);
      setResult((prevResult) => ({
        ...prevResult,
        [registration]: {
          ...prevResult[registration],
          marks: event.value,
        },
      }));
    };

    const studentResult = batchExamResult.data?.results?.find((student) => (
      student.batch_student === batchStudentId
    ));

    if (action === 'view') {
      const value = studentResult?.marks;
      const severity = value >= exam.data?.marks_to_qualify ? 'success' : 'danger';

      const onClickMarks = () => {
        if (!studentResult?.remarks) return;
        setActiveRemarks(studentResult.remarks);
        togglePopup('remarksDetail');
      };

      return (
        <Badge
          value={value}
          severity={severity}
          style={{ cursor: studentResult?.remarks ? 'pointer' : 'auto' }}
          onClick={onClickMarks}
        />
      );
    }

    const defaultValue = result?.[registration]?.isPresent ? result?.[registration]?.marks : 0;

    return (
      <InputNumber
        value={defaultValue}
        onChange={handleChange}
        disabled={!result?.[registration]?.isPresent}
      />
    );
  };

  const editResult = (rowData) => {
    togglePopup('editForm');
    setActiveStudent(rowData.registration.number);
  };

  const getEditButton = (rowData) => (
    <Button
      label="Edit"
      icon="pi pi-pencil"
      size="small"
      onClick={() => editResult(rowData)}
    />
  );

  const columns = [
    { header: 'Registration No', field: 'registration.number' },
    { header: 'Student', body: getStudent },
    { header: 'Email', field: 'student.email' },
    { header: 'Attendance', body: getAttendance },
    { header: 'Marks', body: getMarks },
  ];

  if (action === 'view') {
    columns.push({
      header: 'Action',
      body: getEditButton,
    });
  }

  const handleResult = () => {
    const url = api.batchExamResult;
    const method = 'POST';
    const data = {
      exam: Number(examId),
      exam_result: Object.values(result).map((value) => ({
        exam: value.exam,
        is_present: value.isPresent,
        marks: value.isPresent ? (value.marks ?? 0) : 0,
        batch_student: value.batchStudent,
        remarks: value.remarks,
      })),
    };
    batchExamResult.sendRequest({
      url,
      method,
      data,
    });
  };

  const handleCancel = () => {
    navigate(`/batch/${batchId}`);
  };

  const handleRemarksChange = (event) => {
    setResult((prevResult) => ({
      ...prevResult,
      [activeStudent]: {
        ...prevResult[activeStudent],
        remarks: event.target.value,
      },
    }));
  };

  const handleRemarksSubmit = (event = null) => {
    event?.preventDefault();
    togglePopup('remarksForm');
  };

  useEffect(() => {
    if (batchExamResult.status === 201) {
      navigate(`/batch/${batchId}`);
    }
  }, [batchExamResult.status]);

  const buttons = () => {
    if (action === 'view') {
      return [
        {
          label: 'Back',
          icon: 'angle-left',
          onClick: () => navigate(`/batch/${batchId}`),
          extraClasses: 'p-button-outlined',
        },
      ];
    }
    return false;
  };

  const eventHandlers = {
    onSuccess: () => {
      togglePopup('editForm');
      const url = `${api.examResult}?filter_by={"batch_student__batch":${batchId},"exam":${examId}}`;
      batchExamResult.sendRequest({ url });
      editResultAlert.setAlert(
        'success',
        'Exam result updated succesfully',
      );
    },
    onCancel: () => togglePopup('editForm'),
  };

  return (
    <>
      <PageHeader title={title} buttons={buttons()} />
      <DetailStudentPopup
        visible={showPopup.studentDetail}
        onHide={() => togglePopup('studentDetail')}
        student={studentDetail.data}
      />

      <ListDataTable
        data={students.data}
        columns={columns}
      />

      <Dialog
        header="Exam remarks"
        visible={showPopup.remarksForm}
        style={{ width: '50vw' }}
        onHide={handleRemarksSubmit}
      >
        <form>
          <div className="flex flex-column gap-2">
            <label
              htmlFor="remarks"
              className="block text-900 font-medium mt-4 mb-2"
            >
              Remarks
            </label>
            <InputTextarea
              autoResize
              value={result?.[activeStudent]?.remarks}
              onChange={handleRemarksChange}
              rows={5}
              cols={30}
            />
            <div className="mt-2">
              <Button
                type="submit"
                label="Submit"
                className="mr-2 mb-2"
                onClick={handleRemarksSubmit}
              />
            </div>
          </div>
        </form>
      </Dialog>

      <Dialog
        header="Edit exam result"
        visible={showPopup.editForm}
        style={{ width: '50vw' }}
        onHide={() => togglePopup('editForm')}
      >
        <FormContainer
          path="exam-result"
          resourceId={result?.[activeStudent]?.resultId}
          defaultData={{
            exam: result?.[activeStudent]?.exam,
            batch_student: result?.[activeStudent]?.batchStudent,
          }}
          eventHandlers={eventHandlers}
        />
      </Dialog>

      {(action === 'view') && (
        <Dialog
          header="Remarks"
          visible={showPopup.remarksDetail}
          style={{ width: '50vw' }}
          onHide={() => togglePopup('remarksDetail')}
        >
          {activeRemarks}
        </Dialog>
      )}

      {(action === 'create') && (
        <div className="mt-4">
          <Button
            className="mr-2 mb-2"
            onClick={handleResult}
          >
            Submit Exam Result
          </Button>
          <Button
            label="Cancel"
            className="mr-2 mb-2 p-button-outlined"
            onClick={handleCancel}
          />
        </div>
      )}

      {batchExamResult.getApiAlert}
      {editResultAlert.getAlert}
    </>
  );
}

export default ListExamResultContainer;
