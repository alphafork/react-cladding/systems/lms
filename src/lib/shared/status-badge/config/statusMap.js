export default {
  registrationStatus: {
    applied: 'danger',
    approved: 'warning',
    completed: 'success',
    deregistered: 'info',
    discontinued: 'info',
  },
  batchStudentStatus: {
    unenrolled: 'warning',
    active: 'primary',
    migrated: 'warning',
    discontinued: 'danger',
    completed: 'success',
  },
  paymentStatus: {
    due: 'warning',
    paid: 'success',
    extension: 'info',
    overdue: 'danger',
  },
};
