import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

const handleFormChange = (event, fieldType, nativeType, setFormData) => {
  dayjs.extend(utc);
  const { name } = event.target;
  let { value } = event.target;
  const isDate = fieldType === 'date';
  const isDateTime = fieldType === 'datetime';
  const isTime = fieldType === 'time';
  const isMultiSelect = nativeType === 'ManyRelatedField';
  const isJson = nativeType === 'JSONField';

  if (value instanceof Date) {
    value = isDate ? dayjs(value).format('YYYY-MM-DD') : value;
    value = isDateTime ? dayjs.utc(value).format('YYYY-MM-DDTHH:mm:ss[Z]') ?? false : value;
    value = isTime ? dayjs(value).set('second', 0).format('HH:mm') : value;
  }
  value = isMultiSelect ? value.map((item) => item.id) : value;

  if (isJson) {
    value = value.split(',');
  }

  setFormData((prevFormData) => ({
    ...prevFormData,
    [name]: value,
  }));
};

export default handleFormChange;
