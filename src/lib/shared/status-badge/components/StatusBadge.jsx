import React from 'react';
import { Tag } from 'primereact/tag';
import statusMap from '../config/statusMap';

function StatusBadge({ statusType, status }) {
  const value = `${status[0].toUpperCase?.()}${status.slice?.(1)}`;
  const severity = statusMap[statusType][status];

  return (
    <Tag
      value={value}
      severity={severity}
    />
  );
}

export default StatusBadge;
