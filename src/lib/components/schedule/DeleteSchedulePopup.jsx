import React, { useState } from 'react';
import { Dialog } from 'primereact/dialog';
import { RadioButton } from 'primereact/radiobutton';
import { Button } from 'primereact/button';
import api from '../../config/api';

function DeleteSchedulePopup({
  visible,
  onHide,
  event,
  confirmDelete,
}) {
  const options = [
    { key: 0, value: 'Single' },
    { key: 1, value: 'All future instances' },
    { key: 2, value: 'All future instances of same time range' },
  ];
  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleSubmit = (e) => {
    e.preventDefault();
    let apiUrl = `${api.deleteSchedule}/${event.id}`;
    if (selectedOption.key !== 0) {
      apiUrl += '?delete_multiple=';
    }
    if (selectedOption.key === 1) {
      apiUrl += 'all_future_instances';
    }
    if (selectedOption.key === 2) {
      apiUrl += 'all_future_instances_of_time_range';
    }
    const method = 'GET';
    confirmDelete(apiUrl, method);
  };

  return (
    <Dialog
      header="Delete schedule"
      visible={visible}
      style={{ width: '50vw' }}
      onHide={onHide}
    >
      <form>
        <div className="flex flex-column gap-2">
          {options.map((option) => (
            <div key={option.key} className="flex align-items-center">
              <RadioButton
                inputId={option.key}
                name="option"
                value={option}
                onChange={(e) => setSelectedOption(e.value)}
                checked={selectedOption.key === option.key}
              />
              <label htmlFor={option.key} className="ml-2">{option.value}</label>
            </div>
          ))}
        </div>
        <div className="mt-4">
          <Button
            type="submit"
            label="Submit"
            className="mr-2 mb-2"
            onClick={handleSubmit}
          />
        </div>
      </form>
    </Dialog>
  );
}

export default DeleteSchedulePopup;
