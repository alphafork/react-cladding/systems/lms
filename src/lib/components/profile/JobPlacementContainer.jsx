import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Card } from 'primereact/card';
import { Dialog } from 'primereact/dialog';
import { ConfirmDialog } from 'primereact/confirmdialog';
import PageHeader from '../PageHeader';
import DetailProfileSection from './DetailProfileSection';
import FormContainer from '../crud/FormContainer';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function JobPlacementContainer() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const studentId = state?.studentId ?? window.localStorage.getItem('USER_ID');
  const placement = useApi(`${api.studentPlacement}/?{filter_by="student":${studentId}}`);
  const [placementSection, setPlacementSection] = useState(null);
  const [showPopup, setShowPopup] = useState(false);

  const onDelete = () => {
    setPlacementSection(null);
    placement.sendRequest({
      url: `${api.studentPlacement}/?{filter_by="student":${studentId}}`,
    });
  };

  useEffect(() => {
    if (placement.status !== 200 || placementSection) return;
    const _placementSection = [];
    for (const job of placement.data.results) {
      _placementSection.push(
        <div
          key={job.id}
          className="mb-6"
        >
          <DetailProfileSection
            section="student/placement"
            resourceId={job.id}
            customEventHandlers={{ onDelete }}
            multiEntry
          />
        </div>,
      );
    }
    setPlacementSection(_placementSection);
  }, [placement.status, placement.data?.results, studentId, placementSection]);

  const eventHandlers = {
    onSuccess: () => {
      setShowPopup((prevPopup) => !prevPopup);
      const url = `${api.studentPlacement}/?{"filter_by=student":${studentId}}`;
      setPlacementSection(null);
      placement.sendRequest({ url });
    },
    onCancel: () => setShowPopup((prevPopup) => !prevPopup),
  };

  const buttons = [
    {
      label: 'Back',
      icon: 'angle-left',
      onClick: () => navigate(`/student/${studentId}`),
      extraClasses: 'p-button-outlined',
    }, {
      label: 'Add',
      icon: 'plus',
      onClick: () => setShowPopup((prevPopup) => !prevPopup),
    },
  ];

  return (
    <>
      <PageHeader
        title="Job Placement"
        buttons={buttons}
      />

      {placementSection?.length > 0 && (
        <Card className="surface-ground my-5">
          {placementSection}
        </Card>
      )}

      <Dialog
        header="Add Job Placement"
        visible={showPopup}
        style={{ width: '75vw' }}
        onHide={() => setShowPopup((prevPopup) => !prevPopup)}
      >
        <FormContainer
          path="student/placement"
          defaultData={{ student: studentId }}
          eventHandlers={eventHandlers}
          redirectOnError={false}
        />
      </Dialog>

      <ConfirmDialog />
    </>
  );
}

export default JobPlacementContainer;
