import React from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import PageHeader from '../PageHeader';
import FormContainer from '../crud/FormContainer';

function CreateContainer({ isPermanent }) {
  const navigate = useNavigate();
  const role = isPermanent ? 'Permanent' : 'Temporary';
  let path;
  if (isPermanent) {
    path = 'staff-permanent-designation';
  } else {
    path = 'staff-temporary-designation';
  }

  const eventHandlers = {
    onSuccess: () => navigate('/staff', { state: { isPermanent } }),
  };

  const defaultData = {
    report_date: dayjs().format('YYYY-MM-DD'),
    is_active: true,
    is_default: true,
  };

  return (
    <>
      <PageHeader title={`Grant ${role} Designation`} />
      <FormContainer
        path={path}
        eventHandlers={eventHandlers}
        defaultData={defaultData}
      />
    </>
  );
}

export default CreateContainer;
