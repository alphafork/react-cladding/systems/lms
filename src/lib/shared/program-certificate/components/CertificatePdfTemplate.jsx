import React from 'react';
import dayjs from 'dayjs';
import {
  Document,
  Image,
  Font,
  Page,
  StyleSheet,
  Text,
  View,
  Svg,
  Path,
} from '@react-pdf/renderer';
import certbg from '../assets/images/certbg.png';
import certificateFont from '../assets/fonts/CormorantInfant-Bold.ttf';
import nametextFont from '../assets/fonts/CormorantInfant-Regular.ttf';
import nameFont from '../assets/fonts/Allura-Regular.ttf';
import coursetextFont from '../assets/fonts/CormorantInfant-MediumItalic.ttf';
import courseFont from '../assets/fonts/CormorantInfant-BoldItalic.ttf';

function CertificatePdfTemplate({ certificate }) {
  Font.register({
    family: 'CormorantInfantR',
    format: 'sans-serif',
    src: nametextFont,
  });

  Font.register({
    family: 'Allura',
    format: 'sans-serif',
    src: nameFont,
  });

  Font.register({
    family: 'CormorantInfantM',
    format: 'sans-serif',
    src: coursetextFont,
  });

  Font.register({
    family: 'CormorantInfantBI',
    format: 'sans-serif',
    src: courseFont,
  });

  Font.register({
    family: 'CormorantInfantB',
    format: 'sans-serif',
    src: certificateFont,
  });

  Font.register({ family: 'Helvetica' });

  const styles = StyleSheet.create({
    body: {
      margin: 0,
      position: 'relative',
    },
    page: {
      flexDirection: 'row',
      backgroundColor: '#ffffff',
      margin: '5px',
      fontFamily: 'Helvetica',
    },
    pageBackground: {
      position: 'absolute',
      width: '100%',
      top: 0,
      zIndex: '-1',
    },
    pageView: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      display: 'block',
    },
    certificateText: {
      position: 'absolute',
      top: 155,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '24px',
      fontFamily: 'CormorantInfantB',
      color: '#000',
      textTransform: 'uppercase',
    },
    nameText: {
      position: 'absolute',
      top: 225,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '16px',
      fontFamily: 'CormorantInfantR',
      color: '#333',
      textTransform: 'uppercase',
    },
    name: {
      position: 'absolute',
      top: 250,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '44px',
      fontFamily: 'Allura',
      color: '#000',

    },
    courseText: {
      position: 'absolute',
      top: 305,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '22px',
      fontFamily: 'CormorantInfantM',
      color: '#333',
    },

    course: {
      position: 'absolute',
      top: 335,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '30px',
      fontFamily: 'CormorantInfantBI',
      color: '#333',
    },
    date: {
      position: 'absolute',
      top: 380,
      width: '100%',
      display: 'block',
      textAlign: 'center',
      fontSize: '22px',
      fontFamily: 'CormorantInfantM',
    },
    issueText: {
      position: 'absolute',
      top: 460,
      left: 10,
      width: '40%',
      display: 'block',
      textAlign: 'center',
      fontSize: '22px',
      fontFamily: 'CormorantInfantM',
    },
    directorText: {
      position: 'absolute',
      top: 460,
      right: 30,
      width: '30%',
      display: 'block',
      textAlign: 'center',
      fontSize: '20px',
      fontFamily: 'CormorantInfantM',
    },
    qrcodeFix: {
      width: 400,
      height: 400,
      position: 'absolute',
      top: 350,
      left: 120,

    },
  });

  return (
    <Document>
      <Page size="A4" orientation="landscape" dpi="72" style={styles.body}>
        <View style={styles.pageView}>
          <Text style={styles.certificateText}>Certificate of Achievement</Text>
          <Text style={styles.nameText}>This Certificate is hereby bestowed upon</Text>
          <Text style={styles.name}>{certificate?.name}</Text>
          <Text style={styles.courseText}>for successfully completing the training program in</Text>
          <Text style={styles.course}>{certificate?.program}</Text>
          <Text style={styles.date}>{`from ${dayjs(certificate?.start_date)?.format?.('DD/MM/YYYY')} to ${dayjs(certificate?.end_date)?.format?.('DD/MM/YYYY')}`}</Text>
          <Text style={styles.issueText}>{`Date of issue : ${dayjs(certificate?.date_of_issue)?.format?.('DD/MM/YYYY')}`}</Text>
          <Text style={styles.directorText}>Managing Director</Text>
          <Svg style={styles.qrcodeFix}>
            <Path
              d={certificate?.url_qr?.match?.(/d="(.+)"\/>/)[1]}
              stroke="rgb(0, 0, 0)"
              strokeWidth={1}
              transform="scale(3)"
            />
          </Svg>
          <Image src={certbg} style={styles.pageBackground} alt="Certificate image background" />
        </View>
      </Page>
    </Document>
  );
}

export default CertificatePdfTemplate;
