import React from 'react';
import { Navigate, Routes, Route } from 'react-router-dom';
import { Login, Logout, Register } from '@ims/user-auth';
import {
  Home,
  ListContainer,
  DetailContainer,
  CreateContainer,
  UpdateContainer,
  PublicRegistrationContainer,
  RegistrationContainer,
  InstitutionProfileContainer,
  ProgramApplication,
  DetailBatchContainer,
  ListMigrationContainer,
  CreateExamContainer,
  ListExamResultContainer,
  ListAttendanceContainer,
  ListAllStudentContainer,
  ListStudentContainer,
  ListTransactionContainer,
  StudentProfile,
  DetailStudentProgram,
  DetailProfileContainer,
  RegisterStaffContainer,
  ListStaffContainer,
  CreateStaffContainer,
  ListFeeContainer,
  ListOperationContainer,
  DetailOperationContainer,
  ListOperationLogContainer,
  DetailOperationLogContainer,
  DailyAccountingOperation,
  ListInvoiceContainer,
  CreateInvoiceContainer,
  ListVoucherContainer,
  CreateVoucherContainer,
  ListPermissionContainer,
  ListNotificationContainer,
  CertificatePreview,
  JobPlacementContainer,
  ReferralContainer,
  ListReferrerContainer,
  ListReferralContainer,
  ChangePasswordContainer,
} from './lib/main';
import Dashboard from './Dashboard';
import '@ims/dashboard/dist/style.css';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Dashboard />}>
        <Route path="home" element={<Home />} />
        <Route path=":page" element={<ListContainer />} />
        <Route path=":page/create" element={<CreateContainer />} />
        <Route path=":page/:id" element={<DetailContainer />} />
        <Route path=":page/:id/edit" element={<UpdateContainer />} />
        <Route path="institution-profile" element={<InstitutionProfileContainer />} />
        <Route path="student/register" element={<RegistrationContainer />} />
        <Route path="student" element={<ListStudentContainer />} />
        <Route path="student/:id" element={<StudentProfile />} />
        <Route path="student/:studentId/program/:registrationId" element={<DetailStudentProgram />} />
        <Route path="student/academic/all" element={<ListAllStudentContainer />} />
        <Route path="student/academic/:slug" element={<ListStudentContainer />} />
        <Route path="student/fee/:slug?" element={<ListFeeContainer />} />
        <Route path="batch/:id" element={<DetailBatchContainer />} />
        <Route path="batch/:id/migrate" element={<ListMigrationContainer />} />
        <Route path="batch/:batchId/attendance/:dailyScheduleId" element={<ListAttendanceContainer />} />
        <Route path="batch/:batchId/attendance/:dailyScheduleId/create" element={<ListAttendanceContainer />} />
        <Route path="batch/:batchId/exam/:examId/result" element={<ListExamResultContainer />} />
        <Route path="batch/:batchId/exam/:examId/result/create" element={<ListExamResultContainer />} />
        <Route path="batch/:batchId/exam/:examId/result/edit" element={<ListExamResultContainer />} />
        <Route path="exam/create" element={<CreateExamContainer />} />
        <Route path="course/all" element={<ProgramApplication />} />
        <Route path="package/all" element={<ProgramApplication />} />
        <Route path="profile" element={<DetailProfileContainer />} />
        <Route path="job-placement" element={<JobPlacementContainer />} />
        <Route path="staff" element={<ListStaffContainer />} />
        <Route path="staff/register" element={<RegisterStaffContainer />} />
        <Route path="staff/permanent" element={<CreateStaffContainer isPermanent />} />
        <Route path="staff/temporary" element={<CreateStaffContainer isPermanent={false} />} />
        <Route path="operation" element={<ListOperationContainer />} />
        <Route path="operation/:id" element={<DetailOperationContainer />} />
        <Route path="operation-log" element={<ListOperationLogContainer />} />
        <Route path="operation-log/:id" element={<DetailOperationLogContainer />} />
        <Route path="accounting/daily-operation" element={<DailyAccountingOperation />} />
        <Route path="accounting/invoice" element={<ListInvoiceContainer />} />
        <Route path="accounting/invoice/create" element={<CreateInvoiceContainer />} />
        <Route path="accounting/voucher" element={<ListVoucherContainer />} />
        <Route path="accounting/voucher/create" element={<CreateVoucherContainer />} />
        <Route path="accounting/transaction" element={<ListTransactionContainer />} />
        <Route path="permission/group/edit?" element={<ListPermissionContainer />} />
        <Route path="notifications" element={<ListNotificationContainer />} />
        <Route path="referral" element={<ReferralContainer />} />
        <Route path="referrer" element={<ListReferrerContainer />} />
        <Route path="referrer/:id" element={<ListReferralContainer />} />
        <Route path="change-password" element={<ChangePasswordContainer />} />
      </Route>
      <Route path="login" element={<Login redirectTo="/" />} />
      <Route path="logout" element={<Logout redirectTo="/login" />} />
      <Route path="register" element={<Register />} />
      <Route path="student-registration" element={<PublicRegistrationContainer />} />
      <Route path="certificate/public/:uuid" element={<CertificatePreview />} />
      <Route path="*" element={<Navigate to="/" replace />} />
    </Routes>
  );
}

export default App;
