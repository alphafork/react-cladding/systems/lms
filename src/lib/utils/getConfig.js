import pages from '../config/pages';
import api from '../config/api';
import routes from '../config/routes';

const getConfig = (page) => {
  // Load default config
  let config = { ...pages.default };

  // Override default config if custom config exists for the given route
  if (pages?.[page]) {
    config = {
      ...config,
      ...pages[page],
    };
  }

  // Try to determine API from given route if API is not explicitly configured
  if (!config.api && routes.includes(page)) {
    const apiName = page.replace(/-./g, (x) => x[1].toUpperCase());
    config.api = api[apiName];
  }

  return config;
};

export default getConfig;
