import { useCallback, useEffect, useState } from 'react';
import apiClient from '../utils/api/apiClient';
import handleError from '../utils/handleError';
import useAlert from './useAlert';

// Custom hook based on apiCilent for sending http requests asynchronously.
// Inspired from https://usehooks.com/useAsync.
// If url is not null, the request will be sent immediately.
// Otherwise sendRequest function will have to be invoked manually.
function useApi(url, method = 'GET', authorized = true) {
  const [status, setStatus] = useState(null);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const { getAlert: getApiAlert, setAlert } = useAlert();

  useEffect(() => {
    if (url === null) {
      setStatus(null);
      setData(null);
      setError(null);
    }
  }, [url]);

  const sendRequest = useCallback((apiConfig) => {
    const config = { ...apiConfig };
    setStatus('loading');
    setData(null);
    setError(null);

    (async () => {
      const [_data, _error, _status] = await apiClient(config);
      setStatus(_status);
      setData(_data);
      setError(_error);
      if (_error) {
        handleError(_error, setAlert);
      }
    })();
  }, []);

  useEffect(() => {
    if (url) {
      sendRequest({ url, method, authorized });
    }

    return () => {
      setStatus(null);
      setData(null);
      setError(null);
    };
  }, [url, method, sendRequest]);

  const resetResponse = () => {
    setStatus(null);
    setData(null);
    setError(null);
  };

  return {
    sendRequest,
    status,
    data,
    error,
    resetResponse,
    getApiAlert,
  };
}

export default useApi;
