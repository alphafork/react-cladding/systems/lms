import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { Chip } from 'primereact/chip';
import { ToggleButton } from 'primereact/togglebutton';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from 'primereact/inputtextarea';
import { Badge } from 'primereact/badge';
import PageHeader from '../PageHeader';
import DetailStudentPopup from '../students/DetailStudentPopup';
import ListDataTable from '../datatable/ListDataTable';
import FormContainer from '../crud/FormContainer';
import useApi from '../../hooks/useApi';
import useAlert from '../../hooks/useAlert';
import api from '../../config/api';

function ListAttendanceContainer() {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  let action = 'view';
  if (pathname.includes('create')) action = 'create';
  const { batchId, dailyScheduleId } = useParams();
  if (Number.isNaN(batchId)) navigate('/');
  const studentsUrl = `${api.batchStudent}?filter_by={"batch": ${batchId}}`;
  const students = useApi(studentsUrl);
  const studentDetail = useApi();
  const batchAttendance = useApi();
  const leaveReasons = useApi(api.studentLeaveReason);
  const leaveReasonAlert = useAlert();
  const editAttendanceAlert = useAlert();
  const [attendance, setAttendance] = useState(null);
  const [activeStudent, setActiveStudent] = useState(null);
  const [activeRemarks, setActiveRemarks] = useState(null);
  const [leaveOptions, setLeaveOptions] = useState(null);

  const [showPopup, setShowPopup] = useState({
    studentDetail: false,
    leaveForm: false,
    editForm: false,
    remarksDetail: false,
  });

  const title = 'Student Attendance';

  useEffect(() => {
    if (action === 'view') {
      const url = `${api.attendance}?filter_by={"daily_schedule": ${dailyScheduleId}}`;
      if (!batchAttendance.data?.results) {
        batchAttendance.sendRequest({ url });
      }
    }
  }, [batchAttendance.data?.results]);

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  useEffect(() => {
    if (students.data?.results) {
      students.data.results.map((student) => {
        setAttendance((prevAttendance) => ({
          ...prevAttendance,
          [student.registration.number]: {
            isPresent: true,
            leaveReason: '',
            remarks: '',
            batchStudent: Number(student.id),
            dailySchedule: Number(dailyScheduleId),
          },
        }));
      });
    }
  }, [students.data?.results]);

  useEffect(() => {
    if (!attendance || batchAttendance.status !== 200) return;
    const existingAttendance = batchAttendance.data.results;
    setAttendance((prevAttendance) => {
      const updatedAttendance = { ...prevAttendance };
      for (const student in updatedAttendance) {
        const studentAttendance = existingAttendance.find(
          (studAttendance) => (
            studAttendance.batch_student === prevAttendance[student].batchStudent
          ),
        );
        updatedAttendance[student].attendanceId = studentAttendance?.id;
        updatedAttendance[student].isPresent = studentAttendance?.is_present;
      }
      return updatedAttendance;
    });
  }, [batchAttendance.status, batchAttendance.data?.results]);

  const onClickStudent = (userId) => {
    const url = `${api.userAccount}/${userId}`;
    studentDetail.sendRequest({ url });
    togglePopup('studentDetail');
  };

  const getStudent = (rowData) => (
    <Chip
      label={rowData.student.name}
      className="cursor-pointer bg-indigo-400 text-indigo-50"
      onClick={() => onClickStudent(rowData.student.id)}
    />
  );

  const getAttendance = (rowData) => {
    const {
      id: batchStudentId,
      registration: {
        number: registration,
      },
    } = rowData;

    const handleChange = (event) => {
      if (event.value === false) {
        togglePopup('leaveForm');
      } else {
        setAttendance((prevAttendance) => ({
          ...prevAttendance,
          [registration]: {
            ...prevAttendance[registration],
            leaveReason: null,
          },
        }));
      }
      setActiveStudent(registration);
      setAttendance((prevAttendance) => ({
        ...prevAttendance,
        [registration]: {
          ...prevAttendance[registration],
          isPresent: event.value,
        },
      }));
    };

    if (action === 'view') {
      const studentAttendance = batchAttendance.data?.results?.find((student) => (
        student.batch_student === batchStudentId
      ));
      const value = studentAttendance?.is_present ? 'Present' : 'Absent';
      const severity = value === 'Present' ? 'success' : 'danger';

      const onClickAbsent = () => {
        if (!studentAttendance?.remarks) return;
        setActiveRemarks(studentAttendance.remarks);
        togglePopup('remarksDetail');
      };

      return (
        <Badge
          value={value}
          severity={severity}
          style={{ cursor: studentAttendance?.remarks ? 'pointer' : 'auto' }}
          onClick={onClickAbsent}
        />
      );
    }

    return (
      <ToggleButton
        onIcon="pi pi-check"
        onLabel="Present"
        offIcon="pi pi-times"
        offLabel="Absent"
        checked={attendance?.[registration]?.isPresent}
        onChange={handleChange}
      />
    );
  };

  const getLeaveReason = (rowData) => {
    const { id: batchStudentId } = rowData;
    const studentAttendance = batchAttendance.data?.results?.find((student) => (
      student.batch_student === batchStudentId
    ));
    const leaveReasonId = studentAttendance?.leave_reason;
    let leaveReason;

    if (leaveReasons.data?.results) {
      leaveReason = leaveReasons.data.results?.find?.((reason) => (
        reason.id === leaveReasonId
      ));
      return leaveReason?.name ?? 'N/A';
    }
  };

  const editAttendance = (rowData) => {
    togglePopup('editForm');
    setActiveStudent(rowData.registration.number);
  };

  const getEditButton = (rowData) => (
    <Button
      label="Edit"
      icon="pi pi-pencil"
      size="small"
      onClick={() => editAttendance(rowData)}
    />
  );

  const columns = [
    { header: 'Registration No', field: 'registration.number' },
    { header: 'Student', body: getStudent },
    { header: 'Email', field: 'student.email' },
    { header: 'Attendance', body: getAttendance },
  ];

  if (action === 'view') {
    columns.push({
      header: 'Leave Reason',
      body: getLeaveReason,
    }, {
      header: 'Action',
      body: getEditButton,
    });
  }

  const handleAttendance = () => {
    const url = api.batchAttendance;
    const method = 'POST';
    const data = {
      daily_schedule: Number(dailyScheduleId),
      attendance: Object.values(attendance).map((value) => ({
        daily_schedule: value.dailySchedule,
        is_present: value.isPresent,
        leave_reason: value.leaveReason,
        batch_student: value.batchStudent,
        remarks: value.remarks,
      })),
    };
    batchAttendance.sendRequest({
      url,
      method,
      data,
    });
  };

  const handleCancel = () => {
    navigate(`/batch/${batchId}`);
  };

  useEffect(() => {
    if (leaveReasons.data?.results) {
      const options = leaveReasons.data.results?.map?.((reason) => ({
        label: reason.name,
        value: reason.id,
      }));
      setLeaveOptions(options);
    }
  }, [leaveReasons.data?.results]);

  const handleLeaveReason = (event) => {
    setAttendance((prevAttendance) => ({
      ...prevAttendance,
      [activeStudent]: {
        ...prevAttendance[activeStudent],
        leaveReason: event.value,
      },
    }));
  };

  const handleRemarksChange = (event) => {
    setAttendance((prevAttendance) => ({
      ...prevAttendance,
      [activeStudent]: {
        ...prevAttendance[activeStudent],
        remarks: event.target.value,
      },
    }));
  };

  const handleRemarksSubmit = (event = null) => {
    event?.preventDefault();
    togglePopup('leaveForm');
    const {
      isPresent,
      leaveReason,
    } = attendance?.[activeStudent] ?? {};
    if (!isPresent && !leaveReason) {
      setAttendance((prevAttendance) => ({
        ...prevAttendance,
        [activeStudent]: {
          ...prevAttendance[activeStudent],
          isPresent: true,
        },
      }));
      leaveReasonAlert.setAlert(
        'error',
        'Leave reason cannot be empty',
      );
    }
  };

  useEffect(() => {
    if (batchAttendance.status === 201) {
      navigate(`/batch/${batchId}`);
    }
  }, [batchAttendance.status]);

  const buttons = () => {
    if (action === 'view') {
      return [
        {
          label: 'Back',
          icon: 'angle-left',
          onClick: () => navigate(`/batch/${batchId}`),
          extraClasses: 'p-button-outlined',
        },
      ];
    }
    return false;
  };

  const eventHandlers = {
    onSuccess: () => {
      togglePopup('editForm');
      const url = `${api.attendance}?filter_by={"daily_schedule": ${dailyScheduleId}}`;
      batchAttendance.sendRequest({ url });
      editAttendanceAlert.setAlert(
        'success',
        'Attendance updated succesfully',
      );
    },
    onCancel: () => togglePopup('editForm'),
  };

  const isSelectable = (data) => (data.status === 'ac');
  const rowClassName = (data) => (isSelectable(data) ? '' : 'p-disabled');
  const isStudentSelectable = (event) => (event.data ? isSelectable(event.data) : true);

  return (
    <>
      <PageHeader title={title} buttons={buttons()} />
      <DetailStudentPopup
        visible={showPopup.studentDetail}
        onHide={() => togglePopup('studentDetail')}
        student={studentDetail.data}
      />

      <ListDataTable
        data={students.data}
        columns={columns}
        isDataSelectable={isStudentSelectable}
        rowClassName={rowClassName}
      />

      <Dialog
        header="Leave details"
        visible={showPopup.leaveForm}
        style={{ width: '50vw' }}
        onHide={handleRemarksSubmit}
      >
        <form>
          <div className="flex flex-column gap-2">
            <label
              htmlFor="leave_reason"
              className="block text-900 font-medium mb-2"
            >
              Leave Reason
            </label>
            <Dropdown
              type="choices"
              value={attendance?.[activeStudent]?.leaveReason}
              onChange={handleLeaveReason}
              options={leaveOptions}
              placeholder="Select"
              className="w-5"
            />
            <label
              htmlFor="remarks"
              className="block text-900 font-medium mt-4 mb-2"
            >
              Remarks
            </label>
            <InputTextarea
              autoResize
              value={attendance?.[activeStudent]?.remarks}
              onChange={handleRemarksChange}
              rows={5}
              cols={30}
            />
            <div className="mt-2">
              <Button
                type="submit"
                label="Submit"
                className="mr-2 mb-2"
                onClick={handleRemarksSubmit}
              />
            </div>
          </div>
        </form>
      </Dialog>

      <Dialog
        header="Edit attendance"
        visible={showPopup.editForm}
        style={{ width: '50vw' }}
        onHide={() => togglePopup('editForm')}
      >
        <FormContainer
          path="attendance"
          resourceId={attendance?.[activeStudent]?.attendanceId}
          defaultData={{
            daily_schedule: attendance?.[activeStudent]?.dailySchedule,
            batch_student: attendance?.[activeStudent]?.batchStudent,
            leave_reason: attendance?.[activeStudent]?.leaveReason ?? null,
          }}
          eventHandlers={eventHandlers}
        />
      </Dialog>

      {(action === 'view') && (
        <Dialog
          header="Leave remarks"
          visible={showPopup.remarksDetail}
          style={{ width: '50vw' }}
          onHide={() => togglePopup('remarksDetail')}
        >
          {activeRemarks}
        </Dialog>
      )}

      {(action === 'create') && (
        <div className="mt-4">
          <Button
            className="mr-2 mb-2"
            onClick={handleAttendance}
          >
            Submit Attendance
          </Button>
          <Button
            label="Cancel"
            className="mr-2 mb-2 p-button-outlined"
            onClick={handleCancel}
          />
        </div>
      )}

      {batchAttendance.getApiAlert}
      {leaveReasonAlert.getAlert}
      {editAttendanceAlert.getAlert}
    </>
  );
}

export default ListAttendanceContainer;
