import React from 'react';
import { Message } from 'primereact/message';
import PageHeader from '../PageHeader';
import FormContainer from '../crud/FormContainer';
import getConfig from '../../utils/getConfig';

function UpdateProfile({
  section,
  resourceId,
  eventHandlers,
}) {
  const config = getConfig(section);

  return (
    <>
      <PageHeader title={config.title} />
      {window.localStorage.getItem('PROFILE_COMPLETED') === 'false' && (
        <Message
          className="p-4 mb-4"
          style={{ borer: 'solid', borderWidth: '0 0 0 5px' }}
          severity="warn"
          text="Welcome. Your profile is incomplete, you need to complete your profile before applying for any courses!"
        />
      )}
      <FormContainer
        path={section}
        resourceId={resourceId}
        eventHandlers={eventHandlers}
        redirectOnError={false}
      />
    </>
  );
}

export default UpdateProfile;
