import React from 'react';
import Detail from '../crud/Detail';
import useFieldParser from '../../hooks/useFieldParser';
import getConfig from '../../utils/getConfig';

function DetailContainer({ id }) {
  const config = getConfig('referrer');
  const {
    displayData,
    getSchemaAlert,
    getApiAlert,
  } = useFieldParser(config, id);

  return (
    <>
      <Detail
        headerFields={displayData?.headerFields}
        fields={displayData?.fields}
        showBlankFields
      />
      { getSchemaAlert }
      { getApiAlert }
    </>
  );
}

export default DetailContainer;
