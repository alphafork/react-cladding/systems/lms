import React from 'react';
import { Link } from 'react-router-dom';

function StudentSideBar() {
  return (
    <div className="layout-sidebar">
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Dashboard</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/home">
                <i className="layout-menuitem-icon pi pi-fw pi-home" />
                <span className="layout-menuitem-text">Home</span>
              </Link>
            </li>
          </ul>
          <div className="layout-menuitem-root-text">ACADEMIC</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/course/all">
                <i className="layout-menuitem-icon pi pi-fw pi-briefcase" />
                <span className="layout-menuitem-text">Courses</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/package/all">
                <i className="layout-menuitem-icon pi pi-fw pi-briefcase" />
                <span className="layout-menuitem-text">Packages</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">PROFILE</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/profile">
                <i className="layout-menuitem-icon pi pi-fw pi-id-card" />
                <span className="layout-menuitem-text">Profile</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  );
}

export default StudentSideBar;
