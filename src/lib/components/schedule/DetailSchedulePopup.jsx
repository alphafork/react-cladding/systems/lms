import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function DetailSchedulePopup({
  visible,
  onHide,
  onClickEditSchedule,
  onClickDeleteSchedule,
  batch,
  event,
}) {
  const navigate = useNavigate();
  const checkAttendance = useApi();
  const [hasAttendance, setHasAttendance] = useState(false);

  useEffect(() => {
    if (event?.schedule) {
      const url = `${api.attendance}?filter_by={"daily_schedule": ${event.id}}`;
      checkAttendance.sendRequest({ url });
    }
  }, [event]);

  useEffect(() => {
    let scheduleAttendance;
    if (checkAttendance.data?.results) {
      scheduleAttendance = checkAttendance.data.results?.find((attendance) => (
        attendance.daily_schedule === event.id
      ));
    }
    if (scheduleAttendance) {
      setHasAttendance({
        [event.id]: true,
      });
    }
  }, [checkAttendance.data?.results]);

  const buttons = () => {
    const scheduleButtons = [
      {
        label: 'Edit',
        icon: 'pencil',
        onClick: onClickEditSchedule,
      },
      {
        label: 'Delete',
        icon: 'trash',
        severity: 'danger',
        onClick: onClickDeleteSchedule,
      },
    ];

    if (!event?.id) {
      return scheduleButtons;
    }

    if (hasAttendance?.[event?.id]) {
      const route = `/batch/${batch}/attendance/${event.id}`;
      scheduleButtons.push({
        label: 'View Attendance',
        icon: 'user-plus',
        severity: 'secondary',
        onClick: () => navigate(route),
      });
      return scheduleButtons;
    }

    const scheduleDate = new Date(event?.headerFields?.title).setHours(0, 0, 0, 0);
    const today = new Date().setHours(0, 0, 0, 0);
    if (scheduleDate === today) {
      const route = `/batch/${batch}/attendance/${event.id}/create`;
      scheduleButtons.push({
        label: 'Take Attendance',
        icon: 'user-plus',
        onClick: () => navigate(route),
      });
    }

    return scheduleButtons;
  };

  return (
    <Dialog
      header="Schedule details"
      visible={visible}
      style={{ width: '50vw' }}
      onHide={onHide}
    >
      <PageHeader title="Schedule" buttons={buttons()} />
      <Detail
        headerFields={event?.headerFields}
        fields={event?.fields}
      />
    </Dialog>
  );
}

export default DetailSchedulePopup;
