import React from 'react';
import { Link } from 'react-router-dom';

function AccountantSideBar() {
  return (
    <div className="layout-sidebar">
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Dashboard</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/home">
                <i className="layout-menuitem-icon pi pi-fw pi-home" />
                <span className="layout-menuitem-text">Home</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">STUDENTS</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/all">
                <i className="layout-menuitem-icon pi pi-fw pi-users" />
                <span className="layout-menuitem-text">All</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/academic/applied">
                <i className="layout-menuitem-icon pi pi-fw pi-hourglass" />
                <span className="layout-menuitem-text">Applied</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/register">
                <i className="layout-menuitem-icon pi pi-fw pi-user-plus" />
                <span className="layout-menuitem-text">New Student Registration</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">ACCOUNTING</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/daily-operation">
                <i className="layout-menuitem-icon pi pi-fw pi-replay" />
                <span className="layout-menuitem-text">Daily Accounting Operation</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/student/fee">
                <i className="layout-menuitem-icon pi pi-fw pi-money-bill" />
                <span className="layout-menuitem-text">Student Fees</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/transaction">
                <i className="layout-menuitem-icon pi pi-fw pi-arrow-right-arrow-left" />
                <span className="layout-menuitem-text">Transactions</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/invoice">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Invoices</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/accounting/voucher">
                <i className="layout-menuitem-icon pi pi-fw pi-copy" />
                <span className="layout-menuitem-text">Vouchers</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/installment-scheme">
                <i className="layout-menuitem-icon pi pi-fw pi-table" />
                <span className="layout-menuitem-text">Installment Schemes</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/transaction-account">
                <i className="layout-menuitem-icon pi pi-fw pi-wallet" />
                <span className="layout-menuitem-text">Transaction Accounts</span>
              </Link>
            </li>
            <li className="menuitem">
              <Link className="p-ripple" to="/finance-account">
                <i className="layout-menuitem-icon pi pi-fw pi-book" />
                <span className="layout-menuitem-text">All Accounts</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
      <ul className="layout-menu">
        <li className="layout-root-menuitem">
          <div className="layout-menuitem-root-text">Referrals</div>
          <ul>
            <li className="menuitem">
              <Link className="p-ripple" to="/referrer">
                <i className="layout-menuitem-icon pi pi-directions" />
                <span className="layout-menuitem-text">Referrers</span>
              </Link>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  );
}

export default AccountantSideBar;
