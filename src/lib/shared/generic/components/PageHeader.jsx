import React from 'react';
import { Button } from 'primereact/button';

function PageHeader({
  children,
  title,
  buttons,
  selectButton,
}) {
  return (
    <div className="flex align-items-start flex-column lg:justify-content-between lg:flex-row">
      <div className="mt-2">
        <h3>{title}</h3>
      </div>
      <div>
        {children ?? (
          buttons && buttons.map((btn) => (
            <Button
              key={btn.label.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')}
              label={btn.label}
              className={`px-3 py-2 ml-2 mb-3 ${btn.extraClasses}`}
              icon={`pi pi-${btn.icon}`}
              severity={btn.severity ?? false}
              onClick={btn.onClick}
            />
          ))
        )}
        {selectButton}
      </div>
    </div>
  );
}

export default PageHeader;
