import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { pdf } from '@react-pdf/renderer';
import { saveAs } from 'file-saver';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { Card } from 'primereact/card';
import PageHeader from '../PageHeader';
import DueFee from '../fee/DueFee';
import FeeManagementActions from '../fee/FeeManagementActions';
import useStore from '../../hooks/useStore';
import CertificatePdfTemplate from './CertificatePdfTemplate';
import useApi from '../../hooks/useApi';
import useAlert from '../../hooks/useAlert';
import api from '../../config/api';

function DetailStudentProgram() {
  dayjs.extend(utc);
  const { role: userRole } = useStore((state) => state.user);
  const { studentId, registrationId } = useParams();
  const studentDetailsUrl = `${api.studentDetails}/${registrationId}`;
  const studentDetails = useApi(studentDetailsUrl);
  const studentFeeUrl = `${api.feeSplitup}/${registrationId}`;
  const studentFee = useApi(studentFeeUrl);
  const courseStatus = useApi();
  const courseStatusAlert = useAlert();
  const certificateUrl = `${api.certificate}/${registrationId}`;
  const certificate = useApi(certificateUrl);
  const fileName = `certificate${registrationId}`;
  const [student, setStudent] = useState(null);
  const [programType, setProgramType] = useState(null);
  const [courses, setCourses] = useState(null);

  useEffect(() => {
    if (student || programType || !studentDetails.data) return;
    if (studentDetails.data?.package) setProgramType('Package');
    if (studentDetails.data?.course) setProgramType('Course');
    setStudent({
      ...studentDetails.data.student,
      registration: studentDetails.data.registration.id,
      registration_no: studentDetails.data.registration.number,
      subscriptionStatus: studentFee.data?.subscription_status,
    });
  }, [
    programType,
    student,
    studentDetails.data,
    studentFee.data?.subscription_status,
  ]);

  useEffect(() => {
    if (courses || !programType || !studentDetails.data) return;

    if (programType === 'Package') {
      setCourses(studentDetails.data.package.courses);
    }
    if (programType === 'Course') {
      setCourses(studentDetails.data.course);
    }
  }, [courses, programType, studentDetails.data]);

  const downloadCertificate = async () => {
    if (certificate?.data) {
      const blob = await pdf((
        <CertificatePdfTemplate
          certificate={certificate.data}
        />
      )).toBlob();
      saveAs(blob, fileName);
    }
  };

  const feeHeader = (
    <div className="flex">
      <div className="flex-auto flex justify-content-between font-bold py-3">Fee Details</div>
      <div className="flex-auto flex align-items-center justify-content-end font-bold  px-5 py-3 gap-3">
        <FeeManagementActions student={student} />
      </div>
    </div>
  );

  const markCourseAsCompleted = () => {
    const url = `${api.studentRegistration}/${registrationId}`;
    const method = 'PUT';
    const data = {
      registration_no: studentDetails.data?.registration_no,
      applied_date: studentDetails.data?.applied_date,
      student: studentId,
      status: 'co',
    };
    courseStatus.sendRequest({
      url,
      method,
      data,
    });
  };

  useEffect(() => {
    if (courseStatus.status === 200) {
      courseStatusAlert.setAlert('success', 'Successfully marked course as completed.');
      studentDetails.sendRequest({ url: studentDetailsUrl });
    }
  }, [courseStatus.status]);

  const getButtons = () => {
    const buttons = [];
    if ((userRole === 'Admin' || userRole === 'Manager')
      && (studentDetails.data?.registration.status === 'enrolled')) {
      buttons.push({
        label: 'Mark as completed',
        icon: 'pi pi-check',
        size: 'small',
        onClick: markCourseAsCompleted,
      });
    }
    if (studentDetails.data?.registration.status === 'completed') {
      buttons.push({
        label: 'Download Certificate',
        icon: 'pi pi-star',
        size: 'small',
        severity: 'success',
        onClick: downloadCertificate,
      });
    }
    return buttons;
  };

  return (
    <>
      <PageHeader title={`${programType} Details`} buttons={getButtons()} />
      <Card title={feeHeader} className="mt-3">
        <div className="surface-0">
          <DueFee registrationId={registrationId} />
        </div>
      </Card>

      {courses?.map?.((course) => (
        course?.batches?.length > 0 && (
          <Card title={course?.course_name} className="surface-ground mt-5 pt-2">
            <p className="m-0 pb-2">
              Batch Name:
              {' '}
              <span className="text-900 font-medium">
                {course?.batches?.[0]?.batch_name ?? '-'}
              </span>
            </p>
            <p className="m-0">
              Enrolled Date:
              {' '}
              <span className="text-900 font-medium">
                {course?.batches?.[0]?.enrolled_date ? dayjs(course.batches[0].enrolled_date).format('MMM D, YYYY') : '-'}
              </span>
            </p>
          </Card>
        )
      ))}

      { courseStatus.getApiAlert }
      { courseStatusAlert.getAlert }
    </>
  );
}

export default DetailStudentProgram;
