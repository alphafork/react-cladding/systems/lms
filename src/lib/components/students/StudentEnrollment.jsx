import React from 'react';
import { Dialog } from 'primereact/dialog';
import FormContainer from './FormContainer';
import api from '../../config/api';

function StudentEnrollment({
  visible,
  onHide,
  student,
  program,
  togglePopup,
  excludedFields,
  onSuccess,
}) {
  let enrollmentApi;
  let filterString;
  const hiddenFields = {
    registration: student?.registration,
  };

  if (program === 'Packages') {
    enrollmentApi = `${api.packageEnrollment}?package_id=${student?.package}`;
  } else {
    enrollmentApi = api.courseEnrollment;
    filterString = `filter_by={"course":"${student?.course}"}`;
  }

  return (
    <Dialog
      header="Enroll student"
      visible={visible}
      style={{ width: '75vw' }}
      onHide={onHide}
    >
      <FormContainer
        path={enrollmentApi}
        filterString={filterString}
        hiddenFields={hiddenFields}
        togglePopup={togglePopup}
        action="create"
        excludedFields={excludedFields}
        onSuccess={onSuccess}
      />
    </Dialog>
  );
}

export default StudentEnrollment;
