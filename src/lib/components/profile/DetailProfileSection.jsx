import React, { useState } from 'react';
import { Dialog } from 'primereact/dialog';
import { confirmDialog } from 'primereact/confirmdialog';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import UpdateProfile from './UpdateProfile';
import useStore from '../../hooks/useStore';
import useApi from '../../hooks/useApi';
import useFieldParser from '../../hooks/useFieldParser';
import getConfig from '../../utils/getConfig';

function DetailProfileSection({
  section,
  resourceId,
  customEventHandlers,
  multiEntry = false,
}) {
  const user = useStore((state) => state.user);
  const setUser = useStore((state) => state.setUser);
  const remove = useApi();
  const [showPopup, setShowPopup] = useState(false);
  const config = getConfig(section);
  const {
    displayData,
    getSchemaAlert,
    getApiAlert,
    sendRequest,
  } = useFieldParser(config, resourceId);

  const deleteItem = () => {
    const url = `${config.api}/${resourceId}`;
    remove.sendRequest({ url, method: 'DELETE' });
    customEventHandlers?.onDelete?.();
  };

  const confirmDelete = () => {
    confirmDialog({
      message: 'Are you sure you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      acceptClassName: 'p-button-danger',
      accept: deleteItem,
    });
  };

  const buttons = [
    {
      label: 'Edit',
      icon: 'pencil',
      onClick: () => setShowPopup(true),
    },
  ];

  if (multiEntry) {
    buttons.push({
      label: 'Delete',
      icon: 'trash',
      onClick: confirmDelete,
      severity: 'danger',
    });
  }

  const eventHandlers = {
    onSuccess: (status, data) => {
      setShowPopup(false);
      const url = `${config.api}/${resourceId}`;
      sendRequest({ url });
      if (section === 'profile/bio' && status === 200 && data?.photo) {
        setUser({
          ...user,
          photo: data.photo,
        });
      }
    },
    onCancel: () => {
      setShowPopup(false);
    },
    ...customEventHandlers,
  };

  return (
    <>
      <PageHeader title={config.title} buttons={buttons} />
      <Detail
        fields={displayData?.fields}
        showBlankFields
      />
      <Dialog
        header="Edit Profile"
        visible={showPopup}
        style={{ width: '75vw' }}
        onHide={() => setShowPopup(false)}
      >
        <UpdateProfile
          section={section}
          resourceId={resourceId}
          eventHandlers={eventHandlers}
        />
      </Dialog>
      { getSchemaAlert }
      { getApiAlert }
    </>
  );
}

export default DetailProfileSection;
