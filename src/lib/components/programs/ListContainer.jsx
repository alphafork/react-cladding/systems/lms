import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Dialog } from 'primereact/dialog';
import { Message } from 'primereact/message';
import { SelectButton } from 'primereact/selectbutton';
import PageHeader from '../PageHeader';
import List from '../crud/List';
import DetailProgram from './DetailProgram';
import useApi from '../../hooks/useApi';
import useAlert from '../../hooks/useAlert';
import api from '../../config/api';
import getConfig from '../../utils/getConfig';

function ListContainer({ studentId, programType }) {
  const profile = useApi(api.profileStatus);
  const { pathname } = useLocation();
  const userId = studentId ?? window.localStorage.getItem('USER_ID');
  const page = programType ?? (
    pathname.includes('package') ? 'package' : 'course'
  );
  let url = page === 'package' ? api.studentPackage : api.studentCourse;
  url = `${url}/${userId}`;
  const { headerFields } = getConfig(page);
  const programs = useApi(url);
  const apply = useApi();
  const applyAlert = useAlert();
  const [programOption, setProgramOption] = useState('Not Applied');
  const [filteredPrograms, setFilteredPrograms] = useState(null);
  const [buttons, setButtons] = useState(null);
  const [activeProgram, setActiveProgram] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const title = `${page.charAt(0).toUpperCase()}${page.replace(/-/g, '').slice(1)} List`;

  useEffect(() => {
    if (profile.data === true) {
      window.localStorage.setItem('PROFILE_COMPLETED', true);
    }
    if (profile.data === false) {
      window.localStorage.setItem('PROFILE_COMPLETED', false);
    }
  }, [profile.data]);

  useEffect(() => {
    if (!programs.data?.results) {
      return;
    }

    setFilteredPrograms(programs.data);

    const onClickView = (programId) => {
      const program = programs.data?.results?.find?.((item) => (
        item.id === programId
      ));
      setActiveProgram(program);
      setShowPopup(true);

      return () => {
        setActiveProgram(null);
      };
    };

    const onClickApply = (programId) => {
      apply.sendRequest({
        url: page === 'package' ? api.packageApplication : api.courseApplication,
        method: 'POST',
        data: {
          [page]: programId,
          student: userId,
        },
      });
      setButtons((prevButtons) => ({
        ...prevButtons,
        apply: {
          ...prevButtons?.apply,
          [programId]: {
            ...prevButtons?.apply?.[programId],
            icon: 'star-fill',
            disabled: true,
          },
        },
      }));
    };

    for (const item of programs.data.results) {
      setButtons((prevButtons) => ({
        ...prevButtons,
        view: {
          ...prevButtons?.view,
          [item.id]: {
            label: 'View',
            icon: 'eye',
            severity: 'secondary',
            onClick: onClickView,
          },
        },
        apply: {
          ...prevButtons?.apply,
          [item.id]: {
            label: 'Apply',
            icon: 'star',
            onClick: onClickApply,
          },
        },
      }));
    }
  }, [programs.data?.results]);

  const onHide = () => {
    setShowPopup(false);
    setActiveProgram(null);
  };

  useEffect(() => {
    if (apply.status === 201) {
      applyAlert.setAlert('success', `Applied for ${page} successfully, please wait for approval.`);
    }
  }, [apply.status]);

  const selectButton = (
    <SelectButton
      className="mb-4"
      value={programOption}
      onChange={(e) => setProgramOption(e.value)}
      options={['Not Applied', 'All']}
    />
  );

  useEffect(() => {
    if (programOption === 'Not Applied') {
      setFilteredPrograms((prevFilteredPrograms) => {
        const _filteredPrograms = prevFilteredPrograms?.results?.filter?.((program) => (
          program.status === 'unregistered'
        ));
        return { results: _filteredPrograms };
      });
    } else {
      setFilteredPrograms(programs.data);
    }
  }, [programOption, programs.data]);

  return (
    <>
      <PageHeader title={title} selectButton={selectButton} />
      { window.localStorage.getItem('PROFILE_COMPLETED') === 'false'
        ? (
          <Message
            className="p-4 mb-4"
            style={{ borer: 'solid', borderWidth: '0 0 0 5px' }}
            severity="warn"
            text="Your profile is incomplete, you need to complete your profile before applying for any course or packages!"
          />
        ) : (
          <>
            <List
              headerFields={headerFields}
              data={filteredPrograms}
              buttons={buttons}
            />
            <Dialog
              header={`${title} Details`}
              visible={showPopup}
              style={{ width: '75vw' }}
              onHide={onHide}
            >
              <DetailProgram program={activeProgram} />
            </Dialog>
            { programs.getApiAlert }
            { apply.getApiAlert }
            { applyAlert.getAlert }
          </>
        )}
    </>
  );
}

export default ListContainer;
