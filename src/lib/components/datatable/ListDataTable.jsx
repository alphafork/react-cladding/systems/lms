import React, { useState } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { ProgressSpinner } from 'primereact/progressspinner';
import { Paginator } from 'primereact/paginator';
import { Ripple } from 'primereact/ripple';
import { classNames } from 'primereact/utils';

function ListDataTable({
  api,
  apiUrl,
  data,
  header,
  globalFilterFields,
  filters,
  columns,
  customPagination,
}) {
  const [first, setFirst] = useState(0);
  const defaultRows = Number(process.env.DEFAULT_NUMBER_OF_ITEMS_PER_PAGE);
  const [rows, setRows] = useState(customPagination?.rows ?? defaultRows);

  const pagination = customPagination ?? {
    onClickPrev: (e, callback) => {
      if (api.data?.previous) {
        api.sendRequest({ url: api.data.previous });
      }
      callback(e);
    },
    onClickNext: (e, callback) => {
      if (api.data?.next) {
        api.sendRequest({ url: api.data.next });
      }
      callback(e);
    },
    onClickPage: (e, options) => {
      let separator = '?';
      if (apiUrl && apiUrl.includes('?')) separator = '&';
      api.sendRequest({ url: `${apiUrl}${separator}limit=${options.props.rows}&offset=${options.props.rows * (options.page)}` });
      options.onClick(e);
    },
  };

  const onPageChange = (e) => {
    setFirst(e.first);
    setRows(e.rows);
  };

  const template = {
    layout: 'PrevPageLink PageLinks NextPageLink CurrentPageReport',
    PrevPageLink: (options) => {
      return (
        <button
          type="button"
          className={classNames(options.className, 'border-round')}
          onClick={(e) => { pagination.onClickPrev(e, options.onClick) }}
          disabled={options.disabled}
        >
          <span className="pi pi-angle-left"></span>
          <Ripple />
        </button>
      );
    },
    NextPageLink: (options) => {
      return (
        <button
          type="button"
          className={classNames(options.className, 'border-round')}
          onClick={(e) => { pagination.onClickNext(e, options.onClick) }}
          disabled={options.disabled}
        >
          <span className="pi pi-angle-right"></span>
          <Ripple />
        </button>
      );
    },
    PageLinks: (options) => {
      if (
        (
          options.view.startPage === options.page
            && options.view.startPage !== 0
        ) || (
          options.view.endPage === options.page
            && options.page + 1 !== options.totalPages
        )
      ) {
        const className = classNames(options.className, { 'p-disabled': true });

        return (
          <span className={className} style={{ userSelect: 'none' }}>
            ...
          </span>
        );
      }

      return (
        <button
          type="button"
          className={options.className}
          onClick={(e) => pagination.onClickPage(e, options)}
        >
          {options.page + 1}
          <Ripple />
        </button>
      );
    },
  };

  return (
    <div>
      {!data
        ? (
          <div className="grid">
            <ProgressSpinner />
          </div>
        ) : (
          <>
            <DataTable
              header={header}
              filters={filters}
              filterDisplay="row"
              globalFilterFields={globalFilterFields}
              dataKey="id"
              value={data?.results}
            >
              {columns.map((col, index) => (
                <Column
                  key={col.field ?? index}
                  field={col.field}
                  header={col.header}
                  body={col.body}
                  filter={col.filter}
                  filterElement={col.filterElement}
                  showFilterMenu={false}
                  filterMenuStyle={{ width: '14rem' }}
                  style={{ minWidth: '12rem' }}
                />
              ))}
            </DataTable>
            { pagination && (
              <Paginator
                template={template}
                first={first}
                rows={rows}
                totalRecords={data.count}
                onPageChange={onPageChange}
              />
            )}
          </>
        )}
    </div>
  );
}

export default ListDataTable;
