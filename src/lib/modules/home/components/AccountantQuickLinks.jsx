const AccountantQuickLinks = [
  {
    label: 'Student Fees',
    link: '/student/fee',
  },
  {
    label: 'Transactions',
    link: '/accounting/transaction',
  },
];

export default AccountantQuickLinks;
