const ManagerQuickLinks = [
  {
    label: 'Courses',
    link: '/course',
  },
  {
    label: 'Packages',
    link: '/package',
  },
  {
    label: 'Batches',
    link: '/batch',
  },
];

export default ManagerQuickLinks;
