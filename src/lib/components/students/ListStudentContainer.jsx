import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Tag } from 'primereact/tag';
import { Chip } from 'primereact/chip';
import { Button } from 'primereact/button';
import { SelectButton } from 'primereact/selectbutton';
import { Dialog } from 'primereact/dialog';
import { Panel } from 'primereact/panel';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import DetailStudentPopup from './DetailStudentPopup';
import FeeSubscriptionPopup from './FeeSubscriptionPopup';
import FeePayment from '../fee/FeePayment';
import StudentEnrollment from './StudentEnrollment';
import useApi from '../../hooks/useApi';
import useAlert from '../../hooks/useAlert';
import students from '../../config/students';
import api from '../../config/api';

function ListStudentContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const enrollmentAlert = useAlert();
  const applicants = useApi();
  const student = useApi();
  const programFee = useApi();
  const subscription = useApi();
  const { slug: path } = useParams();
  const { excludedFields } = students[path] ?? [];
  const programOptions = ['Courses', 'Packages'];
  const [activeProgramType, setActiveProgramType] = useState(programOptions[0]);
  const [activeStudent, setActiveStudent] = useState(null);
  const [programs, setPrograms] = useState(null);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    'program.enrollment_status': { value: null, matchMode: FilterMatchMode.EQUALS },
  });

  const [showPopup, setShowPopup] = useState({
    studentDetail: false,
    subscriptionForm: false,
    paymentForm: false,
    enrollmentForm: false,
    enrollmentDetail: false,
  });

  const title = `${path.charAt(0).toUpperCase()}${path.slice(1)} Students`;

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const getEnrollmentDetail = (rowData) => {
    togglePopup('enrollmentDetail');
    setActiveStudent({ ...rowData });
  };

  const getEnrollmentStatusSeverity = (status) => {
    switch (status) {
      case 'none':
        return 'danger';

      case 'all':
        return 'success';

      default:
        return null;
    }
  };

  const getEnrollmentStatus = (rowData) => {
    const {
      enrollment_count: enrollmentCount,
      enrollment_status: enrollmentStatus,
    } = rowData?.program ?? {};
    return (
      <Tag
        value={enrollmentCount}
        severity={getEnrollmentStatusSeverity(enrollmentStatus)}
        className="px-2 py-1"
        style={{ cursor: 'pointer' }}
        onClick={() => getEnrollmentDetail(rowData)}
      />
    );
  };

  const getEnrollmentStatusFilterTemplate = (enrollmentStatus) => (
    <Tag
      value={enrollmentStatus}
      severity={getEnrollmentStatusSeverity(enrollmentStatus)}
      className="px-2 py-1"
    />
  );

  const getEnrollmentStatusFilter = (options) => {
    const statusOptions = ['none', 'partial', 'all'];
    return (
      <Dropdown
        value={options.value}
        options={statusOptions}
        onChange={(e) => options.filterApplyCallback(e.value)}
        itemTemplate={
          (status) => getEnrollmentStatusFilterTemplate(status)
        }
        placeholder="Select One"
        className="p-column-filter"
        showClear
        style={{ minWidth: '12rem' }}
      />
    );
  };

  const getApplicants = (status) => {
    const statusCode = {
      applied: 'ap',
      approved: 'pr',
      alumni: 'co',
    };

    let url;
    if (Object.keys(statusCode).includes(status)) {
      url = `${api.studentDetails}?status=${statusCode[status]}`;
    } else {
      navigate('/');
    }
    applicants.sendRequest({ url });
  };

  useEffect(() => {
    getApplicants(path);
  }, [path]);

  useEffect(() => {
    const data = { ...applicants.data };
    if (!data?.results) return;
    let programType = 'course';
    if (activeProgramType === 'Packages') {
      programType = 'package';
    }
    const results = data.results.filter((result) => (
      result?.program?.type === programType
    ));
    data.results = results;
    setPrograms(data);
  }, [applicants.data, activeProgramType]);

  const onClickStudent = (userId) => {
    const url = `${api.userAccount}/${userId}`;
    student.sendRequest({ url });
    togglePopup('studentDetail');
  };

  const getStudent = (rowData) => (
    <Chip
      label={rowData.student.name}
      className="cursor-pointer bg-indigo-400 text-indigo-50"
      onClick={() => onClickStudent(rowData.student.id)}
    />
  );

  const onClickApprove = (rowData) => {
    togglePopup('subscriptionForm');
    setActiveStudent({
      ...rowData.student,
      registration: rowData.registration.id,
      registration_no: rowData.registration.number,
      program_name: rowData.program.name,
      program_type: rowData.program.type,
    });
    let url = `${api.course}/${rowData.program.id}`;
    if (activeProgramType === 'Packages') {
      url = `${api.package}/${rowData.program.id}`;
    }
    programFee.sendRequest({ url });
  };

  useEffect(() => {
    if (!programFee.data) return;
    setActiveStudent((prevActiveStudent) => ({
      ...prevActiveStudent,
      program_fee: programFee.data.fee,
    }));
  }, [programFee.data]);

  useEffect(() => {
    if (subscription.status === 200 || subscription.status === 201) {
      getApplicants(path);
    }
  }, [subscription.status]);

  const onClickEnroll = (rowData) => {
    togglePopup('enrollmentForm');
    let programType;
    if (activeProgramType === 'Packages') {
      programType = 'package';
    } else {
      programType = 'course';
    }
    setActiveStudent({
      ...rowData.student,
      registration: rowData.registration.id,
      registration_no: rowData.registration.number,
      [programType]: rowData.program.id,
    });
  };

  const actions = (rowData) => {
    const classes = 'm-1 px-3 py-2';
    const buttons = [];
    if (path === 'applied') {
      buttons.push(
        <Button
          label="Approve"
          icon="pi pi-check-circle"
          className={classes}
          onClick={() => onClickApprove(rowData)}
        />,
      );
    }

    if (path === 'approved') {
      buttons.push(
        <Button
          label="Enroll"
          icon="pi pi-star"
          className={classes}
          onClick={() => onClickEnroll(rowData)}
        />,
      );
    }

    const studentId = rowData?.student?.id;
    buttons.push(
      <Button
        label="View Profile"
        icon="pi pi-eye"
        className={classes}
        severity="success"
        onClick={() => (
          navigate(`/student/${studentId}`)
        )}
      />,
    );

    return buttons;
  };

  const columns = [
    { header: 'Registration No', field: 'registration.number' },
    { header: 'Student', body: getStudent },
    { header: 'Email', field: 'student.email' },
    {
      header: activeProgramType === 'Packages' ? 'Package' : 'Course',
      field: 'program.name',
    },
  ];

  if (path === 'approved' && activeProgramType === 'Packages') {
    columns.push({
      header: 'Enrolled',
      field: 'program.enrollment_status',
      body: getEnrollmentStatus,
      filter: true,
      filterElement: getEnrollmentStatusFilter,
      showFilterMenu: false,
    });
  }

  columns.push({
    header: 'Actions',
    body: actions,
  });

  const pagination = {
    onClickPrev: (e, callback) => {
      if (applicants.data?.previous) {
        applicants.sendRequest({ url: applicants.data.previous });
      }
      callback(e);
    },
    onClickNext: (e, callback) => {
      if (applicants.data?.next) {
        applicants.sendRequest({ url: applicants.data.next });
      }
      callback(e);
    },
  };

  const onSubmitSubscription = (event, formData) => {
    event.preventDefault();
    subscription.sendRequest({
      url: api.studentFee,
      method: 'POST',
      data: {
        ...formData,
        registration: activeStudent.registration,
      },
    });
    togglePopup('subscriptionForm');
  };

  useEffect(() => {
    if (subscription.status === 201) {
      togglePopup('paymentForm');
    }
  }, [subscription.status]);

  const onCancelSubscription = (event) => {
    event.preventDefault();
    togglePopup('subscriptionForm');
  };

  const onEnrollSuccess = () => {
    getApplicants(path);
    togglePopup('enrollmentForm');
    const message = 'Student enrolled to batch successfully';
    enrollmentAlert.setAlert('success', message);
  };

  const selectButton = (
    <SelectButton
      className="mb-4"
      value={activeProgramType}
      onChange={(e) => setActiveProgramType(e.value)}
      options={programOptions}
    />
  );

  return (
    <>
      <PageHeader title={title} selectButton={selectButton} />
      {subscription.getApiAlert}

      <DetailStudentPopup
        visible={showPopup.studentDetail}
        onHide={() => togglePopup('studentDetail')}
        student={student.data}
      />

      <FeeSubscriptionPopup
        visible={showPopup.subscriptionForm}
        onHide={() => togglePopup('subscriptionForm')}
        student={activeStudent}
        togglePopup={() => togglePopup('subscriptionForm')}
        onFormSubmit={onSubmitSubscription}
        onFormCancel={onCancelSubscription}
        excludedFields={excludedFields}
      />

      <FeePayment
        student={activeStudent}
        visible={showPopup.paymentForm}
        togglePaymentPopup={() => togglePopup('paymentForm')}
      />

      <StudentEnrollment
        visible={showPopup.enrollmentForm}
        onHide={() => togglePopup('enrollmentForm')}
        student={activeStudent}
        program={activeProgramType}
        togglePopup={() => togglePopup('enrollmentForm')}
        onSuccess={onEnrollSuccess}
        excludedFields={excludedFields}
      />

      <Dialog
        header="Enrollment Details"
        visible={showPopup.enrollmentDetail}
        style={{ width: '50vw', 'min-height': '50%' }}
        onHide={() => togglePopup('enrollmentDetail')}
      >
        <div className="surface-0 shadow-2 p-4 mt-2 border-round">
          <div className="mb-4">
            <div className="font-medium text-2xl text-900 mb-2">
              {activeStudent?.student_name}
              <Tag
                value={`Reg No: ${activeStudent?.registration?.number}`}
                className="ml-2"
              />
            </div>
            <div className="text-500 mb-3">
              {activeStudent?.program?.name}
            </div>
          </div>
          <Panel header="Enrolled" className="mb-4">
            <ol className="m-0">
              {activeStudent?.program?.courses?.filter?.((course) => (
                course.batch !== null
              ))?.map?.((course) => (
                <li>{course.course_name}</li>
              ))}
            </ol>
          </Panel>
          <Panel header="Unenrolled" className="mb-4">
            <ol className="m-0">
              {activeStudent?.program?.courses?.filter?.((course) => (
                !course.batch
              ))?.map?.((course) => (
                <li>{course.course_name}</li>
              ))}
            </ol>
          </Panel>
        </div>
      </Dialog>

      <ListDataTable
        data={programs}
        header={renderHeader}
        globalFilterFields={[
          'registration.number',
          'student.name',
          'student.email',
          'program.name',
        ]}
        columns={columns}
        filters={filters}
        pagination={pagination}
      />
      {enrollmentAlert.getAlert}
    </>
  );
}

export default ListStudentContainer;
