import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import PageHeader from '../PageHeader';
import FormContainer from './FormContainer';

function CreateContainer() {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const path = pathname.replace(/\/$/, '').replace(/\/create$/, '/');
  const title = 'Add Invoice';
  const [invoice, setInvoice] = useState(null);

  useEffect(() => {
    if (invoice?.status === 200 || invoice?.status === 201) {
      navigate(path);
    }
  }, [navigate, path, invoice?.status]);

  const onFormSubmit = (data, status) => {
    if (!data || !status) {
      return;
    }
    setInvoice({ data, status });
  };

  return (
    <>
      <PageHeader title={title} />
      <FormContainer
        path={path}
        action="create"
        onFormSubmit={onFormSubmit}
      />
    </>
  );
}

export default CreateContainer;
