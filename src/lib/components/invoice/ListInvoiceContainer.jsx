import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import DetailInvoice from './DetailInvoice';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListInvoiceContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const invoices = useApi(api.invoice);
  const [invoiceId, setInvoiceId] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });
  const title = 'Invoice List';

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const togglePopup = () => {
    setShowPopup((prevShowPopup) => {
      if (prevShowPopup) setInvoiceId(null);
      return !prevShowPopup;
    });
  };

  const getDate = (rowData) => (
    dayjs(rowData.date)?.format?.('MMM D, YYYY')
  );

  const onClickViewInvoice = (id) => {
    togglePopup();
    setInvoiceId(id);
  };

  const actions = (rowData) => (
    <Button
      label="View Invoice"
      icon="pi pi-file"
      className="mr-2 px-3 py-2"
      onClick={() => onClickViewInvoice(rowData.id)}
    />
  );

  const columns = [
    { header: 'Title', field: 'title' },
    { header: 'Amount', field: 'invoice_amount' },
    { header: 'Date', body: getDate },
    { header: 'Action', body: actions },
  ];

  const pagination = {
    onClickPrev: (e, callback) => {
      if (invoices.data?.previous) {
        invoices.sendRequest({ url: invoices.data.previous });
      }
      callback(e);
    },
    onClickNext: (e, callback) => {
      if (invoices.data?.next) {
        invoices.sendRequest({ url: invoices.data.next });
      }
      callback(e);
    },
  };

  const buttons = [
    {
      label: 'Add',
      icon: 'plus',
      onClick: () => navigate('/accounting/invoice/create'),
    },
  ];

  return (
    <>
      <PageHeader title={title} buttons={buttons} />
      <ListDataTable
        data={invoices.data}
        header={renderHeader}
        globalFilterFields={[
          'title',
          'invoice_amount',
        ]}
        columns={columns}
        filters={filters}
        pagination={pagination}
      />
      {invoiceId && (
        <DetailInvoice
          visible={showPopup}
          onHide={togglePopup}
          invoiceId={invoiceId}
        />
      )}
    </>
  );
}

export default ListInvoiceContainer;
