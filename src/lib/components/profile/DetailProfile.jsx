import React, { useEffect, useState } from 'react';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { ConfirmDialog } from 'primereact/confirmdialog';
import DetailProfileSection from './DetailProfileSection';
import FormContainer from '../crud/FormContainer';
import useAlert from '../../hooks/useAlert';
import useApi from '../../hooks/useApi';
import api from '../../config/api';
import profile from '../../config/pages/profile.common';

function DetailProfile({ userId }) {
  const education = useApi(`${api.userEducation}/?filter_by={"user":${userId}}`);
  const experience = useApi(`${api.userWorkExperience}/?filter_by={"user":${userId}}`);
  const imUser = useApi(`${api.imUser}/${userId}`);
  const imUserAlert = useAlert();
  const [commonSections, setCommonSections] = useState(null);
  const [educationSection, setEducationSection] = useState(null);
  const [experienceSection, setExperienceSection] = useState(null);
  const [profileSections, setProfileSections] = useState(null);
  const [showPopup, setShowPopup] = useState({
    education: false,
    experience: false,
    notifications: false,
  });

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  useEffect(() => {
    if (commonSections) return;
    const _commonSections = [];
    for (const section in profile) {
      _commonSections.push(
        <div
          key={section.replace('/', '-')}
          className="mb-6"
        >
          <DetailProfileSection
            section={section}
            resourceId={userId}
          />
        </div>,
      );
    }
    setCommonSections(_commonSections);
  }, [userId]);

  const educationEventHandlers = {
    onDelete: () => {
      setEducationSection(null);
      education.sendRequest({
        url: `${api.userEducation}/?filter_by={"user":${userId}}`,
      });
    },
  };

  useEffect(() => {
    if (education.status !== 200 || educationSection) return;
    const _educationSection = [];
    for (const edu of education.data.results) {
      _educationSection.push(
        <div
          key={edu.id}
          className="mb-6"
        >
          <DetailProfileSection
            section="profile/education"
            resourceId={edu.id}
            customEventHandlers={educationEventHandlers}
            multiEntry
          />
        </div>,
      );
    }
    setEducationSection(_educationSection);
  }, [education.status, education.data?.results, userId, educationSection]);

  const experienceEventHandlers = {
    onDelete: () => {
      setExperienceSection(null);
      experience.sendRequest({
        url: `${api.userWorkExperience}/?filter_by={"user":${userId}}`,
      });
    },
  };

  useEffect(() => {
    if (experience.status !== 200 || experienceSection) return;
    const _experienceSection = [];
    for (const exp of experience.data.results) {
      _experienceSection.push(
        <div
          key={exp.id}
          className="mb-6"
        >
          <DetailProfileSection
            section="profile/workexperience"
            resourceId={exp.id}
            customEventHandlers={experienceEventHandlers}
            multiEntry
          />
        </div>,
      );
    }
    setExperienceSection(_experienceSection);
  }, [experience.status, experience.data?.results, userId, experienceSection]);

  const getHeader = (title, popup) => (
    <div className="flex my-2">
      <h3 className="flex-auto flex justify-content-between mt-4">
        {title}
      </h3>
      <div className="flex-auto flex align-items-center justify-content-end font-bold">
        <Button
          label="Add"
          icon="pi pi-plus"
          onClick={() => togglePopup(popup)}
        />
      </div>
    </div>
  );

  useEffect(() => {
    if (!commonSections || !educationSection || !experienceSection) return;
    setProfileSections([
      ...commonSections,
      getHeader('Educational Qualification', 'education'),
      educationSection.length > 0 ? (
        <Card className="surface-ground my-5">
          {educationSection}
        </Card>
      ) : (
        null
      ),
      getHeader('Work Experience', 'experience'),
      experienceSection.length > 0 ? (
        <Card className="surface-ground my-5">
          {experienceSection}
        </Card>
      ) : (
        null
      ),
      getHeader('Phone Notifications', 'notifications'),
    ]);
  }, [commonSections, educationSection, experienceSection]);

  const eventHandlers = {
    education: {
      onSuccess: () => {
        togglePopup('education');
        const url = `${api.userEducation}/?filter_by={"user":${userId}}`;
        setEducationSection(null);
        education.sendRequest({ url });
      },
      onCancel: () => togglePopup('education'),
    },
    experience: {
      onSuccess: () => {
        togglePopup('experience');
        const url = `${api.userWorkExperience}/?filter_by={"user":${userId}}`;
        setExperienceSection(null);
        experience.sendRequest({ url });
      },
      onCancel: () => togglePopup('education'),
    },
    notifications: {
      onSuccess: (status) => {
        togglePopup('notifications');
        let message;
        if (status === 201) {
          message = 'IM connected successfully, you will receive notifications from now on.';
        }
        if (status === 200) {
          message = 'IM connection updated successfully.';
        }
        imUserAlert.setAlert('success', message);
      },
      onCancel: () => togglePopup('notifications'),
    },
  };

  return (
    <>
      {profileSections}

      <Dialog
        header="Add Educational Qualificaiton"
        visible={showPopup.education}
        style={{ width: '75vw' }}
        onHide={() => togglePopup('education')}
      >
        <FormContainer
          path="profile/education"
          defaultData={{ user: userId }}
          eventHandlers={eventHandlers.education}
          redirectOnError={false}
        />
      </Dialog>

      <Dialog
        header="Add Work Experience"
        visible={showPopup.experience}
        style={{ width: '75vw' }}
        onHide={() => togglePopup('experience')}
      >
        <FormContainer
          path="profile/workexperience"
          defaultData={{ user: userId }}
          eventHandlers={eventHandlers.experience}
          redirectOnError={false}
        />
      </Dialog>

      {imUser.status && (
        <Dialog
          header="Phone Notifications"
          visible={showPopup.notifications}
          style={{ width: '50vw' }}
          onHide={() => togglePopup('notifications')}
        >
          <FormContainer
            path="im-user"
            resourceId={imUser.data?.user}
            defaultData={{
              user: userId,
              im: 1,
            }}
            eventHandlers={eventHandlers.notifications}
          />
        </Dialog>
      )}

      <ConfirmDialog />
      {imUserAlert.getAlert}
    </>
  );
}

export default DetailProfile;
