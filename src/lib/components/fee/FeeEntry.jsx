import React from 'react';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';

function FeeEntry({
  onFormSubmit,
  onFormCancel,
}) {
  return (
    <form onSubmit={onFormSubmit}>
      <div className="flex flex-column gap-2">
        <label
          htmlFor="payableFee"
          className="block text-900 font-medium mb-2"
        >
          Fee Amount
        </label>
        <InputText id="payableFee" />
        <small id="username-help">
          Enter payable amount
        </small>
        <div className="mt-2">
          <Button
            type="submit"
            label="Pay Fee"
            icon="pi pi-check"
            className="mr-2 mb-2"
          />
          <Button
            type="button"
            label="Cancel"
            icon="pi pi-times"
            className="mr-2 mb-2 p-button-outlined"
            onClick={onFormCancel}
          />
        </div>
      </div>
    </form>
  );
}

export default FeeEntry;
