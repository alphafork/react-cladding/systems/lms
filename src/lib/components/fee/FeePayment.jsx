import React, { useEffect, useState } from 'react';
import FeeEntryPopup from './FeeEntryPopup';
import InstallmentInvoice from '../invoice/InstallmentInvoice';
import DetailInvoice from '../invoice/DetailInvoice';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function FeePayment({
  student,
  visible,
  togglePaymentPopup,
  onSuccess,
}) {
  const invoiceAddress = useApi();
  const installmentTransaction = useApi();
  const [invoiceData, setInvoiceData] = useState(null);
  const [showPopup, setShowPopup] = useState({
    invoiceForm: false,
    invoiceDetail: false,
  });

  const togglePopup = (key) => {
    setShowPopup((prevShowPopup) => ({
      ...prevShowPopup,
      [key]: !prevShowPopup[key],
    }));
  };

  const onClickCancelDueFee = (event) => {
    event.preventDefault();
    togglePaymentPopup();
  };

  const onSubmitDueFee = (event) => {
    event.preventDefault();
    const url = `${api.userAddress}/${student.id}`;
    invoiceAddress.sendRequest({ url });
    setInvoiceData((prevInvoiceData) => ({
      ...prevInvoiceData,
      title: `Fee Invoice - ${student.name} | ${student.registration_no}`,
      invoice_amount: event.target.payableFee.value,
    }));
    togglePaymentPopup();
    togglePopup('invoiceForm');
  };

  useEffect(() => {
    if (!invoiceAddress.data) return;
    setInvoiceData((prevInvoiceData) => ({
      ...prevInvoiceData,
      address: invoiceAddress.data.address,
    }));
  }, [invoiceAddress.data]);

  const onSubmitInvoice = (invoiceDetails, status) => {
    if (status === 200 || status === 201) {
      onSuccess?.();
    }
    installmentTransaction.sendRequest({
      url: api.feePayment,
      method: 'POST',
      data: {
        invoice: invoiceDetails.id,
        registration: student.registration,
      },
    });
    setInvoiceData((prevInvoiceData) => ({
      ...prevInvoiceData,
      invoiceId: invoiceDetails.id,
    }));
    togglePopup('invoiceForm');
    togglePopup('invoiceDetail');
  };

  return (
    <>
      <FeeEntryPopup
        visible={visible}
        onHide={togglePaymentPopup}
        student={student}
        onFormSubmit={onSubmitDueFee}
        onFormCancel={onClickCancelDueFee}
      />

      <InstallmentInvoice
        visible={showPopup.invoiceForm}
        onHide={() => togglePopup('invoiceForm')}
        invoiceData={invoiceData}
        onFormSubmit={onSubmitInvoice}
      />

      {invoiceData?.invoiceId && (
        <DetailInvoice
          visible={showPopup.invoiceDetail}
          onHide={() => togglePopup('invoiceDetail')}
          invoiceId={invoiceData.invoiceId}
        />
      )}
    </>
  );
}

export default FeePayment;
