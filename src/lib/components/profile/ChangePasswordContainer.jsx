import React from 'react';
import { useNavigate } from 'react-router-dom';
import PageHeader from '../PageHeader';
import FormContainer from '../crud/FormContainer';
import useAlert from '../../hooks/useAlert';

function ChangePasswordContainer() {
  const navigate = useNavigate();
  const passwordAlert = useAlert();

  const eventHandlers = {
    onSuccess: () => {
      const message = 'Password changed successfully';
      passwordAlert.setAlert('success', message);
    },
    onCancel: () => navigate('/'),
  };

  const buttons = [{
    label: 'Back to Home',
    icon: 'angle-left',
    onClick: () => navigate('/'),
    extraClasses: 'p-button-outlined',
  }];

  return (
    <>
      <PageHeader
        title="Change Password"
        buttons={buttons}
      />
      <FormContainer
        path="change-password"
        eventHandlers={eventHandlers}
      />
      {passwordAlert.getAlert}
    </>
  );
}

export default ChangePasswordContainer;
