import React from 'react';
import dayjs from 'dayjs';
import {
  Page, Document, StyleSheet, Font,
} from '@react-pdf/renderer';
import Html from 'react-pdf-html';

function InvoicePdfTemplate({ invoice }) {
  Font.register({ family: 'Helvetica' });
  const styles = StyleSheet.create({
    page: {
      flexDirection: 'row',
      backgroundColor: '#ffffff',
      margin: '5px',
      fontFamily: 'Helvetica',
    },
  });

  const html = `
    <html>
      <head>
        <meta charset="utf-8" />
        <style>
          .invoice-box {
            width: 580px;
            margin: auto;
            padding: 30px;
            border: 1px solid #888;
            font-size: 11px;
            line-height: 22px;
            color: #333;
            font-family: 'Helvetica';
          }

          .invoice-box .main-title {
            font-size:26px;
            font-weight:bold;
            text-align:center;
          }

          .invoice-box table {
            width: 520px;
            line-height: inherit;
            text-align: left;
            border-bottom:1px solid #eee;
            padding-bottom:15px;
          }

          .invoice-box table td {
            padding: 5px;
            vertical-align: top;
          }

          .invoice-box table tr td:nth-child(2) {
            text-align: right;
          }

          .invoice-box table tr.top table td {
            padding-bottom: 20px;
          }

          .invoice-box table tr.top table td.title {
            font-size: 12px;
            line-height: 18px;
            font-weight:bold;
            color: #333;
            height:44px;
          }

          .invoice-box table tr.information table td {
            padding-bottom: 40px;
            text-align:left;
          }

          .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
          }

          .invoice-box table tr.details td {
            padding-bottom: 20px;
          }

          .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
          }

          .invoice-box table tr.item.last td {
            border-bottom: none;
          }

          .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
          }

          .invoice-box table .title img {
            width: 120px;
          }

          .invoice-box .note-box {
            text-align:center;
            margin-top:120px;
            border-top:1px solid #888;
          }

          .invoice-box .note {
            font-weight:bold;
            font-size:14px;
            text=align:center;
            width:100%;
          }

          .invoice-box .note-list li{
            padding-left:10px;
            list-style-type: square;
          }

          .invoice-box .band {
            border-top:1px solid #888;
            border-bottom:1px solid #888;
            padding: 8px 0px;
          }
        </style>
      </head>

      <body>
        <div class="invoice-box">
          <table cellPadding="0" cellSpacing="0">
            <tbody>
              <tr>
                <td colSpan="2">
                  <p class="main-title">Invoice</p>
                </td>
              </tr>

              <tr>
                <td colSpan="2">
                  <table>
                    <tbody>
                      <tr>
                        <td class="title">
                          Invoice No: ${invoice?.data?.invoice_no}<br /> <br/>
                          Date: ${dayjs(invoice?.data?.date).format('DD/MM/YYYY')}<br />
                        </td>
                        <td>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr class="information">
                <td colSpan="2">
                  <table>
                  <tbody>
                    <tr>
                      <td colSpan="2">
                      ${(invoice?.data?.address)?.replaceAll?.('\n', '<br/>')}
                      </td>
                    </tr>
                  </tbody>
                  </table>
                </td>
              </tr>

              <tr class="heading">
                <td>Description</td>
                <td>Amount</td>
              </tr>

              <tr>
                <td>${invoice?.data?.title}</td>
                <td>${invoice?.data?.invoice_amount}</td>
              </tr>

              <tr class="total">
                <td></td>
                <td>Total: ${invoice?.data?.invoice_amount}</td>
              </tr>
              <tr class="band">
                <td><b>Payment Method:</b><br>
                  ${JSON.stringify(invoice?.data?.payment_splitup)
    ?.replaceAll?.('{', '')
    ?.replaceAll?.('}', '')
    ?.replaceAll?.('"', '')
    ?.replaceAll?.(':', ': ')
    ?.replaceAll?.(',', '<br>')
}
                </td>
                <td></td>
              </tr>

              <tr>
                <td>Remarks: ${invoice?.data?.remarks}</td>
                <td></td>
              </tr>

              <tr class="note-box">
                <td colspan="2">
                  <p class="note"><b><u>Please Note</u></b></p>
                </td>
              </tr>

              <tr>
                <td colspan="2">
                  <ul class="note-list">
                    <li>The organization reserves the right to expel any student during the training period.</li>
                    <li>Fees once paid will not be refunded or adjusted against other courses/batches/students under any circumstances whatsoever.</li>
                    <li>Students should always carry their ID cards with them and shall produce the same as and when required.<li>
                    <li>Once a batch is allotted it will be permanent and no shifting will be entertained.</li>
                    <li>In case, a student fails to attend the given batch the given amount will be forfeited against his/her name.</li>
                    <li>Payments of cheques are subject to realization.</li>
                    <li>Any discrepancies are subject to Ernakulam jurisdiction.</li>
                    <li>Vehicles are kept at owner's risk.<li>
                    <li>Down gradation or up gradation is not permitted once the package is selected.</li>
                  </ul>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </body>
    </html>
  `;

  return (
    html && (
      <Document>
        <Page size="A4" orientation="portrait" dpi="72" style={styles.page}>
          <Html resetStyles>{html}</Html>
        </Page>
      </Document>
    )
  );
}

export default InvoicePdfTemplate;
