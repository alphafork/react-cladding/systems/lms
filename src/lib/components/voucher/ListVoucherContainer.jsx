import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import PageHeader from '../PageHeader';
import ListDataTable from '../datatable/ListDataTable';
import DetailVoucher from './DetailVoucher';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function ListTransactionContainer() {
  dayjs.extend(utc);
  const navigate = useNavigate();
  const vouchers = useApi(api.voucher);
  const [voucherId, setVoucherId] = useState(null);
  const [showPopup, setShowPopup] = useState(false);
  const [globalFilterValue, setGlobalFilterValue] = useState('');
  const [filters, setFilters] = useState({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
  });
  const title = 'Voucher List';

  const onGlobalFilterChange = (e) => {
    const { value } = e.target;
    const _filters = { ...filters };
    _filters.global.value = value;
    setFilters(_filters);
    setGlobalFilterValue(value);
  };

  const renderHeader = () => (
    <div className="flex justify-content-end">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          value={globalFilterValue}
          onChange={onGlobalFilterChange}
          placeholder="Keyword Search"
        />
      </span>
    </div>
  );

  const togglePopup = () => {
    setShowPopup((prevShowPopup) => {
      if (prevShowPopup) setVoucherId(null);
      return !prevShowPopup;
    });
  };

  const getDate = (rowData) => (
    dayjs(rowData.date)?.format?.('MMM D, YYYY')
  );

  const onClickViewVoucher = (id) => {
    togglePopup();
    setVoucherId(id);
  };

  const actions = (rowData) => (
    <Button
      label="View Voucher"
      icon="pi pi-file"
      className="mr-2 px-3 py-2"
      onClick={() => onClickViewVoucher(rowData.id)}
    />
  );

  const columns = [
    { header: 'Title', field: 'title' },
    { header: 'Amount', field: 'voucher_amount' },
    { header: 'Date', body: getDate },
    { header: 'Action', body: actions },
  ];

  const pagination = {
    onClickPrev: (e, callback) => {
      if (vouchers.data?.previous) {
        vouchers.sendRequest({ url: vouchers.data.previous });
      }
      callback(e);
    },
    onClickNext: (e, callback) => {
      if (vouchers.data?.next) {
        vouchers.sendRequest({ url: vouchers.data.next });
      }
      callback(e);
    },
  };

  const buttons = [
    {
      label: 'Add',
      icon: 'plus',
      onClick: () => navigate('/accounting/voucher/create'),
    },
  ];

  return (
    <>
      <PageHeader title={title} buttons={buttons} />
      <ListDataTable
        data={vouchers.data}
        header={renderHeader}
        globalFilterFields={[
          'title',
          'voucher_amount',
        ]}
        columns={columns}
        filters={filters}
        pagination={pagination}
      />
      {voucherId && (
        <DetailVoucher
          visible={showPopup}
          onHide={togglePopup}
          voucherId={voucherId}
        />
      )}
    </>
  );
}

export default ListTransactionContainer;
