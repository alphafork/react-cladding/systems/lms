// Parse error messages returned by drf-standardized-errors and run given
// callback function with those errors as input.
const handleError = (error, callback) => {
  let msg = 'Some error occured';
  if (!error) {
    callback('error', msg);
  }

  if (typeof error === 'string') {
    callback('error', error);
  }

  const { errors, type } = error ?? {};
  if (errors && Array.isArray(errors)) {
    const summary = type.replace?.(/_/g, ' ').toUpperCase?.();
    for (const err of errors) {
      const { attr, detail } = err;
      if (!detail) {
        callback('error', msg);
      }
      if (attr && typeof attr === 'string') {
        msg = `${attr[0].toUpperCase?.()}${attr?.slice?.(1)?.replaceAll('_', ' ')}: ${detail}`;
      } else {
        msg = detail;
      }
      callback('error', msg, summary);
    }
  }
};

export default handleError;
