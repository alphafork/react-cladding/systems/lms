import React from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';
import {
  PageHeader,
  StudentProfile,
} from '../../../submodules';
import QuickLinks from './QuickLinks';
import useStore from '../../../hooks/useStore';

function Home() {
  const {
    id: userId,
    role: userRole,
  } = useStore((state) => state.user);

  if (!userRole) {
    return (
      <div className="flex justify-content-center m-5">
        <ProgressSpinner />
      </div>
    );
  }

  if (userRole === 'Student') {
    return (
      <StudentProfile userId={userId} />
    );
  }

  return (
    <>
      <PageHeader title="Home" />
      <QuickLinks userRole={userRole} />
    </>
  );
}

export default Home;
