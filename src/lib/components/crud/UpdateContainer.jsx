import React from 'react';
import { useParams } from 'react-router-dom';
import PageHeader from '../PageHeader';
import FormContainer from './FormContainer';

function UpdateContainer() {
  const { page: path, id: resourceId } = useParams();
  const title = `Update ${path.replace(/-/g, ' ')}`;

  return (
    <>
      <PageHeader title={title} />
      <FormContainer path={path} resourceId={resourceId} />
    </>
  );
}

export default UpdateContainer;
