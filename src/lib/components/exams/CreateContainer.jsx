import React from 'react';
import PageHeader from '../PageHeader';
import FormContainer from '../crud/FormContainer';
import useApi from '../../hooks/useApi';
import api from '../../config/api';

function CreateContainer() {
  const examType = useApi(api.examType);

  const eventHandlers = {
    afterChange: async (event, formData, setFormData) => {
      const { name, value } = event.target;
      const selectedExamType = await examType.data.results.find((item) => (
        item.id === value
      ));

      if (!selectedExamType) {
        return;
      }

      if (name === 'exam_type') {
        const {
          duration_in_minutes: duration,
          max_marks: maxMarks,
          marks_to_qualify: qualificationMarks,
        } = selectedExamType;

        setFormData({
          ...formData,
          duration_in_minutes: duration,
          max_marks: maxMarks,
          marks_to_qualify: qualificationMarks,
        });
      }
    },
  };

  return (
    <>
      <PageHeader title="Add exam" />
      <FormContainer
        path="exam"
        eventHandlers={eventHandlers}
      />
    </>
  );
}

export default CreateContainer;
