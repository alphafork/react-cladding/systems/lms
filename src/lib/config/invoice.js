const invoice = {
  create: {
    excludedFields: [
      'id',
      'payment_splitup',
    ],
  },
};

export default invoice;
