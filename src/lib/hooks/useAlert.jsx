import React, { useRef } from 'react';
import { Toast } from 'primereact/toast';

function useAlert() {
  const toast = useRef(null);

  const setAlert = (type, msg, summary = null) => {
    toast?.current?.show?.({
      severity: type,
      summary: summary ?? type.toUpperCase(),
      detail: msg,
    });
  };

  const resetAlert = () => toast?.current?.clear();

  const getAlert = (<Toast ref={toast} />);

  return {
    setAlert,
    resetAlert,
    getAlert,
  };
}

export default useAlert;
