import React, { useState } from 'react';
import { Dialog } from 'primereact/dialog';
import PageHeader from '../PageHeader';
import Detail from '../crud/Detail';
import FormContainer from '../crud/FormContainer';
import useStore from '../../hooks/useStore';
import useFieldParser from '../../hooks/useFieldParser';
import getConfig from '../../utils/getConfig';

function InstitutionProfile({ id }) {
  const setInstitution = useStore((state) => state.setInstitution);
  const [showPopup, setShowPopup] = useState(false);
  const config = getConfig('institution-profile');
  const {
    displayData,
    getSchemaAlert,
    getApiAlert,
    sendRequest,
  } = useFieldParser(config, id);

  const buttons = [{
    label: 'Edit',
    icon: 'pencil',
    onClick: () => setShowPopup(true),
  }];

  const eventHandlers = {
    onSuccess: (status, data) => {
      setShowPopup(false);
      const url = `${config.api}/${id}`;
      sendRequest({ url });
      if (status === 200 && data) {
        setInstitution(data);
      }
    },
    onCancel: () => {
      setShowPopup(false);
    },
  };

  return (
    <>
      <PageHeader
        title="Institution Profile"
        buttons={buttons}
      />

      <Detail
        headerFields={displayData?.headerFields}
        fields={displayData?.fields}
        showBlankFields
      />

      <Dialog
        header="Edit Profile"
        visible={showPopup}
        style={{ width: '75vw' }}
        onHide={() => setShowPopup(false)}
      >
        <FormContainer
          path="institution-profile"
          resourceId={id}
          eventHandlers={eventHandlers}
        />
      </Dialog>
      {getSchemaAlert}
      {getApiAlert}
    </>
  );
}

export default InstitutionProfile;
